package es.indra.atributos;

import javax.swing.JOptionPane;

import es.indra.gui.ScreenElements;

public class TestAttributes {

	private static String LOGIN = "";
	private static String SENHA = "";

	private static String TIPO_CONTA = "";
	private static String SEGURADORA = "";
	private static String TIPO_TESTE = "";
	private static String TIPO_PESQUISA = "";
	private static String ELEMENTO_BUSCA = "";

	public String getLogin() {
		return LOGIN;
	}

	public void setLogin(String login) {

		if (login.equals(null) | login.equals("")) {
			JOptionPane.showMessageDialog(null, "Nenhum Login inserido");
			ScreenElements.condStartTest = false;
		}

		else
			LOGIN = login;
	}

	public String getSenha() {
		return SENHA;
	}

	public void setSenha(String senha) {

		if (senha.equals(null) | senha.equals("")) {
			JOptionPane.showMessageDialog(null, "Nenhuma senha inserida");
			ScreenElements.condStartTest = false;
		}

		else
			SENHA = senha;
	}

	public String getTipoConta() {
		return TIPO_CONTA;
	}

	public void setTipoConta(String tipoConta) {
		TIPO_CONTA = tipoConta;
	}

	public String getSeguradora() {
		return SEGURADORA;
	}

	public void setSeguradora(String seguradora) {

		if (seguradora == null) {
			ScreenElements.condStartTest = false;
			JOptionPane.showMessageDialog(null, "Nenhuma Seguradora escolhida");
		}

		else
			SEGURADORA = seguradora;
	}

	public String getTipoTeste() {
		return TIPO_TESTE;
	}

	public void setTipoTeste(String tipoTeste) {
		TIPO_TESTE = tipoTeste;
	}

	public String getTipoPesquisa() {
		return TIPO_PESQUISA;
	}

	public void setTipoPesquisa(String tipoPesquisa) {

		try {
			TIPO_PESQUISA = tipoPesquisa;
		}

		catch (Exception e) {
			ScreenElements.condStartTest = false;
			JOptionPane.showMessageDialog(null, "Nenhum tipo de pesquisa selecionado");
		}
	}

	public String getElementoBusca() {
		return ELEMENTO_BUSCA;
	}

	public void setElementoBusca(String elemento) {

		try {

			if (elemento.equalsIgnoreCase("") | elemento.equalsIgnoreCase(null)
					| elemento.equalsIgnoreCase("Digite uma placa")) {

				ScreenElements.condStartTest = false;
				JOptionPane.showMessageDialog(null, "Nenhuma placa inserida para pesquisa");
			}

			ELEMENTO_BUSCA = elemento;

		} catch (Exception e) {
			ScreenElements.condStartTest = false;
			JOptionPane.showMessageDialog(null, "Nenhuma placa inserida para pesquisa");
		}
	}

}
