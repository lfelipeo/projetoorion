package es.indra.atributos;

import java.io.File;

import es.indra.jsonhandler.JsonObj;

public class JsonAttributes {

	private static final String pathLoginJson = "json/Info_Login.json";
	private static JsonObj interfacejs = new JsonObj(new File(pathLoginJson));
	
	public String getLoginJson() throws Exception {
		return (String) interfacejs.get("login");
	}

	public String getSenhaJson() throws Exception {
		return (String) interfacejs.get("senha");
	}

}
