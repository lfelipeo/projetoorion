package es.indra.framework;

import java.util.Random;

import org.openqa.selenium.By;

import es.indra.step.perito.orcamento.AbaBaremoStep;

public class GerarDadosAleatorio extends GerarArquivo {

	/***************************** ALEATORIO ******************************/

	private Random random = new Random();

	private String placaCarro = placa();
	private String fabCarro = fabricantesCarro();
	private String modCarro = modelosCarro(fabCarro);
	private String corCarro = corCarro();
	private String quiloCarro = quilometragemCarro();
	private String valorCarro = valorCarro();
	private String partNumber = partNumber();
	private String descCarroSubstituicao = descricaoCarro();
	private String descCarroReparacao = descricaoCarro();
	private String maoDeObra = maoDeObra();
	private String precoPeca = precoPeca();
	private String descontoPeca = descontoPeca(precoPeca);
	private String pecaCarroBaremo = pecaCarroBaremo();

	private String valorMecanicaMaoDeObra = valorMaoDeObra();
	private String valorFunilariaMaoDeObra = valorMaoDeObra();
	private String valorPinturaMaoDeObra = valorMaoDeObra();
	private String valorEletricaMaoDeObra = valorMaoDeObra();
	private String valorTapecariaMaoDeObra = valorMaoDeObra();
	private String valorVidracariaMaoDeObra = valorMaoDeObra();
	private String valorReparacaoMaoDeObra = valorMaoDeObra();

	private String numeroLinhaLaudoPmg = linhaLaudoPmg();

	/*************************/

	private static String placa() {

		Random r = new Random();
		String alfabeto = "abcdefghijklmnopqrstuvxywz";

		String[] letrasPlaca = { "", "", "" };
		String[] numerosPlaca = { "", "", "", "" };

		for (int i = 0; i < 3; i++) {
			String letra = "" + alfabeto.charAt(r.nextInt(alfabeto.length()));
			letrasPlaca[i] = letra;
		}

		for (int i = 0; i < 4; i++) {
			int numero = r.nextInt(9);
			while ((numero < 0) | (numero > 9)) {
				numero = r.nextInt(9);
			}
			numerosPlaca[i] = "" + numero;
		}

		String letras = "" + letrasPlaca[0].toUpperCase() + "" + letrasPlaca[1].toUpperCase() + ""
				+ letrasPlaca[2].toUpperCase();

		String numeros = "" + numerosPlaca[0] + "" + numerosPlaca[1] + "" + numerosPlaca[2] + "" + numerosPlaca[3];

		String placa = letras + "-" + numeros;

		return placa;
	}

	public String valorAleatorio(int maximo) {
		int valor = random.nextInt(maximo) + 1;
		return "" + valor;
	}

	/*************************/

	/****** DADOS VEICULO *****/

	private String fabricantesCarro() {

		String fabCarro[] = { "CHEVROLET" };
		int aleatorio = random.nextInt(Math.abs(fabCarro.length));
		return fabCarro[aleatorio];
	}

	private String modelosCarro(String fabricantesCarro) {

		if (fabricantesCarro.equalsIgnoreCase("CHEVROLET")) {
			String modelosCarroChevrolet[] = { "ONIX/13 - ONIX 5P" };
			int aleatorio = random.nextInt(Math.abs(modelosCarroChevrolet.length));
			return modelosCarroChevrolet[aleatorio];
		} else if (fabricantesCarro.equalsIgnoreCase("MITSUBISHI")) {
			String modelosCarroMitsubish[] = { "LANCER/10 - LANCER 4P" };
			int aleatorio = random.nextInt(Math.abs(modelosCarroMitsubish.length));
			return modelosCarroMitsubish[aleatorio];
		}

		return null;
	}

	private String corCarro() {
		String corCarro[] = { "PRETO", "VINHO", "CINZA", "BRANCO", "VERDE" };
		int aleatorio = random.nextInt(Math.abs(corCarro.length));
		return corCarro[aleatorio];
	}

	private String quilometragemCarro() {
		int aleatorio = random.nextInt(8000);
		return "" + aleatorio;
	}

	private String valorCarro() {
		int aleatorio = random.nextInt(40000) + 20000;
		return "" + aleatorio;
	}

	public void clicarOpcionais(String id_tabela, int qtnOpcionais) throws Exception {

		int tr = -1;
		int td = -1;

		for (int i = 1; i <= qtnOpcionais; i++) {

			tr = random.nextInt(getDSL().contaQtnTrTabelaId(By.id(id_tabela))) + 1;
			while (tr > getDSL().contaQtnTrTabelaId(By.id(id_tabela))) {
				tr = random.nextInt(getDSL().contaQtnTrTabelaId(By.id(id_tabela))) + 1;
			}

			if (tr == getDSL().contaQtnTrTabelaId(By.id(id_tabela))) {
				td = 1;
			}

			else if (tr != getDSL().contaQtnTrTabelaId(By.id(id_tabela))) {
				td = random.nextInt(3) + 1;
				while (td > 3) {
					td = random.nextInt(3) + 1;
				}
			}

			String xpathClick = "*//table[@id='" + id_tabela + "']/tbody/tr[" + tr + "]/td[" + td + "]/input";
			getDSL().clica(By.xpath(xpathClick));
		}
	}

	/***********************************/

	/******** ABA BAREMO *******/

	public void clicarPecaCarro() throws Exception {

		int tr = random.nextInt(3);
		int td = random.nextInt(3);

		while (tr <= 0 | tr > 3) {
			tr = random.nextInt(3);
		}
		while (td <= 0 | td > 3) {
			td = random.nextInt(3);
		}

		if (td == 1) {
			getDSL().clica(By.xpath("//tr[@id='td_Areas']/td/div/table/tbody/tr[" + tr + "]/td/div"));

		} else if (td == 2 || td == 3) {
			getDSL().clica(By.xpath("//tr[@id='td_Areas']/td/div/table/tbody/tr[" + tr + "]/td[" + td + "]/div"));
		}
	}

	public void clicarOpcaoListaPeca() throws Exception {

		int qtnTr = getDSL().contaQtnTrTabelaId(By.id("tbl_Pecas"));
		int trClick = random.nextInt(qtnTr);

		while (trClick <= 0 || trClick > qtnTr) {
			trClick = random.nextInt(qtnTr);
		}

		if (trClick == 1) {
			getDSL().clica(By.xpath("//*[@id='tbl_Pecas']/tbody/tr/td[2]"));
			String texto = getDSL().pegaTexto(By.xpath("//*[@id='tbl_Pecas']/tbody/tr/td[2]"));
			AbaBaremoStep.pecaBaremo = texto;
		}

		else if (trClick > 1) {
			getDSL().clica(By.xpath("//*[@id='tbl_Pecas']/tbody/tr[" + trClick + "]/td[2]"));
			String texto = getDSL().pegaTexto(By.xpath("//*[@id='tbl_Pecas']/tbody/tr[" + trClick + "]/td[2]"));
			AbaBaremoStep.pecaBaremo = texto;
		}
	}

	public void clicarPrecoPeca(String id_table) throws Exception {

		int qtnTr = getDSL().contaQtnTrTabelaId(By.id(id_table));
		int trClick = random.nextInt(getDSL().contaQtnTrTabelaId(By.id(id_table)));

		String valor = null;

		while ((trClick < 1) || (trClick > getDSL().contaQtnTrTabelaId(By.id(id_table)))) {
			trClick = random.nextInt(getDSL().contaQtnTrTabelaId(By.id(id_table)) + 1);
		}

		if (qtnTr == 1) {
			valor = getDSL().pegaValor(By.xpath("//table[@id='" + id_table + "']/tbody/tr/td[2]/input"));
			AbaBaremoStep.descPecaBaremo = valor;
		}

		else if (trClick >= 1) {

			getDSL().clica(By.xpath("//table[@id='" + id_table + "']/tbody/tr[" + trClick + "]/td/span/input"));
			valor = getDSL()
					.pegaValor(By.xpath("//table[@id='" + id_table + "']/tbody/tr[" + trClick + "]/td[2]/input"));
			AbaBaremoStep.descPecaBaremo = valor;

		}

	}

	public void clicaOrigemPeca() throws Exception {

		String[] tiposOrigem = { "Origem_Original", "Origem_CIA", "Origem_MA" };
		int numeroAle = random.nextInt(tiposOrigem.length);

		while (numeroAle > 2) {
			numeroAle = random.nextInt(tiposOrigem.length);
		}

		String id_origem = "ctl00_cph_Form_rdb_" + tiposOrigem[numeroAle];

		if (getDSL().verificaElementoClicavel(By.id(id_origem))) {
			getDSL().clica(By.id(id_origem));
		}
	}

	/****************************/

	/******* ABA MANUAL *******/

	public String partNumber() {
		int number = random.nextInt(9999999);
		return "" + number;
	}

	public String descricaoCarro() {
		int number = random.nextInt(9999);
		while (number <= 0 || number > 9999) {
			number = random.nextInt(9999);
		}
		return "Teste Pe�a " + number;
	}

	public String maoDeObra() {
		String[] tipoMaoDeObra = { "Mec�nica", "Funilaria", "El�trica", "Tape�aria", "Vidra�aria" };
		int indice = random.nextInt(5);
		while (indice <= 0) {
			indice = random.nextInt(5);
		}
		return tipoMaoDeObra[indice];
	}

	public String precoPeca() {
		int preco = random.nextInt(1000);
		while (preco <= 100) {
			preco = random.nextInt(1000);
		}
		return "" + preco;
	}

	public String descontoPeca(String precoS) {
		int desconto = random.nextInt(90);
		while ((desconto < 0) | (desconto > 90)) {
			desconto = random.nextInt(90);
		}

		return "" + desconto;
	}

	public String pecaCarroBaremo() {
		String[] pecas = { "ctl00_cph_Form_div_Reg1", "ctl00_cph_Form_div_Reg9", "ctl00_cph_Form_div_Reg10" };
		int aleatorio = random.nextInt(pecas.length);
		while ((aleatorio < 0) | (aleatorio > pecas.length)) {
			aleatorio = random.nextInt(pecas.length);
		}
		return pecas[aleatorio];
	}

	/*************************/

	/******* ABA ADICIONAIS *******/

	public void selecionaItensAbaAdicionais(String select) throws Exception {

		getDSL().esperarElementoEstarVisivelId(select);
		int linha = random.nextInt(getDSL().contaQtnItensSelect(By.id(select)));

		while (linha < 0 | linha > getDSL().contaQtnItensSelect(By.id(select))) {
			linha = random.nextInt(getDSL().contaQtnItensSelect(By.id(select)));
		}

		getDSL().esperaElemento(By.id(select));
		getDSL().selecionaComboIndice(By.id(select), linha);

	}

	/*************************/

	/******* M�O-DE-OBRA *******/

	private String valorMaoDeObra() {
		int valor = random.nextInt(100) + 20;
		return "" + valor + ",00";
	}

	/***************************/

	/******* LAUDO SALVOS *******/

	public void selecionarOpcaoLaudoDeSalvados(String id_table, int qtnLinhas) throws Exception {

		int coluna = random.nextInt(4) + 1;
		int qtnTr = qtnLinhas + 1;
		int linha = random.nextInt(qtnTr);

		while (linha <= 1) {
			linha = random.nextInt(qtnTr);
		}

		try {
			getDSL().clica(By.xpath("//*[@id='" + id_table + "']//tbody/tr[" + linha + "]/td[" + coluna + "]"));

		} catch (Exception e) {
			getDSL().clica(By.xpath("//*[@id='" + id_table + "']//tbody/tr[" + linha + "]/td[" + coluna + "]"));
		}

		if (getDSL().verificarAlertVisivel()) {
			getDSL().confirmarAlert();
		}
	}

	/****************************/

	/******* LAUDO PMG *******/

	private String linhaLaudoPmg() {
		int linha = random.nextInt(158);
		while (linha < 138 || linha > 158) {
			linha = random.nextInt(158);
		}
		return "" + linha;
	}

	/*************************/

	/**********************************************************************/

	public String get_placaCarro() {
		return placaCarro;
	}

	public String get_fabCarro() {
		return fabCarro;
	}

	public String get_modCarro() {
		return modCarro;
	}

	public String get_corCarro() {
		return corCarro;
	}

	public String get_quilometragemCarro() {
		return quiloCarro;
	}

	public String get_valorCarro() {
		return valorCarro;
	}

	public String get_partNumber() {
		return partNumber;
	}

	public String get_descCarroSubstituicao() {
		return descCarroSubstituicao;
	}

	public String get_descCarroReparacao() {
		return descCarroReparacao;
	}

	public String get_maoDeObra() {
		return maoDeObra;
	}

	public String get_precoPeca() {
		return precoPeca + ",00";
	}

	public String get_descontoPeca() {
		return descontoPeca;
	}

	public String get_pecaCarroBaremo() {
		return pecaCarroBaremo;
	}

	public String get_valorMecanicaMaoDeObra() {
		return valorMecanicaMaoDeObra;
	}

	public String get_valorFunilariaMaoDeObra() {
		return valorFunilariaMaoDeObra;
	}

	public String get_valorPinturaMaoDeObra() {
		return valorPinturaMaoDeObra;
	}

	public String get_valorEletricaMaoDeObra() {
		return valorEletricaMaoDeObra;
	}

	public String get_valorTapecariaMaoDeObra() {
		return valorTapecariaMaoDeObra;
	}

	public String get_valorVidracariaMaoDeObra() {
		return valorVidracariaMaoDeObra;
	}

	public String get_valorReparacaoMaoDeObra() {
		return valorReparacaoMaoDeObra;
	}

	public String get_numeroLinhaLaudoPmg() {
		return numeroLinhaLaudoPmg;
	}

}
