package es.indra.framework;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class GerarData extends StringEd {

	private Date date = new Date();
	private LocalDate localDate = LocalDate.now();
	private SimpleDateFormat formatar;

	/***************************** DATA/HORA *****************************/

	public String amanha() {
		return "" + localDate.plusDays(1);
	}

	public String ontem() {
		return "" + localDate.plusDays(-1);
	}
	
	public String apartirDeHoje(int dif) {
		return "" + localDate.plusDays(dif);
	}
	
	public String horaAtual() {
		formatar = new SimpleDateFormat("kk:mm");
		return formatar.format(date);
	}

	public String dataHoraAtual() {
		formatar = new SimpleDateFormat("dd/MM/yyyy | kk:mm");
		return formatar.format(date);
	}

	/*********************************************************************/
}
