package es.indra.framework;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.text.Normalizer;

import javax.imageio.ImageIO;

public class GerarArquivo extends GerarData {

	/****************************** ARQUIVO ******************************/
	// Criar arquivo
	public File caminhoPastas(String ambiente, String fluxo, String cliente, String pasta, String nome_arquivo,
			String extensao) {

		File pasta1 = new File("Arquivos/" + ambiente + "/Evidencias/" + cliente + "/" + fluxo + "/" + pasta);
		File arquivo = new File(pasta1 + "/" + nome_arquivo + extensao); // Caminho de arquivo

		if (!pasta1.exists()) { // Verifica se existe pasta criada
			pasta1.mkdirs(); // Cria pastas
			return arquivo;// Retorna a cria��o do arquivo
		} else {
			return arquivo; // Retorna o arquivo existente
		}
	}

	// Cria um buffered de escrita
	public BufferedWriter bufferEscrita(String ambiente, String fluxo, String cliente, String pasta2,
			String nome_arquivo) throws IOException {
		// Cria um buffered de escrita
		BufferedWriter escrever = new BufferedWriter(
				new FileWriter(caminhoPastas(ambiente, fluxo, cliente, pasta2, nome_arquivo, ".txt"), true));
		return escrever;
	}

	// Le arquivo e converter em Array
	public String[] converterArquivoEmArray(String ambiente, String fluxo, String nome_arquivo) throws IOException {
		String arq = new String(
				Files.readAllBytes(caminhoPastas(ambiente, fluxo, "", "", nome_arquivo, ".txt").toPath()));
		arq.replaceAll("\r", "");
		String[] linhas = arq.split("\n");
		return linhas;
	}

	public void escreveArquivoLogTxt(String ambiente, String fluxo, String cliente, String pasta_log,
			String nome_arquivo, String texto) throws IOException {

		// Cria um buffered de escrita
		BufferedWriter escrever = bufferEscrita(ambiente, fluxo, cliente, pasta_log, nome_arquivo);
		// Oraganiza e escreve o arquivo
		escrever.write(texto + " - " + getDSL().dataHoraAtual());
		// desce uma linha
		escrever.newLine();
		// finaliza o processo de escrita
		escrever.close();

	}

	public void printarTela(String ambiente, String fluxo, String pasta, String cliente, String nome_print)
			throws IOException, AWTException {
		Robot robot;
		robot = new Robot();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		BufferedImage img = robot
				.createScreenCapture(new Rectangle(0, 0, (int) screenSize.getWidth(), (int) screenSize.getHeight()));
		ImageIO.write(img, "jpeg", caminhoPastas(ambiente, fluxo, cliente, pasta, nome_print, ".jpeg"));
	}

	public String removeCaracterEspecial(String texto) {
		return Normalizer.normalize(texto, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").replaceAll("[?!]", "");
	}

	/*********************************************************************/
}
