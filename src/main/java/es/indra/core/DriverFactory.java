package es.indra.core;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverFactory  {

	private static WebDriver driver;
	private static DSL dsl = null;
	private static WebDriverWait wait;

	public DriverFactory() {
		getDriver();
	}

	public static DSL getDSL() {
		if (dsl == null) {
			dsl = new DSL();
		}
		return dsl;
	}

	public static WebDriver getDriver() {

		if (driver == null) {

			System.setProperty("webdriver.ie.driver", "webdrivers/32bit/13/IEDriverServer.exe");

			driver = new InternetExplorerDriver(new IEOptions());

			driver.manage().window().maximize();

			driver.manage().timeouts().setScriptTimeout(1, TimeUnit.MINUTES);
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(1, TimeUnit.MINUTES);
		}

		return driver;
	}

	public WebDriverWait getWait() {
		wait = new WebDriverWait(getDriver(), 40);
		wait.ignoring(UnhandledAlertException.class).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class);
		return wait;
	}

	protected void voltaTempoDeEspera() {
		driver.manage().timeouts().setScriptTimeout(1, TimeUnit.MINUTES);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(1, TimeUnit.MINUTES);
	}
}
