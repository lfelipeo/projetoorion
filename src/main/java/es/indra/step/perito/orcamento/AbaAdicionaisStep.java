package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.AbaAdicionaisPage;

/**
 * 
 * @author lfelipeo
 *
 */

public class AbaAdicionaisStep extends BaseTeste {

	private OrcamentoStepVar varStep = new OrcamentoStepVar();
	private AbaAdicionaisPage page = new AbaAdicionaisPage();
	private BaseAction baseAc = new BaseAction();

	/**
	 * Clica na aba 'Adicionais'.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Clica na aba Adicionais - \"([^\"]*)\"$")
	public void abaAdicionais(String fluxo) throws Exception {

		try {

			page.clicaAbaAdicionaisOrcamento();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA ADICIONAIS", e);
		}

	}

	/**
	 * Seleciona uma op��o em 'Itens' na aba Adicionais e escreve o log em caso de
	 * sucesso.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@And("^Seleciona uma opcao em Itens na aba Adicionais - \"([^\"]*)\"$")
	public void selecionaItensAbaAdicionaisOrcamento(String fluxo) throws Exception {

		try {

			page.selecionaItens();

			baseAc.escreverLog(
					"ITEM SELECIONADO COM SUCESSO NA COLUNA ITENS NA ABA ADICIONAIS NA TELA DE ORCAMENTO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA ADICIONAIS", e);
		}
	}

	/**
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@And("^Preenche as informacoes na aba Adicionais - \"([^\"]*)\"$")
	public void preencheInfoAbaAdicionaisOrcamento(String fluxo) throws Exception {

		try {

			page.preencheInfoValorOuHora();

			BaseAction.tirarPrint(varStep.getPrintOrcamentoAbaAdicionais());

			baseAc.escreverLog(
					"ITEM SELECIONADO COM SUCESSO NA COLUNA ITENS NA ABA ADICIONAIS NA TELA DE ORCAMENTO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA ADICIONAIS", e);
		}

	}

	@Then("^Clica Adicionar na aba Adicionais - \"([^\"]*)\"$")
	public void adicionaInfoAbaAdicionaisOrcamento(String fluxo) throws Exception {

		try {

			page.clicaAdicionar();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA ADICIONAIS", e);
		}
	}
}
