package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.AbaBaremoPage;

public class AbaBaremoStep extends BaseTeste {

	/* luiz � top */
	
	private AbaBaremoPage page = new AbaBaremoPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();
	private BaseAction baseAc = new BaseAction();

	public static String pecaBaremo;
	public static String descPecaBaremo;
	public static String origem;

	public static boolean eUmaPeca;
	public static boolean pecaAdicionada;

	/**
	 * Verifica se a aba Baremo est� visivel na tela de Or�amento.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("Verifica aba Baremo esta visivel na tela de Orcamento - \"([^\"]*)\"$")
	public void abaBaremo(String fluxo) throws Exception {

		try {
			
			//page.verificarTelaOrcamentoVisivel();
			page.frameAgenda();
			
		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA BAREMO", e);
		}
	}

	/**
	 * Seleciona uma regi�o do carro na aba Baremo na tela de Or�amento e escreve o
	 * log.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@And("^Seleciona uma regiao do carro na Aba Baremo na tela de Orcamento - \"([^\"]*)\"$")
	public void selecionaRegiaoCarroAbaBaremo(String fluxo) throws Exception {

		try {

			page.selecionaRegiaoCarroBaremo();
			baseAc.escreverLog("REGI�O DO CARRO SELECIONADA COM SUCESSO NA ABA BAREMO NA TELA DE OR�AMENTO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA BAREMO", e);
		}
	}

	/**
	 * Adiciona uma pe�a na aba Baremo na tela de Or�amento, tira print e escreve o
	 * log.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Seleciona e adiciona uma peca do carro na Aba Baremo na tela de Orcamento - \"([^\"]*)\"$")
	public void selecionaPecaCarroAbaBaremo(String fluxo) throws Exception {

		try {

			for (int i = 0; i < 1; i++) {

				AbaBaremoStep.pecaAdicionada = false;

				page.selecionaPecaEOrigemBaremo();

				origem = page.opcaoSelecionadaOrigem();

				//baseAc.escreverLog("PE�A SELECIONADA COM SUCESSO NA ABA BAREMO NA TELA DE OR�AMENTO - " + fluxo);

				BaseAction.tirarPrint(varStep.get_printOrcamentoBaremo());

				page.clicaAdicionarBaremo();

				page.verificaSeEUmaPecaOuServico();

				AbaBaremoStep.pecaAdicionada = true;

				//page.esperaTelaOrcamentoVoltarAEstarVisivel();

				baseAc.escreverLog("PE�A ADICIONADA COM SUCESSO NA ABA BAREMO NA TELA DE OR�AMENTO - " + fluxo);

			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA BAREMO", e);
		}
	}

}
