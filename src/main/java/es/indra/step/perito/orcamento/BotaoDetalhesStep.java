package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.BotaoDetalhesPage;

public class BotaoDetalhesStep extends BaseTeste {

	private BotaoDetalhesPage page = new BotaoDetalhesPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();

	private BaseAction baseAc = new BaseAction();

	@Then("^Clica no botao Detalhes na Tela Orcamento - \"([^\"]*)\"$")
	public void botaoDetalhesOrcamento(String fluxo) throws Exception {

		try {

			page.clicaBotaoDetalhes();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO DETALHES NA TELA DE ORCAMENTO", e);
		}
	}

	@And("^Verifica a placa em Detalhes na Tela Orcamento - \"([^\"]*)\"$")
	public void verificaPlacaDetalhes(String fluxo) throws Exception {

		try {

			// Verifica se a placa consta em 'Detalhes Or�amento'
			page.verificaPlacaDetalhesOrcamento(varStep.getElementoBusca());

			// Tira print
			BaseAction.tirarPrint(varStep.get_printDetalhes());

			// Escreve log
			baseAc.escreverLog("DETALHES VERIFICADOS COM SUCESSO NA TELA DE OR�AMENTO" + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO DETALHES NA TELA DE ORCAMENTO", e);
		}

	}

	@Then("^Fecha Detalhes na Tela Orcamento - \"([^\"]*)\"$")
	public void fechaDetalhes(String fluxo) throws Exception {

		try {

			// Fecha 'Detalhes Or�amento'
			page.fechaDetalhesOrcamento();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO DETALHES NA TELA DE ORCAMENTO", e);
		}
	}

}
