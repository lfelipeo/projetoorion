package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.page.perito.orcamento.testclass.AbaFotosPage;

public class AbaFotosStep {

	private AbaFotosPage page = new AbaFotosPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();
	private BaseAction baseAc = new BaseAction();

	/**
	 * Clica na aba 'Fotos' na tela de Or�amento.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Clica na aba fotos - \"([^\"]*)\"$")
	public void abaFotos(String fluxo) throws Exception {

		try {
			page.clicaAbaFotos();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA FOTOS", e);
		}
	}

	// json

	/**
	 * Importa uma foto.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@And("^Envia a foto em Fotos - \"([^\"]*)\"$")
	public void enviaFoto(String fluxo) throws Exception {

		try {

			page.clicaSelecionar();

			page.selecionaFoto(varStep.get_nomeFoto());

			page.clicaImportar();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA FOTOS", e);
		}

	}

	@Then("^Verifica se a foto foi adicionada em Fotos - \"([^\"]*)\"$")
	public void verificaFotoImportada(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.get_printOrcamentoFotos());

			Thread.sleep(150);

			page.verificaSeFotoFoiAdicionada();

			baseAc.escreverLog("FOTO NA ABA FOTOS EXPORTADA COM SUCESSO NA TELA DE OR�AMENTO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA FOTOS", e);
		}
	}
}
