package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.BotaoMaoDeObraPage;

public class BotaoMaoDeObraStep extends BaseTeste {

	private BotaoMaoDeObraPage page = new BotaoMaoDeObraPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();
	private BaseAction baseAc = new BaseAction();

	/***************** M�o-De-Obra (Bot�o) *****************/

	@Then("^Clica no botao Mao-De-Obra na tela de Orcamento - \"([^\"]*)\"$")
	public void botaoMaoDeObra(String fluxo) throws Exception {

		try {

			page.clicaMaoDeObra();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO MAO-DE-OBRA", e);
		}
	}

	@And("^Preenche o campo Mecanica em Mao-de-Obra na tela de Orcamento - \"([^\"]*)\"$")
	public void mecanicaMaoDeObra(String fluxo) throws Exception {

		try {
			page.preencheValorMecanicaMaoDeObra(varStep.get_valorMecanicaMaoDeObra());
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO MAO-DE-OBRA", e);
		}
	}

	@And("^Preenche o campo Funilaria em Mao-de-Obra na tela de Orcamento - \"([^\"]*)\"$")
	public void funilariaMaoDeObra(String fluxo) throws Exception {

		try {
			page.preencheValorFunilariaMaoDeObra(varStep.get_valorFunilariaMaoDeObra());
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO MAO-DE-OBRA", e);
		}
	}

	@And("^Preenche o campo Pintura em Mao-de-Obra na tela de Orcamento - \"([^\"]*)\"$")
	public void pinturaMaoDeObra(String fluxo) throws Exception {

		try {
			page.preencheValorPinturaMaoDeObra(varStep.get_valorPinturaMaoDeObra());
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO MAO-DE-OBRA", e);
		}
	}

	@And("^Preenche o campo Eletrica em Mao-de-Obra na tela de Orcamento - \"([^\"]*)\"$")
	public void eletricaMaoDeObra(String fluxo) throws Exception {

		try {
			page.preencheValorEletricaMaoDeObra(varStep.get_valorEletricaMaoDeObra());
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO MAO-DE-OBRA", e);
		}
	}

	@And("^Preenche o campo Tapecaria em Mao-de-Obra na tela de Orcamento - \"([^\"]*)\"$")
	public void tapecariaMaoDeObra(String fluxo) throws Exception {

		try {
			page.preencheValorTapecariaMaoDeObra(varStep.get_valorTapecariaMaoDeObra());
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO MAO-DE-OBRA", e);
		}
	}

	@And("^Preenche o campo Vidracaria em Mao-de-Obra na tela de Orcamento - \"([^\"]*)\"$")
	public void vidracariaMaoDeObra(String fluxo) throws Exception {

		try {
			page.preencheValorVidracariaMaoDeObra(varStep.get_valorVidracariaMaoDeObra());
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO MAO-DE-OBRA", e);
		}
	}

	@And("^Preenche o campo Reparacao em Mao-de-Obra na tela de Orcamento - \"([^\"]*)\"$")

	public void reparacaoMaoDeObra(String fluxo) throws Exception {

		try {
			page.preencheValorReparacaoMaoDeObra(varStep.get_valorReparacaoMaoDeObra());
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO MAO-DE-OBRA", e);
		}
	}

	@Then("^Clica no botao Confirmar em Mao-De-Obra na tela de Orcamento - \"([^\"]*)\"$")
	public void confirmaMaoDeObra(String fluxo) throws Exception {

		try {

			// Tirar o print
			BaseAction.tirarPrint(varStep.get_printMaoDeObra());

			// Confirma as informa��es preenchidas em 'M�o-de-obra'
			page.clicaConfirmar();

			// Escreve o log
			baseAc.escreverLog("VALORES PREENCHIDOS COM SUCESSO EM 'M�O-DE-OBRA' - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO MAO-DE-OBRA", e);
		}
	}

	/************************************************/

}
