package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.AbaManualPage;

/**
 * 
 * @author lfelipeo
 *
 */

public class AbaManualStep extends BaseTeste {

	private AbaManualPage page = new AbaManualPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();
	private BaseAction baseAc = new BaseAction();

	public static String pecaSubstituicao;
	public static boolean condSubstituicao = false;

	public static String pecaReparacao;
	public static boolean condReparacao = false;

	public static String pecaPintura;
	public static boolean condPintura = false;

	/**
	 * Clica na aba 'Manual' na tela de Or�amento.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Clica na aba manual na tela de Orcamento - \"([^\"]*)\"$")
	public void abaManual(String fluxo) throws Exception {

		try {
			page.clicaAbaManual();
		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA MANUAL", e);
		}

	}

	/**
	 * Seleciona as pe�as em 'Substitui��o', tira print, confirma a pe�a e verifica
	 * se a pe�a foi adicionada.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@And("^Preenche as informacoes em Substituicao na aba Manual - \"([^\"]*)\"$")
	public void substituicaoAbaManual(String fluxo) throws Exception {

		try {

			pecaSubstituicao = varStep.get_descricaoSubstituicao();

			page.selecionaPecaSubstituicao(varStep.get_partNumber(), pecaSubstituicao, varStep.get_maoDeObra(),
					varStep.get_precoPeca(), varStep.get_descontoPeca());

			BaseAction.tirarPrint(varStep.get_printOrcamentoManualSubstituicao());

			page.confirmaPecaSubstituicao();

			page.verificaTabelaDescricaoSubstituicao(pecaSubstituicao);

			condSubstituicao = true;

			baseAc.escreverLog("PE�AS ESCOLHIDAS COM SUCESSO EM 'SUBSTITUI��O' ABA MANUAL - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA MANUAL", e);
		}
	}

	/**
	 * Clica na aba 'Repara��o', seleciona uma pe�a, tira print, confirma a pe�a e
	 * verifica se a pe�a foi adicionada.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@And("^Preenche as informacoes em Reparacao na aba Manual - \"([^\"]*)\"$")
	public void reparacaoAbaManual(String fluxo) throws Exception {

		try {

			page.clicaReparacaoAbaManual();

			pecaReparacao = varStep.get_descricaoReparacao();

			page.selecionaPecaReparacao(pecaReparacao);

			BaseAction.tirarPrint(varStep.get_printOrcamentoManualReparacao());

			page.confirmaPecaReparacao(varStep.get_descricaoReparacao());

			page.verificaTabelaDescricaoReparacao(varStep.get_descricaoReparacao());

			condReparacao = true;

			baseAc.escreverLog("PE�AS ESCOLHIDAS COM SUCESSO EM 'REPARA��O' ABA MANUAL - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA MANUAL", e);
		}
	}

	/**
	 * Clica na aba 'Pintura', seleciona uma pe�a, tira print, confirma a pe�a e
	 * verifica se a pe�a foi adicionada.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@And("^Preenche as informacoes em Pintura na aba Manual - \"([^\"]*)\"$")
	public void pinturaAbaManual(String fluxo) throws Exception {

		try {

			page.clicaPinturaAbaManual();

			pecaPintura = "1234";

			page.selecionarPecaPinturaManualOrcamento(pecaPintura, "3", "10", "50");

			BaseAction.tirarPrint(varStep.getPrintOrcamentoManualPintura());

			page.confirmarPecaPinturaManualOrcamento();

			page.verificaTabelaDescricaoReparacao("1234");

			condPintura = true;

			baseAc.escreverLog("PE�AS ESCOLHIDAS COM SUCESSO EM 'PINTURA' ABA MANUAL - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA ABA MANUAL", e);
		}

	}

	/************************************************/

}
