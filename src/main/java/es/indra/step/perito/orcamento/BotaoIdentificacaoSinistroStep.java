package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.BotaoIdentificacaoSinistroPage;

public class BotaoIdentificacaoSinistroStep extends BaseTeste {

	private BotaoIdentificacaoSinistroPage page = new BotaoIdentificacaoSinistroPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();

	private BaseAction baseAc = new BaseAction();

	@Then("^Clica no botao Identificacao de Sinistro - \"([^\"]*)\"$")
	public void botaoIdentificacaoSinistro(String fluxo) throws Exception {

		try {

			page.clicaBotaoIdentificacaoSinistro();

			// Escreve log
			baseAc.escreverLog("TELA DE IDENTIFICAÇÃO DE SINISTRO VERIFICADA COM SUCESSO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO IDENTIFICACAO DO SINISTRO", e);
		}

	}

	@And("^Verifica a placa em Identificacao de Sinistro - \"([^\"]*)\"$")
	public void verificaEComparaPlaca(String fluxo) throws Exception {

		try {

			page.verificarPlaca(varStep.getElementoBusca());

			BaseAction.tirarPrint(varStep.get_printIdentificacaoSinistro());

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO IDENTIFICACAO DO SINISTRO", e);
		}
	}

	@Then("^Fecha Identificacao de Sinistro - \"([^\"]*)\"$")
	public void fechaIdentificacaoSinistro(String placa) throws Exception {

		try {

			page.fechaIdentificacaoSinistro();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO IDENTIFICACAO DO SINISTRO", e);
		}
	}

	/******************************************************************/
}
