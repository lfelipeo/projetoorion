package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.PreOrcamentoPage;

public class PreOrcamentoStep extends BaseTeste {

	private OrcamentoStepVar varStep = new OrcamentoStepVar();
	private PreOrcamentoPage page = new PreOrcamentoPage();
	private BaseAction baseAc = new BaseAction();

	@Then("^Abre a tela de Pre Orcamento - \"([^\"]*)\"$")
	public void abrePreOrcamento(String fluxo) throws Exception {

		if (varStep.getSeguradora().equalsIgnoreCase("MITSUI")) {

			/*
			 * Abre a tela de Pr�-Or�amento, caso o Sinistro seja novo ir� abrir depois da
			 * tela Dados do Veiculo, caso contr�rio ir� clicar no bot�o 'Pr�-Or�amento' na
			 * tela de Or�amento. S� executara o 'Pr� Or�amento' se a seguradora for a
			 * Mitsui,devido limita��es da seguradora 'Tokio' (09/05/2018).
			 */

			try {
				page.esperaAjaxEIcone();

				if (page.verificarAlertVisivel()) {
					page.confirmarAlert();

				} else {
					page.abrePreOrcamento();
				}

				BaseAction.tirarPrint(varStep.get_printPreOrcamento());
				baseAc.escreverLog("TELA DE PR� OR�AMENTO ABERTA COM SUCESSO - " + fluxo);

			} catch (Exception e) {
				baseAc.logarErro("ERRO EM PRE ORCAMENTO NA TELA DE ORCAMENTO", e);
			}
		}
	}

	@And("^Seleciona uma regiao do carro em Pre Orcamento na tela de Orcamento - \"([^\"]*)\"$")
	public void escolheRegiaoCarroPreOrcamento(String fluxo) throws Exception {

		/*
		 * Seleciona uma regi�o do carro, clica no bot�o 'Calcular Pr�-Or�amento'.
		 */

		if (varStep.getSeguradora().equalsIgnoreCase("MITSUI")) {

			try {

				page.escolheRegiaoPreOrcamento();
				page.clicaBtnCalcularPreOrcamento();

			} catch (Exception e) {
				baseAc.logarErro("ERRO EM PRE ORCAMENTO NA TELA DE ORCAMENTO", e);
			}
		}

	}

	@And("^Escolhe peca em Pre Orcamento na tela de Orcamento - \"([^\"]*)\"$")
	public void escolhePecaPreOrcamento(String fluxo) throws Exception {

		/*
		 * Escolha uma pe�a em 'Pe�as' na tela de Pr�-Or�amento.
		 */

		if (varStep.getSeguradora().equalsIgnoreCase("MITSUI")) {

			try {

				page.escolhePecaTelaPreOrcamento();

				BaseAction.tirarPrint(varStep.get_printPreOrcamento());
				baseAc.escreverLog(
						"PE�AS SELECIONADA COM SUCESSO NA PRIMEIRA TELA (PR� OR�AMENTO) EM PR� OR�AMENTO - " + fluxo);

			} catch (

			Exception e) {
				baseAc.logarErro("ERRO EM PRE ORCAMENTO NA TELA DE ORCAMENTO", e);
			}
		}
	}

	@And("^Escolhe peca na tela Pecas em Pre Orcamento na tela de Orcamento - \"([^\"]*)\"$")
	public void escolhePecaTelaPecasPreOrcamento(String fluxo) throws Exception {

		if (varStep.getSeguradora().equalsIgnoreCase("MITSUI")) {

			try {

				page.clicaBtnAdicionarTelaPreOrcamento();

				page.selecionaRegiaoCarroTelaPecasPreOrcamento();

				page.selecionaListaPecasServicosPreOrcamento();

				page.clicaBtnAdicionarListaPecasPreOrcamento();

				// Tirar o print
				BaseAction.tirarPrint(varStep.get_printPreOrcamento());

				// Escreve o log
				baseAc.escreverLog("PE�AS ESCOLHIDAS COM SUCESSO NA SEGUNDA TELA (PE�AS) EM PR� OR�AMENTO - " + fluxo);

			} catch (Exception e) {
				baseAc.logarErro("ERRO EM PRE ORCAMENTO NA TELA DE ORCAMENTO", e);
			}
		}
	}

	@Then("^Clica no botao atualizar do Pre Orcamento na tela Orcamento - \"([^\"]*)\"$")
	public void atualizaPreOrcamento(String fluxo) throws Exception {

		if (varStep.getSeguradora().equalsIgnoreCase("MITSUI")) {

			try {

				// Confirma o Pr� Or�amento clicando em atualizar
				page.clicaBtnAtualizarTelaPreOrcamento();

				// Tirar o print
				BaseAction.tirarPrint(varStep.get_printPreOrcamento());

				// Escreve o log
				baseAc.escreverLog("PR� OR�AMENTO FINALIZADO COM SUCESSO - " + fluxo);

			} catch (Exception e) {
				baseAc.logarErro("ERRO EM PRE ORCAMENTO NA TELA DE ORCAMENTO", e);
			}
		}
	}
}
