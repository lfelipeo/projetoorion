package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.page.perito.orcamento.testclass.BotaoLaudoPmgPage;

public class BotaoLaudoPmgStep {

	private BotaoLaudoPmgPage page = new BotaoLaudoPmgPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();

	private BaseAction baseAc = new BaseAction();

	@Then("^Clica no botao Laudo PMG na tela de Orcamento - \"([^\"]*)\"$")
	public void botaoLaudoPmg(String fluxo) throws Exception {

		try {

			page.clicaLaudoPmgBotao();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO LAUDO PMG", e);
		}

	}

	@And("^Seleciona uma opcao em PMG Automovel em Laudo PMG - \"([^\"]*)\"$")
	public void pmgAutomovel(String fluxo) throws Exception {

		try {

//			if (varStep.getSeguradora().equalsIgnoreCase("Mitsui"))
//				page.selecionaPecaColunaDanificada(varStep.get_linhaColunaDanificadaLaudoPmg());
//
//			if (varStep.getSeguradora().equalsIgnoreCase("Tokio"))
//				page.selecionaPecaColunaDanificada(varStep.get_linhaColunaDanificadaLaudoPmgTOKIO());
			
			
			System.out.println("Passou do ifs!");
			page.selecionaColunaDanificadaTeste();

			baseAc.escreverLog("OP��O ESCOLHIDA COM SUCESSO EM PMG AUTOM�VEL EM LAUDO DE PMG - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO LAUDO PMG", e);
		}
	}

	@And("^Preenche as informacoes em Observacao em Laudo PMG - \"([^\"]*)\"$")
	public void observacao(String fluxo) throws Exception {

		try {

			page.clicaObservacao();

			page.preencherObservacaoLaudoPmg("Teste Automatizado", "Luiz", "123", "123");

			baseAc.escreverLog("OBSERVACAO PREENCHIDA COM SUCESSO EM LAUDO PMG - " + fluxo);

			BaseAction.tirarPrint(varStep.getPrintLaudoPmgObservacao());

			page.clicaConfirmarObservacao();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO LAUDO PMG", e);
		}
	}

	@Then("^Clica em Confirmar em Laudo PMG - \"([^\"]*)\"$")
	public void confirma(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.getPrintLaudoPmgObservacao());

			page.clicaConfirmar();

			page.esperaOBotaoVoltarASerVisivel();

			baseAc.escreverLog("LAUDO PMG CONFIRMADO COM SUCESSO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO LAUDO PMG", e);
		}
	}

}
