package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.BotaoEmissaoDeRelatoriosPage;

public class BotaoEmissaoDeRelatoriosStep extends BaseTeste {

	private BotaoEmissaoDeRelatoriosPage page = new BotaoEmissaoDeRelatoriosPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();
	private BaseAction baseAc = new BaseAction();

	private static String janelaEmissaoRelatorios;

	public static boolean condHistorico;
	public static boolean condRevisao;

	/**
	 * Clica no bot�o Emiss�o de Rel�torios na tela de Or�amento e atribui o id da
	 * janela principal a variavel 'janelaEmissaoRelatorios'.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Clica no botao Emissao de Relatorios - \"([^\"]*)\"$")
	public void botaoEmissaoRelatorios(String fluxo) throws Exception {

		try {

			page.clicaEmissaoDeRelatorios();
			janelaEmissaoRelatorios = page.pegarIdJanela();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no botao 'Ordenar Por Descri��o' em Emiss�o de Rel�torios, troca para a
	 * janela que ir� abrir e verifica as pe�as adicionadas. Caso uma pe�a tenha
	 * sido adicionada na aba Baremo e for uma do tipo pe�a ir� verificar em
	 * 'Substitui��o', caso seja do tipo servi�o ir� verificar em
	 * 'Desmontagem/Montagem'. Se uma pe�a for adiciona na aba Manual, ser�
	 * verificada conforme o seu tipo: Substitui��o, Repara��o ou Pintura.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Abre a janela Ordenar Por Descricao em Emissao de Relatorios - \"([^\"]*)\"$")
	public void ordenarPorDescricao(String fluxo) throws Exception {

		try {

			page.clicaOrdenarPorDescricao();
			page.trocaParaJanela(varStep.getUrlOrdenarPorDescricaoEmissaoRelatorios());

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}

	}

	@And("^Verifica a peca adicionada na aba Baremo na janela Ordenar Por Descricao em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPecaBaremoOrdenarPorDescricao(String fluxo) throws Exception {

		try {
			if (AbaBaremoStep.pecaAdicionada) {

				if (AbaBaremoStep.eUmaPeca) {
					page.verificaSubstituicaoOrdenarPorDescricao(AbaBaremoStep.pecaBaremo);
					baseAc.escreverLog(
							"PECA VERIFICADA COM SUCESSO EM SUBSTITUICAO NA JANELA ORDENAR POR DESCRICAO - " + fluxo);
				} else {
					page.verificaDesmontagemMontagemOrdenarPorDescricao(AbaBaremoStep.pecaBaremo);
					baseAc.escreverLog(
							"PECA VERIFICADA COM SUCESSO EM DESMONTAGEM/MONTAGEM NA JANELA ORDENAR POR DESCRICAO - "
									+ fluxo);
				}
			}
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica a peca adicionada na aba Manual na janela Ordenar Por Descricao em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPecaManualOrdenarPorDescricao(String fluxo) throws Exception {

		try {

			if (AbaManualStep.condSubstituicao) {
				page.verificaSubstituicaoOrdenarPorDescricao(AbaManualStep.pecaSubstituicao);
				baseAc.escreverLog(
						"PECA VERIFICADA COM SUCESSO EM SUBSTITUICAO NA JANELA ORDENAR POR DESCRICAO - " + fluxo);
			}

			if (AbaManualStep.condReparacao) {
				page.verificaReparacaoOrdenarPorDescricao(AbaManualStep.pecaReparacao);
				baseAc.escreverLog(
						"PECA VERIFICADA COM SUCESSO EM REPARACAO NA JANELA ORDENAR POR DESCRICAO - " + fluxo);
			}

			if (AbaManualStep.condPintura) {
				page.verificaPinturaOrdenarPorDescricao(AbaManualStep.pecaPintura);
				baseAc.escreverLog("PECA VERIFICADA COM SUCESSO EM PINTURA NA JANELA ORDENAR POR DESCRICAO - " + fluxo);
			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha a janela Ordenar Por Descricao em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaOrdenarPorDescricao(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.getPrintOrdenarPorDescricaoEmissaoDeRelatorios());
			page.fecharSegundaTela(janelaEmissaoRelatorios);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no bot�o 'Pe�a' em Emiss�o de Rel�torios, troca para a janela que ir�
	 * abrir.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Abre a janela Peca em Emissao de Relatorios - \"([^\"]*)\"$")
	public void peca(String fluxo) throws Exception {

		try {
			page.clicaPeca();
			page.trocaParaJanela(varStep.get_urlPecasEmissaoRelatorios());
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica a peca adicionada na aba Baremo na janela Peca em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPecaBaremoPeca(String fluxo) throws Exception {

		try {

			if (AbaBaremoStep.eUmaPeca && AbaBaremoStep.descPecaBaremo != null) {

				if (AbaBaremoStep.origem.contains("Original Genu�na")) {
					page.verificaOriginalGenuinaPeca(AbaBaremoStep.descPecaBaremo);
					baseAc.escreverLog(
							"PECA ADICIONADA NA ABA BAREMO VERIFICADA COM SUCESSO EM ORIGINAL GENUINA NA JANELA PECA - "
									+ fluxo);
				}

				else if (AbaBaremoStep.origem.contains("Fornecido pela Cia")) {
					page.verificaFornecidoPelaCiaPeca(AbaBaremoStep.descPecaBaremo);
					baseAc.escreverLog(
							"PECA ADICIONADA NA ABA BAREMO VERIFICADA COM SUCESSO EM FORNECIDO PELA CIA NA JANELA PECA - "
									+ fluxo);
				}

				else if (AbaBaremoStep.origem.contains("Original Gen�rica")) {
					page.verificaOriginalGenericaPeca(AbaBaremoStep.descPecaBaremo);
					baseAc.escreverLog(
							"PECA ADICIONADA NA ABA BAREMO VERIFICADA COM SUCESSO EM ORIGINAL GENERICA NA JANELA PECA - "
									+ fluxo);
				}
			}
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica a peca adicionada na aba Manual na janela Peca em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPecaManualPeca(String fluxo) throws Exception {

		try {

			if (AbaManualStep.condSubstituicao) {
				page.verificaOriginalGenuinaPeca(AbaManualStep.pecaSubstituicao);
				baseAc.escreverLog(
						"PECA ADICIONADA NA ABA MANUAL VERIFICADA COM SUCESSO EM ORIGINAL GENUINA NA JANELA PECA - "
								+ fluxo);
			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha a janela Peca em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaPeca(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.getPrintPecaEmissaoDeRelatorios());
			page.fecharSegundaTela(janelaEmissaoRelatorios);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no bot�o 'Servi�o' em Emiss�o de Rel�torios, troca para a janela que
	 * ir� abrir.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@And("^Abre a janela Servico em Emissao de Relatorios - \"([^\"]*)\"$")
	public void servico(String fluxo) throws Exception {

		try {

			page.clicaServico();
			page.trocaParaJanelaServico(varStep.get_urlServicosEmissaoRelatorios());

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}

	}

	@And("^Verifica a peca adicionada na aba Baremo na janela Servico em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPecaBaremoServico(String fluxo) throws Exception {

		try {

			if (AbaBaremoStep.pecaAdicionada) {
				page.verificaPecaServico(AbaBaremoStep.pecaBaremo);
			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica a peca adicionada na aba Manual na janela Servico em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPecaManualServico(String fluxo) throws Exception {

		try {

			if (AbaManualStep.condSubstituicao) {
				page.verificaPecaServico(AbaManualStep.pecaSubstituicao);
			}
			if (AbaManualStep.condReparacao) {
				page.verificaPecaServico(AbaManualStep.pecaReparacao);
			}
			if (AbaManualStep.condPintura) {
				page.verificaPecaServico(AbaManualStep.pecaPintura);
			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha a janela Servico em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaServico(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.getPrintPecaEmissaoDeRelatorios());
			page.fecharSegundaTela(janelaEmissaoRelatorios);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no bot�o 'Salvado' em Emiss�o de Relat�rios, troca para a janela que
	 * ir� abrir.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Abre a janela Salvado em Emissao de Relatorios - \"([^\"]*)\"$")
	public void salvado(String fluxo) throws Exception {

		try {

			page.clicaSalvado();
			page.trocaParaJanela(varStep.getUrlSalvadoEmissaoRelatorios());

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica o numero da placa do Sinistro na janela Salvado em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPlacaSinistroSalvado(String fluxo) throws Exception {

		try {

			page.verificaNumeroPecaSinistroSalvado(varStep.getElementoBusca());

			baseAc.escreverLog("PE�A DO SINISTRO VERIFICADA COM SUCESSO NA JANELA SALVADO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha a janela Salvado em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaSalvado(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.getPrintSalvadoEmissaoDeRelatorios());
			page.fecharSegundaTela(janelaEmissaoRelatorios);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no bot�o 'Pmg' em Emiss�o de Relat�rios, troca para a janela que ir�
	 * abrir.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Abre a janela Pmg em Emissao de Relatorios - \"([^\"]*)\"$")
	public void pmg(String fluxo) throws Exception {

		try {

			page.clicaPmg();
			page.trocaParaJanela(varStep.getUrlPmgEmissaoRelatorios());

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica o numero da placa do Sinistro na janela Pmg em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPlacaSinistroPmg(String fluxo) throws Exception {

		try {

			page.verificaNumeroPlacaSinistroPmg(varStep.getElementoBusca());

			baseAc.escreverLog("PE�A DO SINISTRO VERIFICADA COM SUCESSO NA JANELA PMG - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha a janela Pmg em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaPmg(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.getPrintPmgEmissaoDeRelatorios());
			page.fecharSegundaTela(janelaEmissaoRelatorios);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no bot�o 'Foto' em Emiss�o de Relat�rios, troca para a janela que ir�
	 * abrir.
	 * 
	 * @param fluxo
	 * @throws Exception
	 * @param fluxo
	 */

	@Then("^Abre o pop-up Foto em Emissao de Relatorios - \"([^\"]*)\"$")
	public void foto(String fluxo) throws Exception {

		try {

			if (varStep.getSeguradora().equalsIgnoreCase("Tokio")) {

			}

			else
				page.clicaFoto();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica a tabela de fotos na janela Foto em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaFoto(String fluxo) throws Exception {

		try {

			if (varStep.getSeguradora().equalsIgnoreCase("Tokio")) {

			}

			else
				page.verificaTableFoto();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha o pop-up Foto em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaFoto(String fluxo) throws Exception {

		try {

			if (varStep.getSeguradora().equalsIgnoreCase("Tokio")) {

			}

			else {
				BaseAction.tirarPrint(varStep.getPrintFotoEmissaoDeRelatorios());
				page.fechaFoto();
			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no bot�o 'Historico' em Emiss�o de Relat�rios, troca para a janela que
	 * ir� abrir.
	 * 
	 * @param fluxo
	 * @throws Exception
	 * @param fluxo
	 */

	@Then("^Abre a janela Historico em Emissao de Relatorios - \"([^\"]*)\"$")
	public void historico(String fluxo) throws Exception {

		try {

			page.clicaHistorico();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica o numero da placa do Sinistro na janela Historico em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPlacaSinistroHistorico(String fluxo) throws Exception {

		try {

			page.verificaJanelaOuAlert(varStep.getUrlHistoricoEmissaoRelatorios());

			page.verificaNumeroPlacaSinistroHistorico(varStep.getElementoBusca());

			baseAc.escreverLog("PE�A DO SINISTRO VERIFICADA COM SUCESSO NA JANELA PMG - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha a janela Historico em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaHistorico(String fluxo) throws Exception {

		try {

			if (BotaoEmissaoDeRelatoriosStep.condHistorico) {

				BaseAction.tirarPrint(varStep.getPrintHistoricoEmissaoDeRelatorios());

				page.fecharSegundaTela(janelaEmissaoRelatorios);
			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no bot�o 'Revisao' em Emiss�o de Relat�rios, troca para a janela que
	 * ir� abrir.
	 * 
	 * @param fluxo
	 * @throws Exception
	 * @param fluxo
	 */

	@Then("^Abre a janela Revisao em Emissao de Relatorios - \"([^\"]*)\"$")
	public void revisao(String fluxo) throws Exception {

		try {

			page.clicaRevisao();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica o numero da placa do Sinistro na janela Revisao em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPlacaSinistroRevisao(String fluxo) throws Exception {

		try {

			page.verificaPopUpRevisao();

			if (BotaoEmissaoDeRelatoriosStep.condRevisao) {
				page.clicaConfirmarRevisao();
				page.trocaParaJanela(varStep.getUrlRevisaoEmissaoRelatorios());
				page.verificaNumeroPlacaSinistroRevisao(varStep.getElementoBusca());
			}

			baseAc.escreverLog("PE�A DO SINISTRO VERIFICADA COM SUCESSO NA JANELA PMG - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha a janela Revisao em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaRevisao(String fluxo) throws Exception {

		try {

			if (BotaoEmissaoDeRelatoriosStep.condRevisao) {
				BaseAction.tirarPrint(varStep.getPrintRevisaoEmissaoDeRelatorios());
				page.fecharSegundaTela(janelaEmissaoRelatorios);
			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no bot�o 'Acesso' em Emiss�o de Relat�rios, troca para a janela que ir�
	 * abrir.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Abre a janela Acesso em Emissao de Relatorios - \"([^\"]*)\"$")
	public void acesso(String fluxo) throws Exception {

		try {
			page.clicaAcesso();
			page.trocaParaJanela(varStep.getUrlAcessoEmissaoRelatorios());
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica o numero da placa do Sinistro na janela Acesso em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPlacaSinistroAcesso(String fluxo) throws Exception {

		try {

			page.verificaNumeroPlacaSinistroAcesso(varStep.getElementoBusca());

			baseAc.escreverLog("PE�A DO SINISTRO VERIFICADA COM SUCESSO NA JANELA PMG - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha a janela Acesso em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaAcesso(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.getPrintAcessoEmissaoDeRelatorios());
			page.fecharSegundaTela(janelaEmissaoRelatorios);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no bot�o 'Connect' em Emiss�o de Relat�rios, troca para a janela que
	 * ir� abrir.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Abre a janela Connect em Emissao de Relatorios - \"([^\"]*)\"$")
	public void connect(String fluxo) throws Exception {

		try {

			page.clicaConnect();
			page.trocaParaJanela(varStep.getUrlConnectEmissaoRelatorios());

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica o numero da placa do Sinistro na janela Connect em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPlacaSinistroConnect(String fluxo) throws Exception {

		try {

			page.verificaNumeroPlacaSinistroConnect(varStep.getElementoBusca());

			baseAc.escreverLog("PE�A DO SINISTRO VERIFICADA COM SUCESSO NA JANELA CONNECT - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha a janela Connect em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaConnect(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.getPrintConnectEmissaoDeRelatorios());
			page.fecharSegundaTela(janelaEmissaoRelatorios);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no bot�o 'Avarias' em Emiss�o de Relat�rios, troca para a janela que
	 * ir� abrir.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Abre a janela Avarias em Emissao de Relatorios - \"([^\"]*)\"$")
	public void avarias(String fluxo) throws Exception {

		try {

			page.clicaAvarias();
			page.trocaParaJanela(varStep.getUrlAvariasEmissaoRelatorios());

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica o numero da placa do Sinistro na janela Avarias em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPlacaSinistroAvarias(String fluxo) throws Exception {

		try {

			page.verificaNumeroPlacaSinistroAvarias(varStep.getElementoBusca());
			baseAc.escreverLog("PE�A DO SINISTRO VERIFICADA COM SUCESSO NA JANELA AVARIAS - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha a janela Avarias em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaAvarias(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.getPrintAvariasEmissaoDeRelatorios());
			page.fecharSegundaTela(janelaEmissaoRelatorios);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	/**
	 * Clica no bot�o 'Fotos Avarias' em Emiss�o de Relat�rios, troca para a janela
	 * que ir� abrir.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Abre a janela Fotos Avarias em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fotosAvarias(String fluxo) throws Exception {

		try {

			page.clicaFotosAvariadas();
			;
			page.trocaParaJanela(varStep.getUrlFotosAvariasEmissaoRelatorios());

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@And("^Verifica o numero da placa do Sinistro na janela Fotos Avarias em Emissao de Relatorios - \"([^\"]*)\"$")
	public void verificaPlacaSinistroFotosAvarias(String fluxo) throws Exception {

		try {

			page.verificaNumeroPlacaSinistroFotosAvarias(varStep.getElementoBusca());
			baseAc.escreverLog("PE�A DO SINISTRO VERIFICADA COM SUCESSO NA JANELA FOTOS AVARIAS - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	@Then("^Fecha a janela Fotos Avarias em Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaFotosAvarias(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.getPrintFotosAvariasEmissaoDeRelatorios());
			page.fecharSegundaTela(janelaEmissaoRelatorios);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}

	////

	@Then("^Fecha Emissao de Relatorios - \"([^\"]*)\"$")
	public void fechaEmissaoRelatorios(String fluxo) throws Exception {

		try {

			page.fechaEmissaoDeRelatorios();
			page.verificarTelaOrcamentoVisivel();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO EMISSAO DE RELATORIOS", e);
		}
	}
}
