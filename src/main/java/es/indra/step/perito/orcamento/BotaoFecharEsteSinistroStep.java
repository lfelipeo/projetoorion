package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.BotaoFecharEsteSinistroPage;

public class BotaoFecharEsteSinistroStep extends BaseTeste {

	private BotaoFecharEsteSinistroPage page = new BotaoFecharEsteSinistroPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();

	private BaseAction baseAc = new BaseAction();

	private static String janelaPrincipal;

	/**
	 * Clica no bot�o 'Fechar este Sinistro' na tela de Or�amento.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Clica no botao Fechar Esta Sinistro na tela de Orcamento - \"([^\"]*)\"$")
	public void botaoFecharEsteOrcamento(String fluxo) throws Exception {

		try {

			page.clicaBotaoFecharEsteSinistro();
			janelaPrincipal = page.pegarIdJanela();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO FECHAR ESTE SINISTRO", e);
		}
	}

	/**
	 * Seleciona uma op��o em 'Carimbo do Processo', baseado em qual seguradora
	 * estiver sendo usada.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@And("^Seleciona uma opcao em Carimbo do Processo em Fechar Esta Sinistro - \"([^\"]*)\"$")
	public void carimboDoProcesso(String fluxo) throws Exception {

		try {

			if (varStep.getSeguradora().equalsIgnoreCase("tokio")) {
				page.selecionaCarimboDoProcesso(varStep.get_carimboTokio());
			}

			else if (varStep.getSeguradora().equalsIgnoreCase("mitsui")) {
				page.selecionaCarimboDoProcesso(varStep.get_carimboMitsui());
			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO FECHAR ESTE SINISTRO", e);
		}
	}

	/**
	 * Escreve em 'Observa��o'.
	 * 
	 * @throws Exception
	 */

	@And("^Preenche Observacao em Fechar Esta Sinistro - \"([^\"]*)\"$")
	public void observacao(String fluxo) throws Exception {

		try {
			page.escreveObservacao("Teste Automatizado!");
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO FECHAR ESTE SINISTRO", e);
		}
	}

	/**
	 * Tira um print e clica no bot�o 'Confirmar'.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Clica em Confirmar em Fechar Esta Sinistro - \"([^\"]*)\"$")
	public void confirmar(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.get_printEncerramentoSinistro());
			page.clicaConfirmar();
			baseAc.escreverLog("OR�AMENTO FOI FINALIZADO COM SUCESSO EM FECHAR ESTE SINISTRO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO FECHAR ESTE SINISTRO", e);
		}
	}

	@And("^Verifica a janela pos botao confirmar em Fechar Esta Sinistro - \"([^\"]*)\"$")
	public void verificaJanelaPosConfirmacao(String fluxo) throws Exception {

		try {

				page.verificaJanelaPosConfirmacao(varStep.getUrlJanelaPosFecharEsteSinistro());
				BaseAction.tirarPrint(varStep.getPrintJanelaPosFecharEsteSinistro());
				page.fecharSegundaTela(janelaPrincipal);
				baseAc.escreverLog("JANELA P�S FECHAR ESTE SINISTRO VERIFICADA E PRINTADA COM SUCESSO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO FECHAR ESTE SINISTRO", e);
		}
	}
}
