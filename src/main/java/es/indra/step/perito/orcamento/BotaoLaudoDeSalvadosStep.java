package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.BotaoLaudoDeSalvadosPage;

public class BotaoLaudoDeSalvadosStep extends BaseTeste {

	private BotaoLaudoDeSalvadosPage page = new BotaoLaudoDeSalvadosPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();

	private BaseAction baseAc = new BaseAction();

	@Then("^Clica no botao Laudo de Salvados - \"([^\"]*)\"$")
	public void botaoLaudosDeSalvados(String fluxo) throws Exception {

		try {

			page.clicaBotaoLaudoDeSalvados();
			baseAc.escreverLog("LAUDOS DE SALVADOS ABERTO COM SUCESSO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO	LAUDOS DE SALVADOS", e);
		}
	}

	@And("^Seleciona a opcao em Carroceria em Laudo de Salvados - \"([^\"]*)\"$")
	public void carroceriaLaudoDeSalvados(String fluxo) throws Exception {

		try {

			if (page.verificaOpcaoExiste("CARROCERIA")) {

				// Seleciona uma op��o em 'CARROCERIA'
				page.selecionaSituacao("CARROCERIA", 4);

				// Tira print
				BaseAction.tirarPrint(varStep.getPrintCarroceriaLaudoDeSalvados());

			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO EM CARROCERIA EM LAUDO DE SALVADOS", e);
		}
	}

	@And("^Seleciona a opcao em Vidros em Laudo de Salvados - \"([^\"]*)\"$")
	public void vidrosLaudoDeSalvados(String fluxo) throws Exception {

		try {

			if (page.verificaOpcaoExiste("VIDROS")) {

				// Seleciona a op��o 'VIDROS' em Qualifica��o
				page.selecionaOpcaoQualificacao("VIDROS");

				// Seleciona uma op��o em 'VIDROS'
				page.selecionaSituacao("VIDROS", 4);

			}

			// Tira print
			BaseAction.tirarPrint(varStep.getPrintVidrosLaudoDeSalvados());

		} catch (Exception e) {
			baseAc.logarErro("ERRO EM VIDROS EM LAUDO DE SALVADOS", e);
		}
	}

	@And("^Seleciona a opcao em Acessorios em Laudo de Salvados - \"([^\"]*)\"$")
	public void acessoriosLaudosDeSalvados(String fluxo) throws Exception {

		try {

			if (page.verificaOpcaoExiste("ACESS�RIOS")) {

				// Seleciona a op��o 'ACESS�RIOS' em Qualifica��o
				page.selecionaOpcaoQualificacao("ACESS�RIOS");

				// Seleciona uma op��o em 'ACESS�RIOS'
				page.selecionaSituacao("ACESS�RIOS", 4);

				// Tira print
				BaseAction.tirarPrint(varStep.getPrintAcessoriosLaudoDeSalvados());

			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO EM ACESS�RIOS EM LAUDO DE SALVADOS", e);
		}
	}

	@And("^Seleciona a opcao em Pneus/Roda em Laudo de Salvados - \"([^\"]*)\"$")
	public void pneusRodaLaudoDeSalvados(String fluxo) throws Exception {

		try {

			if (page.verificaOpcaoExiste("PNEUS/RODA")) {

				// Seleciona a op��o 'PNEUS/RODA' em Qualifica��o
				page.selecionaOpcaoQualificacao("PNEUS/RODA");

				// Seleciona uma op��o em 'PNEUS/RODA'
				page.selecionaSituacao("PNEUS/RODA", 4);

				// Tira print
				BaseAction.tirarPrint(varStep.getPrintPneusRodaLaudoDeSalvados());

			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO EM PNEUS/RODA EM LAUDO DE SALVADOS", e);
		}
	}

	@And("^Seleciona a opcao em Instalacao Eletrica em Laudo de Salvados - \"([^\"]*)\"$")
	public void instalacaoEletricaLaudoDeSalvados(String fluxo) throws Exception {

		try {

			if (page.verificaOpcaoExiste("INSTALA��O EL�TRICA")) {

				page.selecionaOpcaoQualificacao("INSTALA��O EL�TRICA");

				page.selecionaSituacao("INSTALA��O EL�TRICA", 4);

				BaseAction.tirarPrint(varStep.getPrintInstalacaoEletricaLaudoDeSalvados());

			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO EM INSTALA��O EL�TRICA EM LAUDO DE SALVADOS", e);
		}
	}

	@And("^Seleciona a opcao em Mecanica em Laudo de Salvados - \"([^\"]*)\"$")
	public void mecanicaLaudoDeSalvados(String fluxo) throws Exception {

		try {

			String mecanica = "MEC�NICA";

			if (page.verificaOpcaoExiste(mecanica)) {

				// Seleciona a op��o 'MEC�NICA' em Qualifica��o
				page.selecionaOpcaoQualificacao(mecanica);

				// Seleciona uma op��o em 'MEC�NICA'
				page.selecionaSituacao(mecanica, 4);

				// Tira print
				BaseAction.tirarPrint(varStep.getPrintMecanicaLaudoDeSalvados());

			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO EM MEC�NICA EM LAUDO DE SALVADOS", e);
		}
	}

	@Then("^fecha Laudo de Salvados - \"([^\"]*)\"$")
	public void fecharLaudoDeSalvados(String fluxo) throws Exception {

		try {

			page.fecharLaudoDeSalvados();

		} catch (Exception e) {
			baseAc.logarErro("ERRO AO FECHAR LAUDO DE SALVADOS", e);
		}
	}

}
