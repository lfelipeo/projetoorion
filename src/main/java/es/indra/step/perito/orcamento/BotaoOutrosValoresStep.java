package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.BotaoOutrosValoresPage;

public class BotaoOutrosValoresStep extends BaseTeste {

	private BotaoOutrosValoresPage page = new BotaoOutrosValoresPage();
	private BaseAction baseAc = new BaseAction();

	private OrcamentoStepVar varStep = new OrcamentoStepVar();

	@Then("^Clica no botao Outros Valores - \"([^\"]*)\"$")
	public void botaoOutrosValores(String fluxo) throws Exception {

		try {

			// Clica no bot�o e abre a tela 'Outros Valores'
			page.abreOutrosValoresBotao();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO OUTROS VALORES NA TELA ORCAMENTO", e);
		}
	}

	@And("^Altera a Previsao de Entrega em Outros Valores - \"([^\"]*)\"$")
	public void alteraPrevisaoDeEntregaOutrosValores(String fluxo) throws Exception {

		try {

			page.alteraPrevisaoDeEntrega(getDSL().apartirDeHoje(7));

			page.alteraPrevisaoDeEntregaHora(getDSL().horaAtual());

			baseAc.escreverLog(
					"PREVIS�O DE ENTREGA ALTERADA COM SUCESSO EM OUTROS VALORES NA TELA DE OR�AMENTO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO OUTROS VALORES NA TELA ORCAMENTO", e);
		}
	}

	@And("^Altera a Importancia Segurada em Outros Valores - \"([^\"]*)\"$")
	public void alteraImportanciaSeguradaOutrosValores(String fluxo) throws Exception {

		try {

			page.alteraImportanciaSegurada();

			baseAc.escreverLog(
					"IMPORT�NCIA SEGURADA ALTERADA COM SUCESSO EM OUTROS VALORES NA TELA DE OR�AMENTO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO OUTROS VALORES NA TELA ORCAMENTO", e);
		}
	}

	@And("^Preenche o capmo Observacao em Outros Valores - \"([^\"]*)\"$")
	public void preencheObservacao(String fluxo) throws Exception {

		try {

			page.preencheObservacao("Teste Automatizado - " + getDSL().dataHoraAtual());

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO OUTROS VALORES NA TELA ORCAMENTO", e);
		}
	}

	@Then("^Clica em confirmar Outros Valores - \"([^\"]*)\"$")
	public void confirmaOutrosValores(String fluxo) throws Exception {

		try {

			BaseAction.tirarPrint(varStep.getPrintOutrosValores());

			baseAc.escreverLog("OUTROS VALORES CONFIRMADOS COM SUCESSO NA TELA DE OR�AMENTO - " + fluxo);

			page.clicaConfirmarOutrosValores();

			page.verificaBotaoOutrosValoresVoltoouASerVisivel();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO OUTROS VALORES NA TELA ORCAMENTO", e);
		}
	}

	@Then("^Fecha Outros Valores - \"([^\"]*)\"$")
	public void fecharOutrosValores(String fluxo) throws Exception {

		try {

			page.fechaOutrosValores();
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO OUTROS VALORES NA TELA ORCAMENTO", e);
		}
	}

}
