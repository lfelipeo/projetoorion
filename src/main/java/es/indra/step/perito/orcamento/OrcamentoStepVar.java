package es.indra.step.perito.orcamento;

import java.io.File;

import es.indra.atributos.TestAttributes;
import es.indra.framework.GerarDadosAleatorio;
import es.indra.jsonhandler.JsonObj;

public class OrcamentoStepVar {

	private TestAttributes testAt = new TestAttributes();
	private GerarDadosAleatorio dadosAle = new GerarDadosAleatorio();

	private static final String pathFotoJson = "json/Foto.json";
	private JsonObj fotojs = new JsonObj(new File(pathFotoJson));

	/***** OR�AMENTO - BAREMO *****/
	private static final String _PECA_ABS_IMPACTOS = "ABSORVEDOR IMPACTOS DIA";
	private static String PECA_BAREMO = "";
	/*****************************/

	/***** OR�AMENTO - FOTOS *****/
	private final String _NOME_FOTO = fotojs.get("nome");
	/*****************************/

	/***** OR�AMENTO - EMISS�O RELAT�RIOS *****/

	private final String URL_ORDENAR_POR_DESC_EMISSAO_RELATORIOS = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Orcamento/web_Relatorio_Orcamento";

	private final String URL_PECAS_EMISSAO_RELATORIOS = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Pecas/web_Relatorio_Pecas";

	private final String URL_SERVICOS_EMISSAO_RELATORIOS = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Ordem_Servico/web_Relatorio_Ordem_Servico";

	private final String URL_SALVADO_EMISSAO_RELATORIOS = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Salvados/web_Relatorio_Salvados";

	private final String URL_PMG_EMISSAO_RELATORIOS = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Pmg";

	private final String URL_ACESSO_EMISSAO_RELATORIOS = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Acesso";

	private final String URL_CONNECT_EMISSAO_RELATORIOS = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Connect";

	private final String URL_AVARIAS_EMISSAO_RELATORIOS = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Avarias/web_Relatorio_Avarias";

	private final String URL_FOTOS_AVARIAS_EMISSAO_RELATORIOS = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Avarias/web_Relatorio_Fotos_Avarias";

	private final String URL_HISTORICO_EMISSAO_RELATORIOS = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Historico";

	private final String URL_REVISAO_EMISSAO_RELATORIOS = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Revisao";
	/*****************************/

	private static final String _CARIMBO[] = { "Liberado Reparos", "Reparos Liberados" };
	private static final String _SITUACAO_SINISTRO_ESPERADO = "REALIZADO";
	private final String URL_JANELA_POS_FECHAR_ESTE_SINISTRO = "http://valida.orionbr.com.br/" + getSeguradora()
			+ ".orcamentos.v6/Relatorio/Orcamento/web_Relatorio";

	/********** PRINTS **********/

	private final String _PRINT_DADOS_VEICULO = "Dados_Veiculo";
	private final String _PRINT_PRE_ORCAMENTO = "Pre_Orcamento";
	private final String _PRINT_ORCAMENTO_BAREMO = "AbaBaremo";

	private final String _PRINT_ORCAMENTO_MANUAL_SUBSTITUICAO = "AbaManual_Substituicao";
	private final String _PRINT_ORCAMENTO_MANUAL_REPARACAO = "AbaManual_Reparacao";
	private final String PRINT_ORCAMENTO_MANUAL_PINTURA = "AbaManual_Pintura";

	private final String PRINT_ABA_ADICIONAIS = "AbaAdicionais";

	private final String _PRINT_ORCAMENTO_FOTOS = "AbaFotos";
	private final String _PRINT_BOTAO_FOTOS = "Fotos";
	private final String _PRINT_LISTA_PECAS = "ListaPecas";

	private final String _PRINT_CARROCERIA_LAUDO_DE_SALVADOS = "LaudoDeSalvados_Carroceria";
	private final String _PRINT_VIDROS_LAUDO_DE_SALVADOS = "LaudoDeSalvados_Vidros";
	private final String _PRINT_ACESSORIOS_LAUDO_DE_SALVADOS = "LaudoDeSalvados_Acessorios";
	private final String _PRINT_PNEUS_RODA_LAUDO_DE_SALVADOS = "LaudoDeSalvados_PneusRodas";
	private final String _PRINT_INSTALACAO_ELETRICA_LAUDO_DE_SALVADOS = "LaudoDeSalvados_InstalacaoEletrica";
	private final String _PRINT_MECANICA_LAUDO_DE_SALVADOS = "LaudoDeSalvados_Mecanica";

	private final String PRINT_LAUDO_PMG = "LaudoPMG";
	private final String PRINT_LAUDO_PMG_OBSERVACAO = "LaudoPMG_Observacao";

	private final String _PRINT_PECA_EMISSAO_RELATORIOS = "EmissaoDeRelatorios_Peca";
	private final String _PRINT_SERVICO_EMISSAO_RELATORIOS = "EmissaoDeRelatorios_Servico";
	private final String PRINT_ORDENAR_POR_DESCRICAO_EMISSAO_DE_RELATORIOS = "EmissaoDeRelatorios_OrdenarPorDescricao";
	private final String PRINT_PECA_EMISSAO_DE_RELATORIOS = "EmissaoDeRelatorios_Peca";
	private final String PRINT_SALVADO_EMISSAO_DE_RELATORIOS = "EmissaoDeRelatorios_Salvado";
	private final String PRINT_PMG_EMISSAO_DE_RELATORIOS = "EmissaoDeRelatorios_Pmg";
	private final String PRINT_FOTO_EMISSAO_DE_RELATORIOS = "EmissaoDeRelatorios_Foto";
	private final String PRINT_HISTORICO_EMISSAO_DE_RELATORIOS = "EmissaoDeRelatorios_Historico";
	private final String PRINT_REVISAO_EMISSAO_DE_RELATORIOS = "EmissaoDeRelatorios_Revisao";
	private final String PRINT_ACESSO_EMISSAO_DE_RELATORIOS = "EmissaoDeRelatorios_Acesso";
	private final String PRINT_CONNECT_EMISSAO_DE_RELATORIOS = "EmissaoDeRelatorios_Connect";
	private final String PRINT_AVARIAS_EMISSAO_DE_RELATORIOS = "EmissaoDeRelatorios_Avarias";
	private final String PRINT_FOTOS_AVARIAS_EMISSAO_DE_RELATORIOS = "EmissaoDeRelatorios_FotosAvarias";

	private final String _PRINT_ENCERRAMENTO_SINISTRO = "Encerramento";
	private final String _PRINT_PERFIL_ANALISTA = "Perfil_Analista";

	private final String _PRINT_DETALHES = "Detalhes";
	private final String _PRINT_IDENTIFICACAO_SINISTRO = "Identificacao_Sinistro";
	private final String _PRINT_MAO_DE_OBRA = "Mao_De_Obra";

	private final String PRINT_OUTROS_VALORES = "Outros_Valores";

	private final String PRINT_JANELA_POS_FECHAR_ESTE_SINISTRO = "FecharEsteSinistro_JanelaFinal";

	/*****************************/

	/****************************************************/
	/********************* GETTERS *********************/
	/****************************************************/

	/***** INTERFACE *****/

	public String getSeguradora() {
		return testAt.getSeguradora().toLowerCase();
	}

	public String getElementoBusca() {
		return testAt.getElementoBusca();
	}

	public String getColunaRef() {
		return testAt.getTipoPesquisa();
	}

	/********************/

	/***** OR�AMENTO *****/

	public String get_carimboTokio() {
		return _CARIMBO[0];
	}

	public String get_carimboMitsui() {

		return _CARIMBO[1];
	}

	/***** OR�AMENTO - BAREMO *****/

	public String get_pecaBaremo() {
		return dadosAle.pecaCarroBaremo();
	}

	public void setPecaBaremo(String nomePeca) {
		PECA_BAREMO = nomePeca;
	}

	public static String getPecaBaremo() {
		return PECA_BAREMO;
	}

	/*****************************/

	/***** OR�AMENTO - MANUAL *****/

	public String get_partNumber() {
		return dadosAle.partNumber();
	}

	public String get_descricaoSubstituicao() {
		return dadosAle.get_descCarroSubstituicao();
	}

	public String get_descricaoReparacao() {
		return dadosAle.get_descCarroReparacao();
	}

	public String get_maoDeObra() {
		return dadosAle.maoDeObra();
	}

	public String get_precoPeca() {
		return dadosAle.get_precoPeca();
	}

	public String get_descontoPeca() {
		return dadosAle.get_descontoPeca();
	}

	/*****************************/

	/***** OR�AMENTO - FOTOS *****/

	public String get_nomeFoto() {
		return _NOME_FOTO;
	}

	/*****************************/

	/***** OR�AMENTO - EMISS�O RELAT�RIOS *****/

	public String get_urlPecasEmissaoRelatorios() {
		return URL_PECAS_EMISSAO_RELATORIOS;
	}

	public String get_urlServicosEmissaoRelatorios() {
		return URL_SERVICOS_EMISSAO_RELATORIOS;
	}

	public String getUrlOrdenarPorDescricaoEmissaoRelatorios() {
		return URL_ORDENAR_POR_DESC_EMISSAO_RELATORIOS;
	}

	public String getUrlSalvadoEmissaoRelatorios() {
		return URL_SALVADO_EMISSAO_RELATORIOS;
	}

	public String getUrlPmgEmissaoRelatorios() {
		return URL_PMG_EMISSAO_RELATORIOS;
	}

	public String getUrlAcessoEmissaoRelatorios() {
		return URL_ACESSO_EMISSAO_RELATORIOS;
	}

	public String getUrlConnectEmissaoRelatorios() {
		return URL_CONNECT_EMISSAO_RELATORIOS;
	}

	public String getUrlAvariasEmissaoRelatorios() {
		return URL_AVARIAS_EMISSAO_RELATORIOS;
	}

	public String getUrlFotosAvariasEmissaoRelatorios() {
		return URL_FOTOS_AVARIAS_EMISSAO_RELATORIOS;
	}

	public String getUrlHistoricoEmissaoRelatorios() {
		return URL_HISTORICO_EMISSAO_RELATORIOS;
	}

	public String getUrlRevisaoEmissaoRelatorios() {
		return URL_REVISAO_EMISSAO_RELATORIOS;
	}

	/*****************************/

	public String get_pecasAbsImpactos() {
		return _PECA_ABS_IMPACTOS;
	}

	public String get_encerramentoSinistro() {
		return _PRINT_ENCERRAMENTO_SINISTRO;
	}

	public String get_situacaoSinistroEsperado() {
		return _SITUACAO_SINISTRO_ESPERADO;
	}

	/*** M�o-De-Obra (Bot�o) ***/

	public String get_valorMecanicaMaoDeObra() {
		return dadosAle.get_valorMecanicaMaoDeObra();
	}

	public String get_valorFunilariaMaoDeObra() {
		return dadosAle.get_valorFunilariaMaoDeObra();
	}

	public String get_valorPinturaMaoDeObra() {
		return dadosAle.get_valorPinturaMaoDeObra();
	}

	public String get_valorEletricaMaoDeObra() {
		return dadosAle.get_valorEletricaMaoDeObra();
	}

	public String get_valorTapecariaMaoDeObra() {
		return dadosAle.get_valorTapecariaMaoDeObra();
	}

	public String get_valorVidracariaMaoDeObra() {
		return dadosAle.get_valorVidracariaMaoDeObra();
	}

	public String get_valorReparacaoMaoDeObra() {
		return dadosAle.get_valorReparacaoMaoDeObra();
	}

	/*** Laudo Pmg ***/

	public String get_linhaColunaDanificadaLaudoPmg() {
		return "ctl00_cph_Form_Botao_" + dadosAle.get_numeroLinhaLaudoPmg() + "_1_1_1_1_9";//"_1_1_1_1_8"; //"_1_1_1_1_7"; // "_1_1_1_1_6";//"_1_1_1_1_2";
																							// // 1
																							// //ctl00_cph_Form_Botao_138_1_1_1_1_6
	}

	public String get_linhaColunaDanificadaLaudoPmgTOKIO() {
		return "ctl00_cph_Form_Botao_" + dadosAle.get_numeroLinhaLaudoPmg() + "_1_1_1_1_4";//"_1_1_1_1_3";// "_1_1_1_1_1";
	}

	// Fechar Este Sinistro

	public String getUrlJanelaPosFecharEsteSinistro() {
		return URL_JANELA_POS_FECHAR_ESTE_SINISTRO;
	}

	/*****************/

	public String get_printDadosVeiculo() {
		return _PRINT_DADOS_VEICULO;
	}

	public String get_printPreOrcamento() {
		return _PRINT_PRE_ORCAMENTO;
	}

	public String get_printOrcamentoBaremo() {
		return _PRINT_ORCAMENTO_BAREMO;
	}

	public String get_printOrcamentoManualSubstituicao() {
		return _PRINT_ORCAMENTO_MANUAL_SUBSTITUICAO;
	}

	public String get_printOrcamentoManualReparacao() {
		return _PRINT_ORCAMENTO_MANUAL_REPARACAO;
	}

	public String getPrintOrcamentoManualPintura() {
		return PRINT_ORCAMENTO_MANUAL_PINTURA;
	}

	public String getPrintOrcamentoAbaAdicionais() {
		return PRINT_ABA_ADICIONAIS;
	}

	public String get_printBotaoFotos() {
		return _PRINT_BOTAO_FOTOS;
	}

	public String get_printListasPecas() {
		return _PRINT_LISTA_PECAS;
	}

	public String getPrintCarroceriaLaudoDeSalvados() {
		return _PRINT_CARROCERIA_LAUDO_DE_SALVADOS;
	}

	public String getPrintVidrosLaudoDeSalvados() {
		return _PRINT_VIDROS_LAUDO_DE_SALVADOS;
	}

	public String getPrintAcessoriosLaudoDeSalvados() {
		return _PRINT_ACESSORIOS_LAUDO_DE_SALVADOS;
	}

	public String getPrintPneusRodaLaudoDeSalvados() {
		return _PRINT_PNEUS_RODA_LAUDO_DE_SALVADOS;
	}

	public String getPrintInstalacaoEletricaLaudoDeSalvados() {
		return _PRINT_INSTALACAO_ELETRICA_LAUDO_DE_SALVADOS;
	}

	public String getPrintMecanicaLaudoDeSalvados() {
		return _PRINT_MECANICA_LAUDO_DE_SALVADOS;
	}

	public String get_printOrcamentoFotos() {
		return _PRINT_ORCAMENTO_FOTOS;
	}

	public String get_printPerfilAnalista() {
		return _PRINT_PERFIL_ANALISTA;
	}

	public String get_printPecaEmissaoRelatorios() {
		return _PRINT_PECA_EMISSAO_RELATORIOS;
	}

	public String get_printServicoEmissaoRelatorios() {
		return _PRINT_SERVICO_EMISSAO_RELATORIOS;
	}

	public String get_printDetalhes() {
		return _PRINT_DETALHES;
	}

	public String get_printIdentificacaoSinistro() {
		return _PRINT_IDENTIFICACAO_SINISTRO;
	}

	public String get_printMaoDeObra() {
		return _PRINT_MAO_DE_OBRA;
	}

	public String get_printEncerramentoSinistro() {
		return _PRINT_ENCERRAMENTO_SINISTRO;
	}

	public String getPrintOutrosValores() {
		return PRINT_OUTROS_VALORES;
	}

	public String getPrintOrdenarPorDescricaoEmissaoDeRelatorios() {
		return PRINT_ORDENAR_POR_DESCRICAO_EMISSAO_DE_RELATORIOS;
	}

	public String getPrintJanelaPosFecharEsteSinistro() {
		return PRINT_JANELA_POS_FECHAR_ESTE_SINISTRO;
	}

	public String getPrintLaudoPmg() {
		return PRINT_LAUDO_PMG;
	}

	public String getPrintLaudoPmgObservacao() {
		return PRINT_LAUDO_PMG_OBSERVACAO;
	}

	public String getPrintPecaEmissaoDeRelatorios() {
		return PRINT_PECA_EMISSAO_DE_RELATORIOS;
	}

	public String getPrintSalvadoEmissaoDeRelatorios() {
		return PRINT_SALVADO_EMISSAO_DE_RELATORIOS;
	}

	public String getPrintPmgEmissaoDeRelatorios() {
		return PRINT_PMG_EMISSAO_DE_RELATORIOS;
	}

	public String getPrintFotoEmissaoDeRelatorios() {
		return PRINT_FOTO_EMISSAO_DE_RELATORIOS;
	}

	public String getPrintHistoricoEmissaoDeRelatorios() {
		return PRINT_HISTORICO_EMISSAO_DE_RELATORIOS;
	}

	public String getPrintRevisaoEmissaoDeRelatorios() {
		return PRINT_REVISAO_EMISSAO_DE_RELATORIOS;
	}

	public String getPrintAcessoEmissaoDeRelatorios() {
		return PRINT_ACESSO_EMISSAO_DE_RELATORIOS;
	}

	public String getPrintConnectEmissaoDeRelatorios() {
		return PRINT_CONNECT_EMISSAO_DE_RELATORIOS;
	}

	public String getPrintAvariasEmissaoDeRelatorios() {
		return PRINT_AVARIAS_EMISSAO_DE_RELATORIOS;
	}

	public String getPrintFotosAvariasEmissaoDeRelatorios() {
		return PRINT_FOTOS_AVARIAS_EMISSAO_DE_RELATORIOS;
	}
}
