package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.BotaoFotosPage;

public class BotaoFotosStep extends BaseTeste {

	private BotaoFotosPage page = new BotaoFotosPage();
	private BaseAction baseAc = new BaseAction();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();

	/**
	 * String respons�vel por armazenar a identifica��o da janela principal.
	 */

	private static String janelaFotos;

	/**
	 * Atribui a identifica��o da janela principal para o atributo janelasFotos e
	 * clica no bot�o 'Fotos'.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@And("^Clica no botao Fotos - \"([^\"]*)\"$")
	public void botaoFotos(String fluxo) throws Exception {

		try {

			BotaoFotosStep.janelaFotos = page.pegaIdJanela();
			page.clicaFotosBotao();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO FOTOS", e);
		}
	}

	/**
	 * Tira um print da janela de fotos.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@And("^Verifica a foto adicionada em Fotos - \"([^\"]*)\"$")
	public void verificaFoto(String fluxo) throws Exception {

		try {

			page.esperaAjaxEIcone();

			page.trocaJanelaFotos();

			Thread.sleep(300);

			BaseAction.tirarPrint(varStep.get_printBotaoFotos());

			baseAc.escreverLog("FOTO VERIFICADA COM SUCESSO NA JANELA DO BOT�O FOTOS EM TELA DE OR�AMENTO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO FOTOS", e);
		}
	}

	/**
	 * Fecha a segunda janela.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Fecha Fotos - \"([^\"]*)\"$")
	public void fechaFotos(String fluxo) throws Exception {

		try {

			page.fechaSegundaTela(janelaFotos);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO FOTOS", e);
		}
	}
}
