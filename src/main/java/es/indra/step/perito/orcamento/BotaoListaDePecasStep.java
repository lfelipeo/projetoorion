package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.BotaoListaDePecasPage;

public class BotaoListaDePecasStep extends BaseTeste {

	private BotaoListaDePecasPage page = new BotaoListaDePecasPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();
	private BaseAction baseAc = new BaseAction();

	/**
	 * Clica no bot�o 'Lista de Pe�as' na tela de Or�amento.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Clica no botao Lista de Pecas na tela Orcamento - \"([^\"]*)\"$")
	public void botaoListaDePecas(String fluxo) throws Exception {

		try {

			page.abreListaDePecas();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO LISTA DE PECAS", e);
		}
	}

	@And("^Verifica a peca adicionada na aba Baremo em Lista de Pecas na tela Orcamento - \"([^\"]*)\"$")
	public void verificaPecaBaremo(String fluxo) throws Exception {

		try {

			if (AbaBaremoStep.pecaAdicionada) {

				page.verificaPecaBaremo(AbaBaremoStep.pecaBaremo);

				baseAc.escreverLog(
						"PE�AS ADICIONADAS NA ABA BAREMO VERIFICADAS COM SUCESSO EM LISTA DE PE�AS NA TELA DE OR�AMENTO - "
								+ fluxo);
			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO AO VERIFICAR AS PE�AS ADICIONADAS NA ABA BAREMO EM LISTA DE PE�AS", e);
		}

	}

	@And("^Verifica a peca adicionada em Substituicao na aba Manual em Lista de Pecas na tela Orcamento - \"([^\"]*)\"$")
	public void verificaPecaSubstituicaoAbaManual(String fluxo) throws Exception {

		if (AbaManualStep.condSubstituicao)
			page.verificaPecaManual(AbaManualStep.pecaSubstituicao);
	}

	@And("^Verifica a peca adicionada em Reparacao na aba Manual em Lista de Pecas na tela Orcamento - \"([^\"]*)\"$")
	public void verificaPecaReparacaoAbaManual(String fluxo) throws Exception {

		if (AbaManualStep.condReparacao)
			page.verificaPecaManual(AbaManualStep.pecaReparacao);
	}

	@And("^Verifica a peca adicionada em Pintura na aba Manual em Lista de Pecas na tela Orcamento - \"([^\"]*)\"$")
	public void verificaPecaPinturaAbaManual(String fluxo) throws Exception {

		if (AbaManualStep.condPintura)
			page.verificaPecaManual(AbaManualStep.pecaPintura);
	}

	@And("^Verifica as pecas adicionadas na aba manual em Lista de Pecas na tela Orcamento - \"([^\"]*)\"$")
	public void verificaPecasManual(String fluxo) throws Exception {

		try {

			page.verificaPecaManual(varStep.get_partNumber());

			BaseAction.tirarPrint(varStep.get_printListasPecas());

			baseAc.escreverLog(
					"PE�AS ADICIONADAS NA ABA MANUAL VERIFICADAS COM SUCESSO EM LISTA DE PE�AS NA TELA DE OR�AMENTO - "
							+ fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO AO VERIFICAR AS PE�AS ADICIONADAS NA ABA MANUAL EM LISTA DE PE�AS", e);
		}
	}

	/**
	 * Clica em fechar e verifica se a tela de Or�amento est� vis�vel.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Then("^Fecha Lista de Pecas - \"([^\"]*)\"$")
	public void fechaListaDePecas(String fluxo) throws Exception {

		try {

			page.fecharListaDePecasOrcamento();
			page.verificarTelaOrcamentoVisivel();

			System.out.println("Pe�a Baremo: " + AbaBaremoStep.descPecaBaremo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO AO FECHAR LISTA DE PE�AS", e);
		}
	}

}
