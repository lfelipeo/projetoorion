package es.indra.step.perito.orcamento;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.orcamento.testclass.BotaoDadosDoVeiculoPage;

public class BotaoDadosDoVeiculoStep extends BaseTeste {

	private BotaoDadosDoVeiculoPage page = new BotaoDadosDoVeiculoPage();
	private OrcamentoStepVar varStep = new OrcamentoStepVar();

	private BaseAction baseAc = new BaseAction();

	/***************** Dados de Veiculo (Bot�o) *****************/

	@Then("^Clica no botao Dados do Veiculo na tela de Orcamento - \"([^\"]*)\"$")
	public void botaoDadosDoVeiculo(String fluxo) throws Exception {

		try {

			// Clica no bot�o 'Dados Veiculo' na tela de Or�amento
			page.clicaBotaoDadosDoVeiculo();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO DADOS DO VEICULO", e);
		}
	}

	@And("^Altera os opcionais em Dados do Veiculo na tela de Orcamento - \"([^\"]*)\"$")
	public void alteraOpcionaisDadosDoVeiculo(String fluxo) throws Exception {

		try {

			// Altera/Adiciona opcionais na tela Dados Veiculo
			page.alterarOpcionaisDadosVeiculoBotao(3);

			// Tirar o print
			BaseAction.tirarPrint(varStep.get_printDadosVeiculo());

			baseAc.escreverLog("OPCIONAIS ALTERADOS COM SUCESSO EM DADOS DO VEICULO NA TELA DE ORCAMENTO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO DADOS DO VEICULO", e);
		}
	}

	@Then("^Confirmar Dados do Veiculo na tela de Orcamento - \"([^\"]*)\"$")
	public void confirmaDadosDoVeiculo(String fluxo) {

		try {

			page.clicaConfirmar();

			page.verificaTelaOrcamentoPosConfirmar();

			baseAc.escreverLog("DADOS DO VEICULO CONFIRMADO COM SUCESSO NA TELA DE ORCAMENTO - " + fluxo);

		} catch (Exception e) {

		}

	}

	/************************************************/

}
