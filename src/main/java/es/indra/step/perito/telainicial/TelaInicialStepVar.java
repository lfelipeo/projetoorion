package es.indra.step.perito.telainicial;

import es.indra.atributos.TestAttributes;

public class TelaInicialStepVar {

	private TestAttributes testAt = new TestAttributes();

	private final String _PRINT_TELA_INICIAL = "Tela_Inicial";
	private final String PRINT_LOCALIZADOR = "Localizador_Pesquisa";

	/***** INTERFACE *****/

	public String getSeguradora() {
		return testAt.getSeguradora();
	}

	public String getElementoBusca() {
		return testAt.getElementoBusca();
	}

	public String getColunaRef() {
		return testAt.getTipoPesquisa();
	}

	/********************/

	/****** PRINTS ******/

	public String get_printTelaInicial() {
		return _PRINT_TELA_INICIAL;
	}

	public String getPrintLocalizador() {
		return PRINT_LOCALIZADOR;
	}

	/*******************/

}
