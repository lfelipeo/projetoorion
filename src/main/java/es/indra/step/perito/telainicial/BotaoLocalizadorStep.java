package es.indra.step.perito.telainicial;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.telainical.BotaoLocalizadorPage;

public class BotaoLocalizadorStep extends BaseTeste {

	private TelaInicialStepVar varStep = new TelaInicialStepVar();
	private BotaoLocalizadorPage page = new BotaoLocalizadorPage();
	private BaseAction baseAc = new BaseAction();

	@Then("^Clica no botao Localizador - \"([^\"]*)\"$")
	public void localizador(String fluxo) throws Exception {

		try {
			page.clicaLocalizador();
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO LOCALIZADOR", e);
		}

	}

	@And("^Seleciona a opcao \"([^\"]*)\" em Criterio de Consulta em Localizador - \"([^\"]*)\"$")
	public void selecionaOpcaoCriterioDeConsulta(String opcaoASerSelecionada, String fluxo) throws Exception {

		try {

			page.selecionaCriterioDeConsulta(opcaoASerSelecionada);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO LOCALIZADOR", e);
		}
	}

	@And("^Preenche a Placa e pesquisa em Criterio de Consulta em Localizador - \"([^\"]*)\"$")
	public void escrevePlaca(String fluxo) throws Exception {

		try {

			page.preencheElementoDeConsulta(varStep.getElementoBusca());

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO LOCALIZADOR", e);
		}

		try {
			page.clicaLupaDentroLocalizador();
		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO LOCALIZADOR", e);
		}

	}

	@And("^Valida se o elemento pesquisado esta com a situacao \"([^\"]*)\" em Localizador - \"([^\"]*)\"$")
	public void validaSituacao(String situacao, String fluxo) throws Exception {

		try {

			page.verificaPlacaPesquisada(varStep.getElementoBusca());
			page.verificaSituacao(situacao);

			BaseAction.tirarPrint(varStep.getPrintLocalizador());
			baseAc.escreverLog("SITUACAO DO SINISTRO VERIFICADA NO LOCALIZADOR E CONSTA COMO REALIZADO - " + fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO LOCALIZADOR", e);
		}
	}

	@Then("^Fecha Localizador - \"([^\"]*)\"$")
	public void fechaLocalizador(String fluxo) throws Exception {

		try {

			page.fechaLocalizador();

		} catch (Exception e) {
			baseAc.logarErro("ERRO NO BOTAO LOCALIZADOR", e);
		}
	}

}
