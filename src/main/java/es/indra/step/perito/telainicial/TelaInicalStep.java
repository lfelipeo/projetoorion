package es.indra.step.perito.telainicial;

import cucumber.api.java.en.Given;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.telainical.TelaInicialPage;

/**
 * 
 * @author LUIZ FELIPE OLIVEIRA
 * 
 *         Classe respons�vel por todas as a��es STEP na tela inicial do sistema
 *         Orion.
 *
 */

public class TelaInicalStep extends BaseTeste {

	private TelaInicialStepVar varStep = new TelaInicialStepVar();
	private TelaInicialPage page = new TelaInicialPage();
	private BaseAction baseAc = new BaseAction();

	@Given("^Que login foi efetuado seleciona o sinistro - \"([^\"]*)\"$")
	public void telaInicialOrion(String fluxo) throws Exception {

		try {

			// Tira print da tela inical
			BaseAction.tirarPrint(varStep.get_printTelaInicial());

			// Seleciona o Sinistro que ser� usado
			page.clicarBtnTabela(varStep.getElementoBusca(), varStep.getColunaRef());

			// Escreve o log
			baseAc.escreverLog("SINISTRO ENCONTRADO E SELECIONADO COM SUCESSO NA TELA INICIAL - " + fluxo);
			
		} catch (Exception e) {
			baseAc.logarErro("ERRO NA TELA INICIAL", e);
		}
	}

}
