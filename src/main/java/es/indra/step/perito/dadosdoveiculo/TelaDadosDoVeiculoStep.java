package es.indra.step.perito.dadosdoveiculo;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.indra.base.BaseAction;
import es.indra.base.BaseTeste;
import es.indra.page.perito.dadosdoveiculo.TelaDadosDeVeiculoPage;

public class TelaDadosDoVeiculoStep extends BaseTeste {

	private BaseAction baseAc = new BaseAction();
	private TelaDadosDoVeiculoStepVar varStep = new TelaDadosDoVeiculoStepVar();
	private TelaDadosDeVeiculoPage page = new TelaDadosDeVeiculoPage();

	private static boolean cond;

	@Then("^Verifica se a tela Dados do Veiculo esta visivel - \"([^\"]*)\"$")
	public void dadosDoVeiculo(String fluxo) throws Exception {

		try {

			page.waitAjax();

			cond = page.verificarDadosVeiculoVisivel();
			voltaTempoDeEspera();

			System.out.println("Tela Dados do Veiculo visivel: " + cond);

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA TELA DE DADOS DO VEICULO", e);
		}
	}

	@And("^Preenche as informacoes do Veiculo na tela Dados do Veiculo - \"([^\"]*)\"$")
	public void informacoesVeiculo(String fluxo) throws Exception {

		try {

			if (cond) {

				page.selecionaMarca(varStep.get_fabCarro());

				page.selecionaModelo(varStep.get_modCarro());

				page.selecionaCor(varStep.get_corCarro());

				page.preencheQuilometragem(varStep.get_qtnQuilometragemCarro());

				page.preencheValorDeMercado(varStep.get_qtnValorCarro());

				baseAc.escreverLog(
						"INFORMAÇÕES DO VEICULO PREENCHIDAS/SELECIONADAS COM SUCESSO NA TELA DE DADOS DO VEICULO - "
								+ fluxo);
			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA TELA DE DADOS DO VEICULO", e);
		}

	}

	@And("^Seleciona os Opcionais na tela Dados do Veiculo - \"([^\"]*)\"$")
	public void selecionaOpcionais(String fluxo) throws Exception {

		try {

			if (cond) {

				page.selecionaOpcionais(3);

				baseAc.escreverLog("OPCIONAIS SELECIONADOS COM SUCESSO NA TELA DE DADOS DO VEICULO - " + fluxo);

			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA TELA DE DADOS DO VEICULO", e);
		}
	}

	@Then("^Clica Confirmar na tela Dados do Veiculo - \"([^\"]*)\"$")
	public void clicaConfirmar(String fluxo) throws Exception {

		try {

			if (cond) {

				BaseAction.tirarPrint(varStep.get_printDadosVeiculo());

				page.clicaConfirmar();

			}

		} catch (Exception e) {
			baseAc.logarErro("ERRO NA TELA DE DADOS DO VEICULO", e);
		}
	}
}
