package es.indra.step.perito.dadosdoveiculo;

import es.indra.framework.GerarDadosAleatorio;

public class TelaDadosDoVeiculoStepVar {

	private GerarDadosAleatorio dadosAle = new GerarDadosAleatorio();

	/***** VAR / CONST *****/

	private final String _PRINT_DADOS_VEICULO = "Dados_Veiculo";

	/***** GETTERS *****/

	public String get_fabCarro() {
		return dadosAle.get_fabCarro();
	}

	public String get_modCarro() {
		return dadosAle.get_modCarro();
	}

	public String get_corCarro() {
		return dadosAle.get_corCarro();
	}

	public String get_qtnQuilometragemCarro() {
		return dadosAle.get_quilometragemCarro();
	}

	public String get_qtnValorCarro() {
		return dadosAle.get_valorCarro();
	}

	public String get_printDadosVeiculo() {
		return _PRINT_DADOS_VEICULO;
	}
}
