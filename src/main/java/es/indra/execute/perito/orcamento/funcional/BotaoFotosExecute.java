package es.indra.execute.perito.orcamento.funcional;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {
		"src\\main\\resources\\feature\\funcional\\BotaoFotos.feature" }, plugin = {
				"json:target/BotaoFotos.json",
				"html:target/cucumber" }, glue = { "" }, monochrome = true, dryRun = false)

public class BotaoFotosExecute {

}
