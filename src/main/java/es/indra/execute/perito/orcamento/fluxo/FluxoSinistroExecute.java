package es.indra.execute.perito.orcamento.fluxo;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "src\\main\\resources\\feature\\fluxo\\FluxoSinistro.feature" }, plugin = {
		"json:target/FluxoSinistro.json", "html:target/cucumber" }, glue = { "" }, monochrome = true, dryRun = false)

public class FluxoSinistroExecute {

}
