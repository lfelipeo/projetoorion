package es.indra.execute.perito.orcamento.funcional;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {
		"src\\main\\resources\\feature\\funcional\\BotaoOutrosValores.feature" }, plugin = {
				"json:target/AbaOutrosValores.json",
				"html:target/cucumber" }, glue = { "" }, monochrome = true, dryRun = false)

public class BotaoOutrosValoresExecute {

}
