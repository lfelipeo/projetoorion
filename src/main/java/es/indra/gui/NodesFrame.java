package es.indra.gui;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;

public class NodesFrame {

	public Label createLabel(String texto, String font, int tamanho_font, int layX, int layY) {
		Label lb = new Label(texto);
		lb.setFont(new Font(font, tamanho_font));
		lb.setLayoutX(layX);
		lb.setLayoutY(layY);
		return lb;
	}

	public TextField createTextField(int prefWidth, int prefHeight, int layX, int layY, String text) {
		TextField txf = new TextField();
		txf.setPrefSize(prefWidth, prefHeight);
		txf.setLayoutX(layX);
		txf.setLayoutY(layY);
		txf.setText(text);
		txf.getStyleClass().add("Txf");
		return txf;
	}

	public PasswordField createPasswordField(int prefWidth, int prefHeight, int layX, int layY, String text) {
		PasswordField psf = new PasswordField();
		psf.setPrefSize(prefWidth, prefHeight);
		psf.setLayoutX(layX);
		psf.setLayoutY(layY);
		psf.setText(text);
		psf.getStyleClass().add("Txf");
		return psf;
	}

	public Button createButton(String text, int prefWidth, int prefHeight, int layX, int layY) {
		Button btn = new Button(text);
		btn.setPrefSize(prefWidth, prefHeight);
		btn.setLayoutX(layX);
		btn.setLayoutY(layY);
		return btn;
	}

	public CheckBox createCheckBox(int prefWidth, int prefHeight, int layX, int layY) {
		CheckBox cb = new CheckBox();
		cb.setPrefSize(prefWidth, prefHeight);
		cb.setLayoutX(layX);
		cb.setLayoutY(layY);
		return cb;
	}

	public ComboBox<String> createComboBox(int prefWidth, int prefHeight, int layX, int layY) {
		ComboBox<String> comboB = new ComboBox<>();
		comboB.setPrefSize(prefWidth, prefHeight);
		comboB.setLayoutX(layX);
		comboB.setLayoutY(layY);
		return comboB;
	}

	public RadioButton createRadioButton(int prefWidth, int prefHeight, int layX, int layY) {
		RadioButton rb = new RadioButton();
		rb.setPrefSize(prefWidth, prefHeight);
		rb.setLayoutX(layX);
		rb.setLayoutY(layY);
		return rb;
	}

	public void addItemComboBox(ComboBox<String> cb, String item) {
		cb.getItems().add(item);
	}

	public ListView<String> createListView(int prefWidth, int prefHeight, int layX, int layY) {
		ListView<String> list = new ListView<>();
		list.setPrefSize(prefWidth, prefHeight);
		list.setLayoutX(layX);
		list.setLayoutY(layY);
		return list;
	}

	public ToggleGroup createToggleGroup() {
		ToggleGroup tg = new ToggleGroup();
		return tg;
	}

	public TabPane createTabPane() {
		TabPane tbp = new TabPane();
		return tbp;
	}
	
	public ImageView createImageIcon(String url_img) {
		ImageView imgIcon = new ImageView();
		Image img = new Image(getClass().getResourceAsStream(url_img));
		imgIcon.setImage(img);
		return imgIcon;
	}
}
