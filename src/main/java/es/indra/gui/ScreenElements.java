package es.indra.gui;

import javax.swing.JOptionPane;

import es.indra.atributos.JsonAttributes;
import es.indra.atributos.TestAttributes;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;

public class ScreenElements {

	private NodesFrame node = new NodesFrame();
	private VariaveisFrame var = new VariaveisFrame();
	private Suport sup = new Suport();

	private JsonAttributes loginAt = new JsonAttributes();
	private TestAttributes testAt = new TestAttributes();

	public static boolean condStartTest;

	// Label
	private Label lbLogin;
	private Label lbSenha;
	private Label lbSalvarInfo;
	private Label lbCenarioSucesso;
	private Label lbCenarioErro;

	// Text/Password Field
	private TextField txfLogin;
	private TextField txfElementoBusca;
	private PasswordField psfSenha;

	// Bot�o
	private Button btnConfirm;
	private Button btnCancel;

	// Checkbox
	private CheckBox cb;

	// Combobox
	private ComboBox<String> comboPerfilConta;
	private ComboBox<String> comboTipoPesquisa;
	private ComboBox<String> comboSeguradora;
	private ComboBox<String> comboTipoTeste;

	// Listview
	private ListView<String> listTestes;

	// ObservableList
	private ObservableList<String> itensFluxosPeritoSucesso;
	private ObservableList<String> itensFluxosPeritoErro;
	private ObservableList<String> itensFluxosAnalista;

	private ObservableList<String> unitariosPeritoSucesso;
	private ObservableList<String> unitariosPeritoErro;

	// Radio
	private RadioButton rbCenarioSucesso;
	private RadioButton rbCenarioErro;
	private ToggleGroup tgCenarioTeste;

	/*********************************************************/
	/******************** GETTERS - NODE ********************/

	// Label

	public Label getLbLogin() {
		lbLogin = node.createLabel("LOGIN", "Arial", 20, 353, 175);
		lbLogin.getStyleClass().add("Labels");
		return lbLogin;
	}

	public Label getLbSenha() {
		lbSenha = node.createLabel("SENHA", "Arial", 20, 353, 220);
		lbSenha.getStyleClass().add("Labels");
		return lbSenha;
	}

	public Label getLbSalvarInfo() {
		lbSalvarInfo = node.createLabel("Salvar Informa��es?", "Arial", 12, 410, 261);
		lbSalvarInfo.setId("lblSalvar");
		return lbSalvarInfo;
	}

	public Label getLbCenarioSucesso() {
		// lbCenarioSucesso = node.createLabel("Sucesso", "Arial", 12, 40, 60);
		lbCenarioSucesso = node.createLabel("Sucesso", "Arial", 12, 40, 86);
		lbCenarioSucesso.getStyleClass().add("LabelsTipoTeste");
		return lbCenarioSucesso;
	}

	public Label getLbCenarioErro() {
		// lbCenarioErro = node.createLabel("Erro", "Arial", 12, 40, 80);
		lbCenarioErro = node.createLabel("Erro", "Arial", 12, 40, 106);
		lbCenarioErro.getStyleClass().add("LabelsTipoTeste");
		return lbCenarioErro;
	}

	// Text/Password Field

	public TextField getTxfLogin() {
		try {
			txfLogin = node.createTextField(120, 5, 420, 175, loginAt.getLoginJson());
			txfLogin.setOnMousePressed((MouseEvent event) -> txfLogin.clear());
			return txfLogin;
		} catch (Exception e) {
			return null;
		}
	}

	public PasswordField getPsfSenha() {
		try {
			psfSenha = node.createPasswordField(120, 5, 420, 220, loginAt.getSenhaJson());
			psfSenha.setOnMousePressed((MouseEvent event) -> psfSenha.clear());
			return psfSenha;
		} catch (Exception e) {
			return null;
		}
	}

	public TextField getTxfElementoBusca() {

		txfElementoBusca = node.createTextField(120, 5, 420, 100, null);
		txfElementoBusca.setEditable(false);
		txfElementoBusca.setOnMousePressed((MouseEvent event) -> txfElementoBusca.clear());
		return txfElementoBusca;
	}

	// Button

	public Button getBtnConfirm() {

		btnConfirm = node.createButton("Confirmar", 69, 25, 390, 285);
		btnConfirm.getStyleClass().add("Btns");
		btnConfirm.setOnAction((ActionEvent event) -> {

			ScreenElements.condStartTest = true;

			testAt.setLogin(txfLogin.getText());
			testAt.setSenha(psfSenha.getText());
			testAt.setElementoBusca(txfElementoBusca.getText());
			testAt.setSeguradora(comboSeguradora.getSelectionModel().getSelectedItem());
			testAt.setTipoPesquisa(
					var.get_tipoPesquisaComboPesquisa(comboTipoPesquisa.getSelectionModel().getSelectedIndex()));

			if (cb.isSelected())
				sup.salvaInfoLogin("Info_Login", testAt.getLogin(), testAt.getSenha());

			if (condStartTest) {
				String tipoTeste = comboTipoTeste.getSelectionModel().getSelectedItem();

				if (tipoTeste.equalsIgnoreCase("Fluxo")) {
					sup.inicia(comboPerfilConta, comboTipoTeste, listTestes, rbCenarioSucesso, rbCenarioErro);
				} else if (tipoTeste.equalsIgnoreCase("Unitario")) {
					sup.inicia(comboPerfilConta, comboTipoTeste, listTestes, rbCenarioSucesso, rbCenarioErro);
				}
			}
		});

		return btnConfirm;
	}

	public Button getBtnCancel() {
		btnCancel = node.createButton("Cancelar", 68, 25, 470, 285);
		btnCancel.getStyleClass().add("Btns");
		btnCancel.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				if (JOptionPane.showConfirmDialog(null, "Tem certeza?", "Aviso",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					System.exit(0);
				} else {
				}
			}
		});
		return btnCancel;
	}

	// Checkbox

	public CheckBox getCb() {
		cb = node.createCheckBox(5, 5, 523, 260);
		cb.setId("cbGravar");
		return cb;
	}

	// Combo

	public ComboBox<String> getComboPerfilConta() {

		// comboPerfilConta = node.createComboBox(120, 30, 100, 63);
		comboPerfilConta = node.createComboBox(200, 33, 20, 48);
		comboPerfilConta.getStyleClass().add("Combos");

		node.addItemComboBox(comboPerfilConta, var.get_contaPerito());
		node.addItemComboBox(comboPerfilConta, var.get_contaAnalista());

		comboPerfilConta.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				String tipoConta = comboPerfilConta.getSelectionModel().getSelectedItem();
				String tipoTeste = comboTipoTeste.getSelectionModel().getSelectedItem();

				try {

					if (tipoTeste.equalsIgnoreCase("Fluxo")) {

						if (tipoConta.equalsIgnoreCase(var.get_contaPerito())) {

							if (rbCenarioSucesso.isSelected()) {
								listTestes.setItems(itensFluxosPeritoSucesso);
							} else if (rbCenarioErro.isSelected()) {
								listTestes.setItems(itensFluxosPeritoErro);
							}

						} else if (tipoConta.equalsIgnoreCase(var.get_contaAnalista())) {
							if (rbCenarioSucesso.isSelected()) {
								listTestes.setItems(itensFluxosAnalista);
							} else if (rbCenarioErro.isSelected()) {
								listTestes.setItems(null);
							}

						}
					}

					if (tipoTeste.equalsIgnoreCase("Unitario")) {

						if (tipoConta.equalsIgnoreCase(var.get_contaPerito())) {

							if (rbCenarioSucesso.isSelected()) {
								listTestes.setItems(getUnitariosPeritoSucesso());

							} else if (rbCenarioErro.isSelected()) {
								listTestes.setItems(null);
							}

						} else if (tipoConta.equalsIgnoreCase(var.get_contaAnalista())) {
							if (rbCenarioSucesso.isSelected()) {
								listTestes.setItems(null);
							} else if (rbCenarioErro.isSelected()) {
								listTestes.setItems(null);
							}

						}

					}
				} catch (Exception e) {

				}
			}
		});

		return comboPerfilConta;
	}

	public ComboBox<String> getComboSeguradora() {
		comboSeguradora = node.createComboBox(120, 30, 420, 135);
		comboSeguradora.getStyleClass().add("Combos");
		node.addItemComboBox(comboSeguradora, var.get_seguradoraMitsui());
		node.addItemComboBox(comboSeguradora, var.get_seguradoraTokio());
		return comboSeguradora;
	}

	public ComboBox<String> getComboTipoPesquisa() {
		comboTipoPesquisa = node.createComboBox(120, 30, 420, 63);
		comboTipoPesquisa.getStyleClass().add("Combos");
		node.addItemComboBox(comboTipoPesquisa, var.get_colunaRefPlaca());
		comboTipoPesquisa.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				txfElementoBusca.setEditable(true);

				if (comboTipoPesquisa.getSelectionModel().getSelectedIndex() == 0) {
					txfElementoBusca.clear();
					txfElementoBusca.setText("Digite uma Placa");
				}

				else if (comboTipoPesquisa.getSelectionModel().getSelectedIndex() == 1) {
					txfElementoBusca.clear();
					txfElementoBusca.setText("Digite um Chassi");
				}
			}
		});

		return comboTipoPesquisa;
	}

	public ComboBox<String> getComboTipoTeste() {

		comboTipoTeste = node.createComboBox(120, 30, 100, 88);

		comboTipoTeste.getStyleClass().add("Combos");

		node.addItemComboBox(comboTipoTeste, "Fluxo");
		node.addItemComboBox(comboTipoTeste, "Unitario");

		comboTipoTeste.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				String tipoConta = comboPerfilConta.getSelectionModel().getSelectedItem();
				String tipoTeste = comboTipoTeste.getSelectionModel().getSelectedItem();

				try {

					if (tipoTeste.equalsIgnoreCase("Fluxo")) {

						if (tipoConta.equalsIgnoreCase(var.get_contaPerito())) {

							if (rbCenarioSucesso.isSelected()) {
								listTestes.setItems(itensFluxosPeritoSucesso);
							} else if (rbCenarioErro.isSelected()) {
								listTestes.setItems(itensFluxosPeritoErro);
							}

						} else if (tipoConta.equalsIgnoreCase(var.get_contaAnalista())) {
							if (rbCenarioSucesso.isSelected()) {
								listTestes.setItems(null);
							} else if (rbCenarioErro.isSelected()) {
								listTestes.setItems(null);
							}

						}

					}

					else if (tipoTeste.equalsIgnoreCase("Unitario")) {

						if (tipoConta.equalsIgnoreCase(var.get_contaPerito())) {

							if (rbCenarioSucesso.isSelected()) {
								listTestes.setItems(getUnitariosPeritoSucesso());

							} else if (rbCenarioErro.isSelected()) {
								listTestes.setItems(null);
							}

						} else if (tipoConta.equalsIgnoreCase(var.get_contaAnalista())) {
							if (rbCenarioSucesso.isSelected()) {
								listTestes.setItems(null);
							} else if (rbCenarioErro.isSelected()) {
								listTestes.setItems(null);
							}

						}
					}

				} catch (Exception e) {

				}

			}
		});

		return comboTipoTeste;
	}

	// List

	public ListView<String> getListTestes() {
		listTestes = node.createListView(200, 190, 20, 130);
		listTestes.setId("listFluxos");
		return listTestes;
	}

	// ObservableList

	public ObservableList<String> getItensFluxosPeritoSucesso() {
		itensFluxosPeritoSucesso = FXCollections.observableArrayList(var.get_fluxoSinistroPerito(),
				var.getFluxoAPerito());
		return itensFluxosPeritoSucesso;
	}

	public ObservableList<String> getItensFluxosPeritoErro() {
		itensFluxosPeritoErro = FXCollections.observableArrayList();
		return itensFluxosPeritoErro;
	}

	public ObservableList<String> getItensFluxosAnalista() {
		itensFluxosAnalista = FXCollections.observableArrayList();
		return itensFluxosAnalista;
	}

	public ObservableList<String> getUnitariosPeritoSucesso() {

		unitariosPeritoSucesso = FXCollections.observableArrayList(var.getUnitarioPeritoOrcamentoSucessoAbAdicionais(),
				var.getUnitarioPeritoOrcamentoSucessoAbaBaremo(), var.getUnitarioPeritoOrcamentoSucessoAbaFotos(),
				var.getUnitarioPeritoOrcamentoSucessoAbaManual(), var.getUnitarioPeritoOrcamentoSucessoPreOrcamento(),
				var.getUnitarioPeritoOrcamentoSucessoBotaoOutrosValores(),
				var.getUnitarioPeritoOrcamentoSucessoBotaoDetalhes(),
				var.getUnitarioPeritoOrcamentoSucessoBotaoLaudoPmg(),
				var.getUnitarioPeritoOrcamentoSucessoBotaoLaudoDeSalvados(),
				var.getUnitarioPeritoOrcamentoSucessoBotaoMaoDeObra(),
				var.getUnitarioPeritoOrcamentoSucessoBotaoFotos());

		return unitariosPeritoSucesso;
	}

	public ObservableList<String> getUnitariosPeritoErro() {
		return unitariosPeritoErro;
	}

	// Radio

	public RadioButton getRbCenarioSucesso() {

		// rbCenarioSucesso = node.createRadioButton(10, 10, 20, 58);
		rbCenarioSucesso = node.createRadioButton(10, 10, 20, 85);
		rbCenarioSucesso.getStyleClass().add("Rb");
		rbCenarioSucesso.setToggleGroup(tgCenarioTeste);
		rbCenarioSucesso.setSelected(true);
		rbCenarioSucesso.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				String tipoConta = comboPerfilConta.getSelectionModel().getSelectedItem();
				String tipoTeste = comboTipoTeste.getSelectionModel().getSelectedItem();

				try {

					if (tipoTeste.equalsIgnoreCase("Fluxo")) {

						if ((tipoConta.equalsIgnoreCase(var.get_contaPerito())) && (rbCenarioSucesso.isSelected())) {
							listTestes.setItems(itensFluxosPeritoSucesso);
						}

						else if ((tipoConta.equalsIgnoreCase(var.get_contaAnalista()))
								&& (rbCenarioSucesso.isSelected())) {
							listTestes.setItems(itensFluxosAnalista);
						}
					} else if (tipoTeste.equalsIgnoreCase("Unitario")) {

						if ((tipoConta.equalsIgnoreCase(var.get_contaPerito())) && (rbCenarioSucesso.isSelected())) {
							listTestes.setItems(unitariosPeritoSucesso);
						}

						else if ((tipoConta.equalsIgnoreCase(var.get_contaAnalista()))
								&& (rbCenarioSucesso.isSelected())) {
							listTestes.setItems(unitariosPeritoErro);
						}
					}

				} catch (Exception e) {
				}
			}
		});
		return rbCenarioSucesso;
	}

	public RadioButton getRbCenarioErro() {
		// rbCenarioErro = node.createRadioButton(10, 10, 20, 78);
		rbCenarioErro = node.createRadioButton(10, 10, 20, 105);
		rbCenarioErro.getStyleClass().add("Rb");
		rbCenarioErro.setToggleGroup(tgCenarioTeste);
		rbCenarioErro.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				String tipoConta = comboPerfilConta.getSelectionModel().getSelectedItem();

				try {

					String tipoTeste = comboTipoTeste.getSelectionModel().getSelectedItem();

					if (tipoTeste.equalsIgnoreCase("Fluxo")) {
						if ((tipoConta.equalsIgnoreCase(var.get_contaPerito())) && (rbCenarioErro.isSelected())) {
							listTestes.setItems(itensFluxosPeritoErro);

						} else if ((tipoConta.equalsIgnoreCase(var.get_contaAnalista()))
								&& (rbCenarioErro.isSelected())) {
							listTestes.setItems(null);
						}
					}

					else if (tipoTeste.equalsIgnoreCase("Unitario")) {
						if ((tipoConta.equalsIgnoreCase(var.get_contaPerito())) && (rbCenarioErro.isSelected())) {
							listTestes.setItems(unitariosPeritoErro);
						}
					} else if ((tipoConta.equalsIgnoreCase(var.get_contaAnalista())) && (rbCenarioErro.isSelected())) {
						listTestes.setItems(null);

					}

				} catch (Exception e) {
				}
			}
		});
		return rbCenarioErro;

	}

	public ToggleGroup getTgCenarioTeste() {
		tgCenarioTeste = node.createToggleGroup();
		return tgCenarioTeste;
	}

}
