package es.indra.gui;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Interface extends Application {

	private Design design = new Design();
	private VariaveisFrame var = new VariaveisFrame();

	@Override
	public void start(Stage frame) throws Exception {
		frame.setScene(design.scene());
		frame.setTitle(var.get_tituloInterface());
		frame.getIcons().add(new Image(getClass().getResourceAsStream("logo_indra.png")));
		frame.setResizable(false);
		frame.centerOnScreen();
		frame.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
