package es.indra.gui;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class Design {

	private static AnchorPane pane;

	private ScreenElements es = new ScreenElements();

	public AnchorPane pane() {

		es.getListTestes();
		es.getItensFluxosPeritoSucesso();
		es.getItensFluxosPeritoErro();
		es.getItensFluxosAnalista();
		es.getTgCenarioTeste();

		pane = new AnchorPane();
		ObservableList<Node> list = pane.getChildren();

		list.addAll(es.getLbLogin(), es.getLbSenha(), es.getTxfLogin(), es.getPsfSenha());

		list.addAll(es.getLbSalvarInfo(), es.getBtnConfirm(), es.getBtnCancel(), es.getCb(), es.getComboTipoPesquisa(),
				es.getComboSeguradora());

		list.addAll(es.getComboPerfilConta(), es.getListTestes(), es.getComboTipoTeste());

		list.addAll(es.getRbCenarioSucesso(), es.getLbCenarioSucesso(), es.getRbCenarioErro(), es.getLbCenarioErro());

		list.addAll(es.getTxfElementoBusca());

		return pane;
	}

	public Scene scene() {
		Scene scene = new Scene(pane, 550, 330);
		scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
		return scene;
	}

	public Design() {
		pane();
	}

}
