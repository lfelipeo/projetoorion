package es.indra.gui;

public class VariaveisFrame {
	
	private final String[] _FLUXOS_PERITO_ORCAMENTO_SUCESSO = {
			"es.indra.execute.perito.orcamento.fluxo.FluxoSinistroExecute",
			"es.indra.execute.perito.orcamento.fluxo.FluxoAExecute" };

	private final String[] _FLUXOS_PERITO_ERRO = { "" };

	private final String[] _FLUXOS_ANALISTA_SUCESSO = { "" };

	/**********************/

	/**** UNITARIOS TESTE ****/

	private final String[] _UNITARIO_PERRITO_ORCAMENTO_SUCESSO = {
			"es.indra.execute.perito.orcamento.funcional.AbaAdicionaisExecute",
			"es.indra.execute.perito.orcamento.funcional.AbaBaremoExecute",
			"es.indra.execute.perito.orcamento.funcional.AbaFotosExecute",
			"es.indra.execute.perito.orcamento.funcional.AbaManualExecute",
			"es.indra.execute.perito.orcamento.funcional.PreOrcamentoExecute",
			"es.indra.execute.perito.orcamento.funcional.BotaoOutrosValoresExecute",
			"es.indra.execute.perito.orcamento.funcional.BotaoDetalhesExecute",
			"es.indra.execute.perito.orcamento.funcional.BotaoLaudoPmgExecute",
			"es.indra.execute.perito.orcamento.funcional.BotaoLaudoDeSalvadosExecute",
			"es.indra.execute.perito.orcamento.funcional.BotaoMaoDeObraExecute",
			"es.indra.execute.perito.orcamento.funcional.BotaoFotosExecute" };

	private final String[] _UNITARIO_PERRITO_ORCAMENTO_ERRO = { "" };

	/**********************/

	private final String[] _NOMES_ITENS_FLUXO_PERITO_SUCESSO = { "Abrir Sinistro", "Fluxo Simples" };

	private final String[] UNITARIO_PERITO_ORCAMENTO_SUCESSO = { "Abrir Sinistro - Aba Adicionais",
			"Abrir Sinistro - Aba Baremo", "Abrir Sinistro - Aba Fotos", "Abrir Sinistro - Aba Manual",
			"Abrir Sinistro - Pre Orcamento", "Abrir Sinistro - Bot�o Outros Valores",
			"Abrir Sinistro - Bot�o Detalhes", "Abrir Sinistro - Bot�o Laudo PMG",
			"Abrir Sinistro - Bot�o Laudo de Salvados", "Abrir Sinistro - Bot�o M�o-De-Obra",
			"Abrir Sinistro - Bot�o Fotos" };

	private final String _TITULO_INTERFACE = "Orion - Testes Automatizados";

	private final String[] _TIPO_PESQUISA_COMBO_PESQUISA = { "Placa", "Chassi" };

	/*******************/

	private static final String _SEGURADORA_MITSUI = "Mitsui";
	private static final String _SEGURADORA_TOKIO = "Tokio";

	private static final String _CONTA_PERITO = "Perito";
	private static final String _CONTA_ANALISTA = "Analista";

	private static final String _COLUNA_REF_PLACA = "Placa";
	private static final String _COLUNA_REF_CHASSI = "Chassi";

	/**************************************************************/
	/************************** GETTERS ***************************/

	/**** FLUXOS - TESTE ****/

	public String get_fluxoPeritoSucesso(int indice) {
		return _FLUXOS_PERITO_ORCAMENTO_SUCESSO[indice];
	}
	
	public String get_fluxoPeritoErro(int indice) {
		return _FLUXOS_PERITO_ERRO[indice];
	}

	public String get_fluxoAnalistaSucesso(int indice) {
		return _FLUXOS_ANALISTA_SUCESSO[indice];
	}

	/**** FLUXO - NOMES ****/

	public String get_fluxoSinistroPerito() {
		return _NOMES_ITENS_FLUXO_PERITO_SUCESSO[0];
	}
	
	public String getFluxoAPerito() {
		return _NOMES_ITENS_FLUXO_PERITO_SUCESSO[1];		
	}

	/**********************/

	/**** UNITARIOS - TESTES ****/

	public String get_unitarioPeritoSucesso(int indice) {
		return _UNITARIO_PERRITO_ORCAMENTO_SUCESSO[indice];
	}

	public String get_unitarioPeritoErro(int indice) {
		return _UNITARIO_PERRITO_ORCAMENTO_ERRO[indice];
	}

	/***************/

	/**** UNITARIOS - NOMES ****/

	public String getUnitarioPeritoOrcamentoSucessoAbAdicionais() {
		return UNITARIO_PERITO_ORCAMENTO_SUCESSO[0];
	}

	public String getUnitarioPeritoOrcamentoSucessoAbaBaremo() {
		return UNITARIO_PERITO_ORCAMENTO_SUCESSO[1];
	}

	public String getUnitarioPeritoOrcamentoSucessoAbaFotos() {
		return UNITARIO_PERITO_ORCAMENTO_SUCESSO[2];
	}

	public String getUnitarioPeritoOrcamentoSucessoAbaManual() {
		return UNITARIO_PERITO_ORCAMENTO_SUCESSO[3];
	}

	public String getUnitarioPeritoOrcamentoSucessoPreOrcamento() {
		return UNITARIO_PERITO_ORCAMENTO_SUCESSO[4];
	}

	public String getUnitarioPeritoOrcamentoSucessoBotaoOutrosValores() {
		return UNITARIO_PERITO_ORCAMENTO_SUCESSO[5];
	}

	public String getUnitarioPeritoOrcamentoSucessoBotaoDetalhes() {
		return UNITARIO_PERITO_ORCAMENTO_SUCESSO[6];
	}

	public String getUnitarioPeritoOrcamentoSucessoBotaoLaudoPmg() {
		return UNITARIO_PERITO_ORCAMENTO_SUCESSO[7];
	}

	public String getUnitarioPeritoOrcamentoSucessoBotaoLaudoDeSalvados() {
		return UNITARIO_PERITO_ORCAMENTO_SUCESSO[8];
	}

	public String getUnitarioPeritoOrcamentoSucessoBotaoMaoDeObra() {
		return UNITARIO_PERITO_ORCAMENTO_SUCESSO[9];
	}

	public String getUnitarioPeritoOrcamentoSucessoBotaoFotos() {
		return UNITARIO_PERITO_ORCAMENTO_SUCESSO[10];
	}

	/**********************/

	public String get_tituloInterface() {
		return _TITULO_INTERFACE;
	}

	public String get_tipoPesquisaComboPesquisa(int indice) {
		return _TIPO_PESQUISA_COMBO_PESQUISA[indice];
	}

	/*******************/

	public String get_seguradoraMitsui() {
		return _SEGURADORA_MITSUI;
	}

	public String get_seguradoraTokio() {
		return _SEGURADORA_TOKIO;
	}

	public String get_contaPerito() {
		return _CONTA_PERITO;
	}

	public String get_contaAnalista() {
		return _CONTA_ANALISTA;
	}

	public String get_colunaRefPlaca() {
		return _COLUNA_REF_PLACA;
	}

	public String get_colunaRefChassi() {
		return _COLUNA_REF_CHASSI;
	}

	/************************************************************/
}
