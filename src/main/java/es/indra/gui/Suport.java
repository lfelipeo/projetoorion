package es.indra.gui;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.runner.JUnitCore;

import es.indra.jsonhandler.JsonObj;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;

public class Suport {

	private JSONObject json = new JSONObject();
	private JSONParser parser = new JSONParser();
	private FileWriter writeFile = null;
	private VariaveisFrame var = new VariaveisFrame();

	public void inicia(ComboBox<String> comboPerfilConta, ComboBox<String> comboTipoTeste, ListView<String> listFluxos,
			RadioButton rbCenarioSucesso, RadioButton rbCenarioErro) {
		try {

			String tipoConta = comboPerfilConta.getSelectionModel().getSelectedItem();
			String tipoTeste = comboTipoTeste.getSelectionModel().getSelectedItem();
			int indiceSelecionado = listFluxos.getSelectionModel().getSelectedIndex();

			if (tipoTeste.equalsIgnoreCase("Fluxo")) {

				if (tipoConta.equalsIgnoreCase("Perito")) {

					if (rbCenarioSucesso.isSelected()) {
						executarTeste(var.get_fluxoPeritoSucesso(indiceSelecionado));
					} else if (rbCenarioErro.isSelected()) {
						executarTeste(var.get_fluxoPeritoErro(indiceSelecionado));
					}

				} else if (tipoConta.equalsIgnoreCase("Analista")) {

					if (rbCenarioSucesso.isSelected()) {
						executarTeste(var.get_fluxoAnalistaSucesso(indiceSelecionado));
					}
				}

			} else if (tipoTeste.equalsIgnoreCase("Unitario")) {

				if (tipoConta.equalsIgnoreCase("Perito")) {

					if (rbCenarioSucesso.isSelected()) {
						executarTeste(var.get_unitarioPeritoSucesso(indiceSelecionado));
					} else if (rbCenarioErro.isSelected()) {
						executarTeste(var.get_unitarioPeritoErro(indiceSelecionado));
					}

				} else if (tipoConta.equalsIgnoreCase("Analista")) {

					if (rbCenarioSucesso.isSelected()) {
						// executarTeste(var.get_fluxoAnalistaSucesso(indiceSelecionado));
					}
				}
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Nenhum fluxo selecionado");
		}
	}

	public void executarTeste(String fluxo) {
		try {
			JUnitCore.main(fluxo);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Nenhum Fluxo Selecionado");
		}
	}

	@SuppressWarnings("unchecked")
	public void salvaInfoLogin(String file, String login, String senha) {

		try {

			JsonObj.criaJson(file);
			json.put("login", login);
			json.put("senha", senha);
			writeFile = new FileWriter("json/" + file + ".json");
			writeFile.write(json.toString());
			writeFile.close();

		} catch (Exception e) {
		}
	}

	public String lerJson(String obj_json) {
		String texto = "";

		try {

			json = (JSONObject) parser.parse(new FileReader("Arquivos/Login/Info_Login.json"));

			if (!json.toString().equals("")) {
				texto = (String) json.get(obj_json);
				System.out.println(texto);
			}
		} catch (IOException | ParseException e1) {
		}
		return texto;
	}

}
