package es.indra.page.perito.telainical;

public class TelaInicialPageVar {

	private final String _TABLE_PRINCIPAL_ID = "ctl00_cph_Form_gvw_Padrao";
	private final String _COLUNA_REF_TABLE_PLACA = "Placa";
	private final String _BTN_LOCALIZAR_ID = "ctl00_cph_Botoes_img_Localizar"; // ctl00_cph_Botoes_img_Localizar
	private final String _BTN_LOGOFF_ID = "img_Logoff";
	private final String _TITLE_TELA_ID = "ctl00_tbc_BarraTitulo";

	private final String COMBO_CRITERIO_DE_CONSULTA = "ctl00_cph_Botoes_ddl_Criterio";// "ctl00_cph_Botoes_txt_Campo";
	private final String CAMPO_ELEMENTO_CONSULTA = "ctl00_cph_Botoes_txt_Campo";//"ctl00$cph_Botoes$txt_Campo";
	private final String BTN_LOCALIZADOR_LOCALIZADOR_ID = "ctl00_cph_Botoes_ibt_Localizar";
	private final String TABLE_LOCALIZADOR_ID = "ctl00_cph_Form_gvw_Padrao";
	private final String BTN_FECHA_LOCALIZADOR_ID = "img_Fechar";

	/********* GETTERS *********/

	public String getTablePrincipalId() {
		return _TABLE_PRINCIPAL_ID;
	}

	public String getColunaRefTablePlaca() {
		return _COLUNA_REF_TABLE_PLACA;
	}

	public String getBtnLocalizarId() {
		return _BTN_LOCALIZAR_ID;
	}

	public String getBtnLogoffId() {
		return _BTN_LOGOFF_ID;
	}

	public String getTitleTelaId() {
		return _TITLE_TELA_ID;
	}

	public String getComboCriterioDeConsulta() {
		return COMBO_CRITERIO_DE_CONSULTA;
	}

	public String getCampoElementoConsulta() {
		return CAMPO_ELEMENTO_CONSULTA;
	}

	public String getBtnLocalizadorLocalizadorId() {
		return BTN_LOCALIZADOR_LOCALIZADOR_ID;
	}

	public String getTableLocalizadorId() {
		return TABLE_LOCALIZADOR_ID;
	}

	public String getBtnFechaLocalizadorId() {
		return BTN_FECHA_LOCALIZADOR_ID;
	}

}
