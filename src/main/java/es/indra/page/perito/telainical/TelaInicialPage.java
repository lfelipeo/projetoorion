package es.indra.page.perito.telainical;

import org.openqa.selenium.By;

import es.indra.base.BasePage;

public class TelaInicialPage extends BasePage {

	private TelaInicialPageVar varPage = new TelaInicialPageVar();

	public void clicarBtnTabela(String elemento, String coluna) throws Exception {

		try {

			getDSL().esperaPaginaCarregarJs();

			frameAgenda();

			getDSL().esperaElemento(By.id(varPage.getTablePrincipalId()));

			getDSL().clicarTabelaDinamica(varPage.getTablePrincipalId(), coluna, elemento);

			// Espera o titulo estar visivel
			getDSL().esperarElementoEstarVisivelId(varPage.getTitleTelaId());

			// Volta o foco para o Frame Principal
			framePrincipal();

		} catch (Exception e) {
			throw new Exception(
					"ERRO AO CLICAR NO BOT�O ABRIR SINISTRO DO SINISTRO (" + elemento + ").\r\n" + e.getMessage());
		}
	}

}
