package es.indra.page.perito.telainical;

import org.openqa.selenium.By;

import es.indra.base.BasePage;

public class BotaoLocalizadorPage extends BasePage {

	private TelaInicialPageVar varPage = new TelaInicialPageVar();

	public void clicaLocalizador() throws Exception {

		try {

			frameAgenda();
			getDSL().clica(By.id(varPage.getBtnLocalizarId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO LOCALIZADOR.\r\n" + e.getMessage());
		}
	}

	public void selecionaCriterioDeConsulta(String texto) throws Exception {

		try {

			frameModalUm();
			getDSL().selecionaComboTexto(By.id(varPage.getComboCriterioDeConsulta()), texto);

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR UMA OPCAO EM CRITERIO DE CONSULTA.\r\n" + e.getMessage());
		}
	}

	public void preencheElementoDeConsulta(String elemento) throws Exception {

		try {

			frameModalUm();
			getDSL().escreveJsId(varPage.getCampoElementoConsulta(), elemento);
			getDSL().esperaValor(By.id(varPage.getCampoElementoConsulta()), elemento);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER O ELEMENTO PARA CONSULTA.\r\n" + e.getMessage());
		}
	}

	public void clicaLupaDentroLocalizador() {

		try {

			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnLocalizadorLocalizadorId()));

		} catch (Exception e) {

		}
	}

	public void verificaPlacaPesquisada(String placa) throws Exception {

		try {
			getDSL().comparaTextoComTextoDaTabela(varPage.getTableLocalizadorId(), placa, 18); // 21
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A SITUACAO DO SINISTRO POR PLACA.\r\n" + e.getMessage());
		}
	}

	public void verificaSituacao(String situacao) throws Exception {

		try {
			getDSL().comparaTextoComTextoDaTabela(varPage.getTableLocalizadorId(), situacao, 21);
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A SITUACAO DO SINISTRO POR PLACA.\r\n" + e.getMessage());
		}
	}

	public void fechaLocalizador() throws Exception {

		try {

			framePrincipal();
			getDSL().clica(By.id(varPage.getBtnFechaLocalizadorId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO FECHAR LOCALIZADOR.\r\n" + e.getMessage());
		}
	}
}
