package es.indra.page.perito.dadosdoveiculo;

public class TelaDadosDeVeiculoPageVar {

	/******* VAR / CONST *******/

	private static final String _TABLE_TITLE_DADOS_VEICULO_ID = "tbl_Titulo_1";
	private static final String _TABLE_DADOS_VEICULO_ID = "ctl00_cph_Form_Table1";
	private static final String _MARCA_FAB_CARRO_ID = "ctl00_cph_Form_ddl_Marca";
	private static final String _MODELO_CARRO_ID = "ctl00_cph_Form_ddl_Modelo";
	private static final String _COR_CARRO_ID = "ctl00_cph_Form_ddl_Cor";
	private static final String _CAMPO_QUILOMETRAGEM_CARRO_ID = "ctl00_cph_Form_txt_Quilometragem";
	private static final String _CAMPO_VALOR_MERCADO_CARRO_ID = "ctl00_cph_Form_txt_ValorMercado";
	private static final String _TABLE_OPCIONAIS_DADOS_VEICULO_ID = "ctl00_cph_Form_chk_Opcional";
	private static final String _BTN_CONFIRMAR_DADOS_VEICULO_ID = "ctl00_cph_Rodape_btn_Confirmar";

	/****************************/

	/********* GETTERS *********/

	public String get_tableTitleDadosVeiculoId() {
		return _TABLE_TITLE_DADOS_VEICULO_ID;
	}

	public String get_tableDadosVeiculoId() {
		return _TABLE_DADOS_VEICULO_ID;
	}

	public String get_marcaFabCarroId() {
		return _MARCA_FAB_CARRO_ID;
	}

	public String get_modeloCarroId() {
		return _MODELO_CARRO_ID;
	}

	public String get_corCarroId() {
		return _COR_CARRO_ID;
	}

	public String get_tableOpcionaisDadosVeiculoId() {
		return _TABLE_OPCIONAIS_DADOS_VEICULO_ID;
	}

	public String get_btnConfirmarDadosVeiculoId() {
		return _BTN_CONFIRMAR_DADOS_VEICULO_ID;
	}

	public String get_campoQuilometragemCarroId() {
		return _CAMPO_QUILOMETRAGEM_CARRO_ID;
	}

	public String get_campoValorMercadoCarroId() {
		return _CAMPO_VALOR_MERCADO_CARRO_ID;
	}

	/****************************/
}
