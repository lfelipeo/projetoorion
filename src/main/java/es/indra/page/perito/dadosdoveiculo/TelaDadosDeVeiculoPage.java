package es.indra.page.perito.dadosdoveiculo;

import org.openqa.selenium.By;

import es.indra.base.BasePage;

public class TelaDadosDeVeiculoPage extends BasePage {

	private TelaDadosDeVeiculoPageVar varPage = new TelaDadosDeVeiculoPageVar();

	public boolean verificaTelaDadosDoVeiculoEstaVisivel() throws Exception {

		try {

			esperaAjaxEIcone();
			framePrincipal();

			getDSL().esperaElemento(By.id(varPage.get_tableTitleDadosVeiculoId()));
			return getDSL().compararTextoId(varPage.get_tableTitleDadosVeiculoId(), "Dados do Veículo");

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE A TELA DADOS DE VEICULOS ESTÁ VISÍVEL. Erro: " + e.toString());
		}
	}

	public void verificaTelaDadosVeiculo() throws Exception {

		try {

			esperaAjaxEIcone();
			frameModalUm();
			getDSL().esperarElementoEstarVisivelId(varPage.get_tableDadosVeiculoId());

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE A TELA DE DADOS DO VEICULO ESTA VISIVEL.\r\n" + e.getMessage());
		}
	}

	public void selecionaMarca(String fabCarro) throws Exception {

		try {

			frameModalUm();
			getDSL().selecionaComboTexto(By.id(varPage.get_marcaFabCarroId()), fabCarro);

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR A MARCA.\r\n" + e.getMessage());
		}
	}

	public void selecionaModelo(String modCarro) throws Exception {

		try {

			esperaAjaxTelaDadosDoVeiculo();
			frameModalUm();

			getDSL().selecionaComboTexto(By.id(varPage.get_modeloCarroId()), modCarro);
		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR O MODELO.\r\n" + e.getMessage());
		}
	}

	public void selecionaCor(String corCarro) throws Exception {

		try {

			esperaAjaxTelaDadosDoVeiculo();
			frameModalUm();

			getDSL().selecionaComboTexto(By.id(varPage.get_corCarroId()), corCarro);
			// getDSL().clica(By.id("ctl00_cph_Form_tbl_Dados"));// Clica na tabela inteira
			// só para o combo perder o foco!

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR COR.\r\n" + e.getMessage());
		}
	}

	public void preencheQuilometragem(String quilometragem) throws Exception {

		try {

			esperaAjaxTelaDadosDoVeiculo();
			frameModalUm();

			getDSL().escreveJsId(varPage.get_campoQuilometragemCarroId(), quilometragem);
			getDSL().esperaValor(By.id(varPage.get_campoQuilometragemCarroId()), quilometragem);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER QUILOMETRAGEM.\r\n" + e.getMessage());
		}
	}

	public void preencheValorDeMercado(String valorMercado) throws Exception {

		try {

			esperaAjaxTelaDadosDoVeiculo();
			frameModalUm();

			getDSL().escreveJsId(varPage.get_campoValorMercadoCarroId(), valorMercado);
			getDSL().esperaValor(By.id(varPage.get_campoValorMercadoCarroId()), valorMercado);
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER 'VALOR DE MERCADO'.\r\n" + e.getMessage());
		}
	}

	public void selecionaOpcionais(int qtnOpcionais) throws Exception {

		esperaAjaxTelaDadosDoVeiculo();
		frameModalUm();

		getDSL().clicarOpcionais(varPage.get_tableOpcionaisDadosVeiculoId(), qtnOpcionais);
	}

	public void clicaConfirmar() throws Exception {

		try {

			esperaAjaxTelaDadosDoVeiculo();
			frameModalUm();

			getDSL().clica(By.id(varPage.get_btnConfirmarDadosVeiculoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO CONFIRMAR.\r\n" + e.getMessage());
		}
		cancelaTodosAlerts();
	}

	private void cancelaTodosAlerts() throws Exception {
		try {

			esperaAjaxTelaDadosDoVeiculo();
			while (getDSL().verificarAlertVisivel()) {
				getDSL().cancelarAlert();
			}

		} catch (Exception e) {
			throw new Exception("ERRO AO CANCELAR O ALERT.\r\n" + e.getMessage());
		}
	}

	public void waitAjax() throws Exception {
		esperaAjaxEIcone();
	}

	public boolean verificarDadosVeiculoVisivel() throws Exception {

		/*
		 * Verifica se a tela 'Dados do Veiculo' est� visivel
		 */

		try {
			return getDSL().verificaVisibilidadeBoolean(By.id(varPage.get_tableTitleDadosVeiculoId()));
			// return
			// getDSL().verificaVisibilidade(By.id(varPage.get_tableTitleDadosVeiculoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE A TELA DADOS DE VEICULOS EST� VIS�VEL. Erro: " + e.toString());
		}
	}

	public void esperaAjaxTelaDadosDoVeiculo() throws Exception {

		try {
			getDSL().waitForAjaxCalls();
			while (!(getDSL().esperaElementoNaoEstarVisivel(By.id("div_progresso")))) {
				Thread.sleep(100);
			}
			framePrincipal();
		} catch (Exception e) {
		}
	}

}
