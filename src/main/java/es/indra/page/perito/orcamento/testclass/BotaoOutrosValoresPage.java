package es.indra.page.perito.orcamento.testclass;

import java.util.Random;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class BotaoOutrosValoresPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();

	public void abreOutrosValoresBotao() throws Exception {

		try {

			//esperaTelaOrcamentoVoltarAEstarVisivel();
			esperaAjaxEIcone();

			frameAgenda();

			getDSL().clica(By.id(varPage.getBtnOutrosValoresOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO OUTROS VALORES. Selenium Erro: " + e.getMessage());
		}

	}

	public void alteraPrevisaoDeEntrega(String data) throws Exception {

		try {

			frameModalUm();

			getDSL().escreveJsId(varPage.getCampoPrevisaoDeEntregaOutrosValoresOrcamentoId(), data);
			getDSL().esperaValor(By.id(varPage.getCampoPrevisaoDeEntregaOutrosValoresOrcamentoId()), data);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER A PREVISAO DE ENTREGA.\r\n" + e.getMessage());
		}

	}

	public void alteraPrevisaoDeEntregaHora(String hora) throws Exception {

		try {

			frameModalUm();

			getDSL().escreveJsId(varPage.getCampoHoraPrevisaoDeEntregaOutrosValoresOrcamentoId(), hora);
			getDSL().esperaValor(By.id(varPage.getCampoHoraPrevisaoDeEntregaOutrosValoresOrcamentoId()), hora);

			framePrincipal();

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER A HORA DE PREVISAO DE ENTREGA.\r\n" + e.getMessage());
		}
	}

	public void alteraImportanciaSegurada() throws Exception {

		try {

			frameModalUm();

			/**
			 * Pega o valor atual que tive no campo 'Importancia Segurada' e soma com um
			 * n�mero aleat�rio de 1 a 5000.
			 **/

			Random random = new Random();
			String[] valorCampo = getDSL().pegaValor(By.id(varPage.getCampoImportanciaSeguradaOrcamentoId()))
					.split(",");
			int valorFinal = (Integer.parseInt(valorCampo[0])) + random.nextInt(5000);

			getDSL().escreveJsId(varPage.getCampoImportanciaSeguradaOrcamentoId(), "" + valorFinal);
			getDSL().esperaValor(By.id(varPage.getCampoImportanciaSeguradaOrcamentoId()), "" + valorFinal);

			framePrincipal();

		} catch (Exception e) {
			throw new Exception("ERRO AO ALTERAR O VALOR EM IMPORTANCIA SEGURADA.\r\n" + e.getMessage());
		}
	}

	public void preencheObservacao(String observacao) throws Exception {

		try {

			frameModalUm();

			getDSL().escreveJsId(varPage.get_campoObservacaoOutrosValoersId(), observacao);
			getDSL().esperaValor(By.id(varPage.get_campoObservacaoOutrosValoersId()), observacao);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHE O CAMPO OBSERVACAO.\r\n" + e.getMessage());
		}
	}

	public void clicaConfirmarOutrosValores() throws Exception {

		try {

			frameModalUm();

			// Clica no bot�o 'Confirmar' em Outros Valores
			getDSL().clica(By.id(varPage.getBtnConfirmarOutrosValoersOrcamentoId()));
			framePrincipal();

		} catch (Exception e) {
			throw new Exception("ERRO AO CONFIRMAR OUTROS VALORES. Selenium Erro: " + e.getMessage());
		}
	}

	public void fechaOutrosValores() throws Exception {

		try {

			framePrincipal();
			getDSL().clica(By.id(varPage.getBtnFecharOutrosValoresOrcamentoId()));
			
			esperaTelaOrcamentoVoltarAEstarVisivel();
			

		} catch (Exception e) {
			throw new Exception("ERRO AO FECHAR OUTROS VALORES. Selenium Erro: " + e.getMessage());
		}
	}

	public void verificaBotaoOutrosValoresVoltoouASerVisivel() throws Exception {

		try {

			frameAgenda();
			getDSL().esperaElemento(By.id(varPage.getBtnOutrosValoresOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE O BOTAO VOLTOU A SER VISIVEL.\r\n" + e.getMessage());
		}
	}
}
