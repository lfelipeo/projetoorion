package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

/**
 * 
 * Classe respons�vel por criar todas as a��es relacionada ao teste do bot�o
 * 'Dados do Veiculo'.
 * 
 * @author lfelipeo
 * 
 */

public class BotaoDadosDoVeiculoPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();

	/**
	 * Espera o ajax call finalizar, troca o foco da p�gina para o frame 'Agenda' e
	 * clica no bot�o 'Dados do Veiculo' na tela de Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaBotaoDadosDoVeiculo() throws Exception {

		try {

			//esperaTelaOrcamentoVoltarAEstarVisivel();

			esperaAjaxEIcone();
			frameAgenda();

			//getWait().until(getDSL().esperaElementoSerVisivelFW(By.id(varPage.getBtnDadosDoVeiculoId())));
			getDSL().clica(By.id(varPage.getBtnDadosDoVeiculoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO DADOS VEICULO.\r\n" + e.getMessage());
		}
	}

	public void alterarOpcionaisDadosVeiculoBotao(int qtnOpicionais) throws Exception {
		try {

			esperaAjaxEIcone();
			frameModalUm();

			getDSL().clicarOpcionais(varPage.get_tableOpcionaisDadosVeiculoId(), qtnOpicionais);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER OS OPCIONAIS DO VEICULO.\r\n" + e.getMessage());
		}
	}

	public void clicaConfirmar() throws Exception {

		try {

			getDSL().clica(By.id(varPage.get_btnConfirmarDadosVeiculoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO CONFIRMAR.\r\n" + e.getMessage());
		}

		try {

			while (getDSL().verificarAlertVisivel()) {
				getDSL().cancelarAlert();
			}

			getDSL().esperaPaginaCarregarJs();
			esperaAjaxEIcone();
			getDSL().esperaPaginaCarregarJs();

		} catch (Exception e) {
			throw new Exception("ERRO AO CONFIRMAR O ALERT.\r\n" + e.getMessage());
		}
	}

	public void verificaTelaOrcamentoPosConfirmar() {

		try {

			frameOrcamento();
			
			getDSL().esperaElementoNaoSerVisivel(By.id("ctl00_cph_Form_div_Reg1"));
			getDSL().esperaElemento(By.id("ctl00_cph_Form_div_Reg1"));
			
			esperaTelaOrcamentoVoltarAEstarVisivel();

		} catch (Exception e) {

		}
	}

}
