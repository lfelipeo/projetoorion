package es.indra.page.perito.orcamento.attributesclass;

public class OrcamentoPageVar {

	private final String _TABLE_OPCIONAIS_DADOS_VEICULO_ID = "ctl00_cph_Form_chk_Opcional";
	private final String LOGOTIPO = "img_Logotipo";
	private final String _TABLE_TITLE_ORCAMENTO_ID = "ctl00_lbl_TituloPagina2";

	/** Pre Orcamento **/
	private final String BTN_PRE_ORCAMENTO_ID = "ctl00_cph_Botoes_img_Pre_Orcamento";
	private final String TITLE_POPUP_PRE_ORCAMENTE_ID = "tbl_Titulo_1";
	private final String BTN_CALCULAR_PRE_ORCAMENTO_ID = "ctl00_cph_FormBTN_Calcula_Pre_Orcamento";
	private final String BTN_ADICIONAR_PECAS_PRE_ORCAMENTO_ID = "ctl00_cph_FormBTN_NovaPeca";
	private final String BTN_REGIAO_DIANTEIRA_PRE_ORCAMENTO_ID = "ctl00_cph_Form_gvw_Padrao_ctl02_txt_Checados";
	private final String BTN_ATUALIZAR_PRE_ORCAMENTO_ID = "ctl00_cph_RodapeBTN_Atualizar";

	/** Aba Baremo **/
	private final String _TITLE_TELA_ID = "ctl00_tbc_BarraTitulo";
	private final String _PARACHOQUE_DIANTEIRO_ORCAMENTO_ID = "ctl00_cph_Form_div_Reg1";
	private final String _ABS_IMPACTOS_DIANTEIRO_XPATH = "//*[@id='tbl_Pecas']/tbody/tr[2]/td[2]";
	private final String BTN_ADICIONAR_ORCAMENTO_ID = "ctl00_cph_Form_btn_AdicionarSubstituicao"; // "ctl00_cph_FormBTN_AdicionarSubstituicao";

	private final String _POPUP_PRECO_PECA_ORCAMENTO_ID = "tbl_Titulo_2";
	private final String _TABLE_PRECO_PECA_BAREMO_ORCAMENTO_ID = "ctl00_cph_Form_tbl_Lista";
	private final String BTN_CONFIRMAR_PRECO_PECA_BAREMO_ORCAMENTO_ID = "ctl00_cph_Rodape_btn_Confirmar"; // "ctl00_cph_RodapeBTN_Confirmar";

	/** Aba Manual **/


	/** Aba Adicionais **/
	private final String BTN_ADICIONAIS_ID = "tab_Orcamento_2";
	private final String SELECT_ITENS_ABA_ADICIONAIS_ORCAMENTO_ID = "ctl00_cph_Form_ltb_Itens";
	private final String CAMPO_SERVICO_ABA_ADICIONAIS_ID = "ctl00_cph_Form_txt_Descricao";
	private final String CAMPO_VALOR_ABA_ADICIONAIS_ORCAMENTO_ID = "ctl00_cph_Form_txt_Vl_Valor";
	private final String CAMPO_HORA_ABA_ADICIONAIS_ID = "ctl00_cph_Form_txt_Qt_Tempo";
	private final String BTN_ADICIONAR_ABA_ADICIONAIS_ID = "ctl00_cph_Form_ibt_Adicionar";

	/** Aba Fotos **/
	private final String _ABA_FOTOS_ORCAMENTO_ID = "tab_Orcamento_3";
	private final String _VERIFICACAO_FOTOS_ORCAMENTO_ID = "UpdatePanel1";
	private final String BTN_SELECIONAR_FOTOS_ID = "SWFUpload_0";
	private final String BTN_IMPORTAR_FOTOS_ID = "mup_Upload_img_Upload2";
	private final String TABLE_FOTOS_ADICIONADAS_ABA_FOTOS_ID = "dtl_Fotos";

	/** Bot�o Emiss�o de Relat�rios **/


	// Bot�o Fotos
	private final String BTN_FOTOS_ID = "ctl00_cph_Botoes_img_Fotos";

	// Lista de Pe�as
	private final String _TABLE_LISTA_PECAS_ID = "ctl00_cph_Form_tbl_Lista_Pecas";

	private final String BTN_PECAS_ORCAMENTO_ID = "ctl00_cph_Botoes_img_Pecas";

	private final String _TABLE_PECAS_ORCAMENTO_ID = "ctl00_cph_Form_tbl_Lista_Pecas";
	private final String BTN_FECHAR_PECAS_ORCAMENTO_ID = "img_Fechar";

	private final String BTN_ENCERRAR_ORCAMENTO_ID = "ctl00_cph_Botoes_img_Encerrar";
	private final String _TITLE_FINALIZAR_SINISTRO_ID = "lbl_TituloPagina_1";
	private final String _COMBO_CARIMBO_FINALIZAR_SINISTRO_ID = "ctl00_cph_Form_ddl_Cd_Carimbo";

	private final String _CAMPO_OBSERVACAO_FINALIZAR_SINISTRO_ID = "ctl00_cph_Form_txt_Ds_Observacao";
	private final String BTN_CANCELAR_FINALIZAR_SINISTRO_ID = "ctl00_cph_RodapeBTN_Cancelar";
	private final String BTN_CONFIRMAR_FINALIZAR_SINISTRO_ID = "ctl00_cph_Rodape_btn_Confirmar"; // "ctl00_cph_RodapeBTN_Confirmar";

	// Laudos de Salvados
	
	private final String BTN_LAUDOS_DE_SALVADOS_ORCAMENTO_ID = "ctl00_cph_Botoes_img_Salvados";
	private final String _COMBO_QUALIFICACAO_LAUDO_DE_SALVADOS_ORCAMENTO_ID = "ctl00_cph_Form_lstGrupos";
	private final String _TABLE_LAUDO_DE_SALVADOS_ORCAMENTO_ID = "tbl_Titulo_1";
	private final String _ABA_QUALIFICACAO_LAUDO_DE_SALVADOS_ORCAMENTO_ID = "tab_Acoes_0";
	private final String _TABLE_QUALIFICACAO_LAUDO_DE_SALVADOS_ORCAMENTO_ID = "ctl00_cph_Form_tbl_Qualificacao";
	private final String BTN_FECHAR_LAUDO_DE_SALVADOS_ORCAMENTO_ID = "img_Fechar";

	// Laudo PMG
	
	private final String BTN_LAUDO_PMG_ORCAMENTO_ID = "ctl00_cph_Botoes_img_PMG";
	private final String _TABLE_LAUDO_PMG_ORCAMENTO_ID = "ctl00_cph_Form_tbl_PMG";

	private final String BTN_OBSERVACAO_LAUDO_PMG_ORCAMENTO_ID = "ctl00_cph_Form_btn_DadosPMG";// "ctl00_cph_FormBTN_DadosPMG";

	private final String _CAMPO_OBSERVACAO_LAUDO_PMG_ORCAMENTO_ID = "ctl00_cph_Form_txt_Observacao";
	private final String _CAMPO_NOME_AGENTE_LAUDO_PMG_ORCAMENTO_ID = "ctl00_cph_Form_txt_NomeAgente";
	private final String _CAMPO_MATRICULA_AGENTE_LAUDO_PMG_ORCAMENTO_ID = "ctl00_cph_Form_txt_MatriculaAgente";
	private final String _CAMPO_N_BO_ACIDENTE_LAUDO_PMG_ORCAMENTO_ID = "ctl00_cph_Form_txt_Boat";
	private final String BTN_CONFIRMAR_OBSERVACAO_LAUDO_PMG_ORCAMENTO_ID = "ctl00_cph_Rodape_btn_Confirmar";// "ctl00_cph_RodapeBTN_Confirmar";
	private final String BTN_CONFIRMAR_LAUDO_PMG_ORCAMENTO_ID = "ctl00_cph_Form_btn_Confirmar"; // "ctl00_cph_FormBTN_Confirmar";
	private final String BTN_FECHAR_LAUDO_PMG_ORCAMENTO_ID = "img_Fechar";

	// M�o-De-Obra
	private final String BTN_MAO_DE_OBRA_ORCAMENTO_ID = "ctl00_cph_Botoes_img_MaoObra";
	private final String _TABLE_TITLE_MAO_DE_OBRA_ORCAMENTO_ID = "tbl_Titulo_1";
	private final String _TABLE_VALORES_MAO_DE_OBRA_ORCAMENTO_ID = "ctl00_cph_Form_gvw_Padrao";
	private final String _CAMPO_MECANICA_MAO_DE_OBRA_ORCAMENTO_ID = "ctl00_cph_Form_gvw_Padrao_ctl02_txt_Valor";
	private final String _CAMPO_FUNILARIA_MAO_DE_OBRA_ORCAMENTO_ID = "ctl00_cph_Form_gvw_Padrao_ctl03_txt_Valor";
	private final String _CAMPO_PINTURA_MAO_DE_OBRA_ORCAMENTO_ID = "ctl00_cph_Form_gvw_Padrao_ctl04_txt_Valor";
	private final String _CAMPO_ELETRICA_MAO_DE_OBRA_ORCAMENTO_ID = "ctl00_cph_Form_gvw_Padrao_ctl05_txt_Valor";
	private final String _CAMPO_TAPECARIA_MAO_DE_OBRA_ORCAMENTO_ID = "ctl00_cph_Form_gvw_Padrao_ctl06_txt_Valor";
	private final String _CAMPO_VIDRACARIA_MAO_DE_OBRA_ORCAMENTO_ID = "ctl00_cph_Form_gvw_Padrao_ctl07_txt_Valor";
	private final String _CAMPO_REPARACAO_MAO_DE_OBRA_ORCAMENTO_ID = "ctl00_cph_Form_gvw_Padrao_ctl08_txt_Valor";
	private final String BTN_CONFIRMAR_MAO_DE_OBRA_ORCAMENTO_ID = "ctl00_cph_Rodape_btn_Confirmar";// "ctl00_cph_RodapeBTN_Confirmar";

	// Outros valores
	private final String BTN_OUTROS_VALORES_ORCAMENTO_ID = "ctl00_cph_Botoes_img_Valores";
	private final String CAMPO_IMPORTANCIA_SEGURADA_ORCAMENTO_ID = "ctl00_cph_Form_txt_Vl_Importancia_Segurada";
	private final String CAMPO_PREVISAO_DE_ENTREGA_OUTROS_VALORES_ORCAMENTO_ID = "ctl00_cph_Form_txt_Data_Prevista";
	private final String CAMPO_HORA_PREVISAO_DE_ENTREGA_OUTROS_VALORES_ORCAMENTO_ID = "ctl00_cph_Form_txt_Hora_Prevista";
	private final String BTN_CONFIRMAR_OUTROS_VALORES_ORCAMENTO_ID = "ctl00_cph_Rodape_btn_Confirmar";// "ctl00_cph_RodapeBTN_Confirmar";
	private final String BTN_FECHAR_OUTROS_VALORES_ORCAMENTO_ID = "img_Fechar";
	private final String CAMPO_OBSERVACAO_OUTROS_VALORES_ID = "ctl00_cph_Form_txt_Ds_Obs_Oficina";

	// Identifica��o do Sinistro
	private final String BTN_IDENTIFICACAO_SINISTRO_ID = "ctl00_cph_Botoes_img_IdentificacaoSinisto";
	private final String _CAMPO_TEXTO_PLACA_IDENTIFICACAO_SINISTRO_ID = "ctl00_cph_Form_lbl_Nr_Placa";

	// Dados Veiculo
	private final String BTN_DADOS_DO_VEICULO_ID = "ctl00_cph_Botoes_img_Veiculo";
	private final String BTN_CONFIRMAR_DADOS_VEICULO_ID = "ctl00_cph_Rodape_btn_Confirmar";// "ctl00_cph_RodapeBTN_Confirmar";

	// Detalhes
	private final String BTN_DETALHES_ORCAMENTO_ID = "ctl00_cph_Botoes_img_Detalhes";
	private final String _TELA_TEXTO_DETALHES_ORCAMENTO_ID = "ctl00_cph_Form_txt_Ds_Observacao";
	private final String BTN_FECHAR_DETALHES_ORCAMENTO_ID = "img_Fechar";

	/*************************/

	/******* GETTERS *******/

	public String get_tableOpcionaisDadosVeiculoId() {
		return _TABLE_OPCIONAIS_DADOS_VEICULO_ID;
	}

	// Pr� Orcamento

	public String get_btnPreOrcamentoId() {
		return BTN_PRE_ORCAMENTO_ID;
	}

	public String get_titlePopupPreOrcamenteId() {
		return TITLE_POPUP_PRE_ORCAMENTE_ID;
	}

	public String get_btnCalcularPreOrcamentoId() {
		return BTN_CALCULAR_PRE_ORCAMENTO_ID;
	}

	public String get_btnAdicionarPecasPreOrcamentoId() {
		return BTN_ADICIONAR_PECAS_PRE_ORCAMENTO_ID;
	}

	public String get_btnRegiaoDianteiraPreOrcamentoId() {
		return BTN_REGIAO_DIANTEIRA_PRE_ORCAMENTO_ID;
	}

	public String get_btnAtualizarPreOrcamentoId() {
		return BTN_ATUALIZAR_PRE_ORCAMENTO_ID;
	}

	// Aba Baremo

	public String get_titleTelaId() {
		return _TITLE_TELA_ID;
	}

	public String get_logotipo() {
		return LOGOTIPO;

	}

	public String get_tableTitleOrcamentoId() {
		return _TABLE_TITLE_ORCAMENTO_ID;
	}

	public String get_parachoqueDianteiroOrcamentoId() {
		return _PARACHOQUE_DIANTEIRO_ORCAMENTO_ID;
	}

	public String get_absImpactosDianteiroXpath() {
		return _ABS_IMPACTOS_DIANTEIRO_XPATH;
	}

	public String getBtnAdicionarOrcamentoId() {
		return BTN_ADICIONAR_ORCAMENTO_ID;
	}

	public String get_popupPrecoPecaOrcamentoId() {
		return _POPUP_PRECO_PECA_ORCAMENTO_ID;
	}

	public String get_tablePrecoPecaBaremoOrcamentoId() {
		return _TABLE_PRECO_PECA_BAREMO_ORCAMENTO_ID;
	}

	public String get_btnConfirmarPrecoPecaBaremoOrcamentoId() {
		return BTN_CONFIRMAR_PRECO_PECA_BAREMO_ORCAMENTO_ID;
	}


	// Aba Adicionais

	public String getBtnAdicionaisOrcamentoId() {
		return BTN_ADICIONAIS_ID;
	}

	public String getSelectItensAbaAdicionaisOrcamentoId() {
		return SELECT_ITENS_ABA_ADICIONAIS_ORCAMENTO_ID;
	}

	public String getCampoServicoAbaAdicionaisId() {
		return CAMPO_SERVICO_ABA_ADICIONAIS_ID;
	}

	public String getCampoValorAbaAdicionaisId() {
		return CAMPO_VALOR_ABA_ADICIONAIS_ORCAMENTO_ID;
	}

	public String getCampoHoraAbaAdicionaisId() {
		return CAMPO_HORA_ABA_ADICIONAIS_ID;
	}

	public String getBtnAdicionarAbaAdicionaisId() {
		return BTN_ADICIONAR_ABA_ADICIONAIS_ID;
	}

	// Aba Fotos

	public String get_btnFotosOrcamentoId() {
		return _ABA_FOTOS_ORCAMENTO_ID;
	}

	public String get_verificacaoFotosOrcamentoId() {
		return _VERIFICACAO_FOTOS_ORCAMENTO_ID;
	}

	public String get_btnSelecionarFotosId() {
		return BTN_SELECIONAR_FOTOS_ID;
	}

	public String get_btnImportarFotosId() {
		return BTN_IMPORTAR_FOTOS_ID;
	}

	public String getTableFotosAdicionadasAbaFotosId() {
		return TABLE_FOTOS_ADICIONADAS_ABA_FOTOS_ID;
	}

	// Bot�o Fotos

	public String get_btnFotosId() {
		return BTN_FOTOS_ID;
	}

	// Emiss�ro de Relatorios


	public String get_btnPecasOrcamentoId() {
		return BTN_PECAS_ORCAMENTO_ID;
	}

	public String get_btnFecharPecasOrcamentoId() {
		return BTN_FECHAR_PECAS_ORCAMENTO_ID;
	}

	public String get_tablePecasOrcamentoId() {
		return _TABLE_PECAS_ORCAMENTO_ID;
	}


	///

	public String get_btnFecharEsteOrcamentoId() {
		return BTN_ENCERRAR_ORCAMENTO_ID;
	}

	public String get_titleFinalizarSinistroId() {
		return _TITLE_FINALIZAR_SINISTRO_ID;
	}

	public String get_comboCarimboFinalizarSinistroId() {
		return _COMBO_CARIMBO_FINALIZAR_SINISTRO_ID;
	}

	public String get_campoObservacaoFinalizarSinistroId() {
		return _CAMPO_OBSERVACAO_FINALIZAR_SINISTRO_ID;
	}

	public String get_btnCancelarFinalizarSinistroId() {
		return BTN_CANCELAR_FINALIZAR_SINISTRO_ID; // PAREI AQUI!!
	}

	public String get_btnConfirmarFinalizarSinistroId() {
		return BTN_CONFIRMAR_FINALIZAR_SINISTRO_ID;
	}

	// Lista de pe�as

	public String get_tableListaPecasId() {
		return _TABLE_LISTA_PECAS_ID;
	}

	// Laudos de Salvados

	public String get_btnLaudoDeSalvadosOrcamentoId() {
		return BTN_LAUDOS_DE_SALVADOS_ORCAMENTO_ID;
	}

	public String get_comboQualificacaoLaudoDeSalvadosOrcamentoId() {
		return _COMBO_QUALIFICACAO_LAUDO_DE_SALVADOS_ORCAMENTO_ID;
	}

	public String get_tableLaudoDeSalvadosOrcamentoId() {
		return _TABLE_LAUDO_DE_SALVADOS_ORCAMENTO_ID;
	}

	public String get_abaQualificacaoLaudoDeSalvadosOrcamentoId() {
		return _ABA_QUALIFICACAO_LAUDO_DE_SALVADOS_ORCAMENTO_ID;
	}

	public String get_tableQualificacaoLaudoDeSalvadosOrcamentoId() {
		return _TABLE_QUALIFICACAO_LAUDO_DE_SALVADOS_ORCAMENTO_ID;
	}

	public String get_btnFecharLaudoDeSalvadosOrcamentoId() {
		return BTN_FECHAR_LAUDO_DE_SALVADOS_ORCAMENTO_ID;
	}

	// Laudo PMG

	public String get_btnLaudoPmgOrcamentoId() {
		return BTN_LAUDO_PMG_ORCAMENTO_ID;
	}

	public String get_tableLaudoPmgOrcamentoId() {
		return _TABLE_LAUDO_PMG_ORCAMENTO_ID;
	}

	public String get_btnObservacaoLaudoPmgOrcamentoId() {
		return BTN_OBSERVACAO_LAUDO_PMG_ORCAMENTO_ID;
	}

	public String get_campoObservacaoLudoPmgOrcamentoId() {
		return _CAMPO_OBSERVACAO_LAUDO_PMG_ORCAMENTO_ID;
	}

	public String get_campoNomeAgenteLaudoPmgOrcamentoId() {
		return _CAMPO_NOME_AGENTE_LAUDO_PMG_ORCAMENTO_ID;
	}

	public String get_campoMatriculaAgenteLaudoPmgOrcamentoId() {
		return _CAMPO_MATRICULA_AGENTE_LAUDO_PMG_ORCAMENTO_ID;
	}

	public String get_campoNBoAcidenteLaudoPmgOrcamentoId() {
		return _CAMPO_N_BO_ACIDENTE_LAUDO_PMG_ORCAMENTO_ID;
	}

	public String get_btnConfirmarObservacaoLaudoPmgOrcamentoId() {
		return BTN_CONFIRMAR_OBSERVACAO_LAUDO_PMG_ORCAMENTO_ID;
	}

	public String get_btnConfirmarLaudoPmgOrcamentoId() {
		return BTN_CONFIRMAR_LAUDO_PMG_ORCAMENTO_ID;
	}

	public String get_btnFecharLaudoPmgOrcamentoId() {
		return BTN_FECHAR_LAUDO_PMG_ORCAMENTO_ID;
	}

	// M�o-De-Obra

	public String get_btnMaoDeObraOrcamentoId() {
		return BTN_MAO_DE_OBRA_ORCAMENTO_ID;
	}

	public String get_tableTileMaoDeObraOrcamentoId() {
		return _TABLE_TITLE_MAO_DE_OBRA_ORCAMENTO_ID;
	}

	public String get_tableValoresMaoDeObraOrcamentoId() {
		return _TABLE_VALORES_MAO_DE_OBRA_ORCAMENTO_ID;
	}

	public String get_campoMecanicaMaoDeObraOrcamentoId() {
		return _CAMPO_MECANICA_MAO_DE_OBRA_ORCAMENTO_ID;
	}

	public String get_campoFunilariaMaoDeObraOrcamentoId() {
		return _CAMPO_FUNILARIA_MAO_DE_OBRA_ORCAMENTO_ID;
	}

	public String get_campoPinturaMaoDeObraOrcamentoId() {
		return _CAMPO_PINTURA_MAO_DE_OBRA_ORCAMENTO_ID;
	}

	public String get_campoEletricaMaoDeObraOrcamentoId() {
		return _CAMPO_ELETRICA_MAO_DE_OBRA_ORCAMENTO_ID;
	}

	public String get_campoTapecariaMaoDeObraOrcamentoId() {
		return _CAMPO_TAPECARIA_MAO_DE_OBRA_ORCAMENTO_ID;
	}

	public String get_campoVidracariaMaoDeObraOrcamentoId() {
		return _CAMPO_VIDRACARIA_MAO_DE_OBRA_ORCAMENTO_ID;
	}

	public String get_campoReparacaoMaoDeObraOrcamentoId() {
		return _CAMPO_REPARACAO_MAO_DE_OBRA_ORCAMENTO_ID;
	}

	public String get_btnConfirmarMaoDeObraOrcamentoId() {
		return BTN_CONFIRMAR_MAO_DE_OBRA_ORCAMENTO_ID;
	}

	public String get_campoObservacaoOutrosValoersId() {
		return CAMPO_OBSERVACAO_OUTROS_VALORES_ID;
	}

	// Outros Valores

	public String getBtnOutrosValoresOrcamentoId() {
		return BTN_OUTROS_VALORES_ORCAMENTO_ID;
	}

	public String getCampoImportanciaSeguradaOrcamentoId() {
		return CAMPO_IMPORTANCIA_SEGURADA_ORCAMENTO_ID;
	}

	public String getCampoPrevisaoDeEntregaOutrosValoresOrcamentoId() {
		return CAMPO_PREVISAO_DE_ENTREGA_OUTROS_VALORES_ORCAMENTO_ID;
	}

	public String getCampoHoraPrevisaoDeEntregaOutrosValoresOrcamentoId() {
		return CAMPO_HORA_PREVISAO_DE_ENTREGA_OUTROS_VALORES_ORCAMENTO_ID;
	}

	public String getBtnConfirmarOutrosValoersOrcamentoId() {
		return BTN_CONFIRMAR_OUTROS_VALORES_ORCAMENTO_ID;
	}

	public String getBtnFecharOutrosValoresOrcamentoId() {
		return BTN_FECHAR_OUTROS_VALORES_ORCAMENTO_ID;
	}

	// Identifica��o do Sinistro

	public String get_btnIdentificacaoSinistroId() {
		return BTN_IDENTIFICACAO_SINISTRO_ID;
	}

	public String get_campoTextoPlacaIdentificacaoSinistroId() {
		return _CAMPO_TEXTO_PLACA_IDENTIFICACAO_SINISTRO_ID;
	}

	// Dados do Veiculo

	public String getBtnDadosDoVeiculoId() {
		return BTN_DADOS_DO_VEICULO_ID;
	}

	public String get_btnConfirmarDadosVeiculoId() {
		return BTN_CONFIRMAR_DADOS_VEICULO_ID;
	}

	// Bot�o Detalhes

	public String get_btnDetalhesOrcamentoId() {
		return BTN_DETALHES_ORCAMENTO_ID;
	}

	public String get_telaTextoDetalhesOrcamentoId() {
		return _TELA_TEXTO_DETALHES_ORCAMENTO_ID;
	}

	public String get_btnFecharDetalhesOrcamentoId() {
		return BTN_FECHAR_DETALHES_ORCAMENTO_ID;
	}

	/************************/

}
