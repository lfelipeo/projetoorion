package es.indra.page.perito.orcamento.testclass;

import java.util.Random;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class BotaoLaudoPmgPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();

	/**
	 * Espera o ajax call finalizar, troca o foco da p�gina para o frame 'Agenda' e
	 * clica no bot�o 'Laudo PMG'.
	 * 
	 * @throws Exception
	 */

	public void clicaLaudoPmgBotao() throws Exception {
		try {

			//esperaTelaOrcamentoVoltarAEstarVisivel();
			esperaAjaxEIcone();

			frameAgenda();

			getDSL().clica(By.id(varPage.get_btnLaudoPmgOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO LAUDO PMG.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera o ajax call finalizar, troca o foco da p�gina para o frame 'Modal Um'
	 * e clica na linha gerada aleatoriamente na coluna danificada. Caso clique em
	 * uma op��o j� selecionada um alert ir� aparecer e ser� confirmado.
	 * 
	 * @param linha
	 * @throws Exception
	 */

	public void selecionaPecaColunaDanificada(String linha) throws Exception {

		try {

			esperaAjaxEIcone();
			frameModalUm();

			getDSL().esperaElemento(By.id(varPage.get_tableLaudoPmgOrcamentoId()));
			getDSL().clica(By.id(linha));
			esperaAjaxEIcone();

			if (getDSL().verificarAlertVisivel()) {
				getDSL().confirmarAlert();
			}

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR A LINHA NA COLUNA DANIFICADA.\r\n" + e.getMessage());
		}

	}

	/**
	 * Clica no bot�o 'Observa��o' e troca o foco da p�gina para o frame 'Modal
	 * Dois'.
	 * 
	 * @throws Exception
	 */

	public void clicaObservacao() throws Exception {
		try {

			frameModalUm();
			getDSL().clica(By.id(varPage.get_btnObservacaoLaudoPmgOrcamentoId()));

			esperaAjaxEIcone();
			frameModalDois();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO OBSERVACAO.\r\n" + e.getMessage());
		}
	}

	/**
	 * Preenche os campos da tela observa��o, campos: Observa��es, Nome Agente,
	 * Registro/Matricula Agente e N BO Acidente.
	 * 
	 * @param texto_observacao
	 * @param nome_agente
	 * @param matricula_agente
	 * @param n_bo_acidente
	 * @throws Exception
	 */

	public void preencherObservacaoLaudoPmg(String texto_observacao, String nome_agente, String matricula_agente,
			String n_bo_acidente) throws Exception {
		preencheObservacao(texto_observacao);
		preencheNomeAgente(nome_agente);
		preencheRegistroMatriculaAgente(matricula_agente);
		preencheNBoAcidente(n_bo_acidente);
	}

	private void preencheObservacao(String texto_observacao) throws Exception {

		try {
			getDSL().escreveJsId(varPage.get_campoObservacaoLudoPmgOrcamentoId(), texto_observacao);
			getDSL().esperaTexto(By.id(varPage.get_campoObservacaoLudoPmgOrcamentoId()), texto_observacao);
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHE OBSERVACAO.\r\n" + e.getMessage());
		}
	}

	private void preencheNomeAgente(String nome_agente) throws Exception {

		try {
			getDSL().escreveJsId(varPage.get_campoNomeAgenteLaudoPmgOrcamentoId(), nome_agente);
			getDSL().esperaValor(By.id(varPage.get_campoNomeAgenteLaudoPmgOrcamentoId()), nome_agente);
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER NOME AGENTE.\r\n" + e.getMessage());
		}
	}

	private void preencheRegistroMatriculaAgente(String matricula_agente) throws Exception {

		try {
			getDSL().escreveJsId(varPage.get_campoMatriculaAgenteLaudoPmgOrcamentoId(), matricula_agente);
			getDSL().esperaValor(By.id(varPage.get_campoMatriculaAgenteLaudoPmgOrcamentoId()), matricula_agente);
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER REGISTRO MATRICULA AGENTE.\r\n" + e.getMessage());
		}
	}

	private void preencheNBoAcidente(String n_bo_acidente) throws Exception {

		try {
			getDSL().escreveJsId(varPage.get_campoNBoAcidenteLaudoPmgOrcamentoId(), n_bo_acidente);
			getDSL().esperaValor(By.id(varPage.get_campoNBoAcidenteLaudoPmgOrcamentoId()), n_bo_acidente);
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER N BO ACIDENTE.\r\n" + e.getMessage());
		}
	}

	public void clicaConfirmarObservacao() throws Exception {
		try {

			getDSL().clica(By.id(varPage.get_btnConfirmarObservacaoLaudoPmgOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO CONFIRMAR EM OBSERVACAO.\r\n" + e.getMessage());
		}
	}

	public void clicaConfirmar() throws Exception {
		try {

			esperaTelaLaudoPmgVoltar();
			frameModalUm();
			getDSL().clica(By.id(varPage.get_btnConfirmarLaudoPmgOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO CONFIRMAR.\r\n" + e.getMessage());
		}
	}

	private void esperaTelaLaudoPmgVoltar() throws Exception {

		try {

			frameModalUm();
			getDSL().esperaElemento(By.id(varPage.get_btnObservacaoLaudoPmgOrcamentoId()));
			getDSL().esperaElemento(By.id(varPage.get_btnConfirmarLaudoPmgOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR A TELA VOLTAR APOS CONFIRMAR A OBSERVACAO.\r\n" + e.getMessage());
		}
	}

	public void clicaFechar() throws Exception {

		try {

			getDSL().clica(By.id(varPage.get_btnFecharLaudoPmgOrcamentoId()));

			esperaTelaOrcamentoVoltarAEstarVisivel();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO FECHAR.\r\n" + e.getMessage());
		}
	}

	public void esperaOBotaoVoltarASerVisivel() throws Exception {

		try {

			frameAgenda();
			getDSL().esperaElemento(By.id(varPage.get_btnLaudoDeSalvadosOrcamentoId()));

			framePrincipal();

		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR A TELA DE LAUDO PMG FECHAR.\r\n" + e.getMessage());
		}
	}

	public void selecionaColunaDanificadaTeste() throws Exception {

		esperaAjaxEIcone();
		frameModalUm();

		getDSL().esperaElemento(By.id(varPage.get_tableLaudoPmgOrcamentoId()));

		getDSL().clica(By.xpath(xpathOpcaoColuna()));
	}

	/**
	 * Gera um xpath aleat�rio, 
	 * @return
	 */
	
	private String xpathOpcaoColuna() {

		Random gerador = new Random();

		int trAleatorio = gerador.nextInt(20) + 2;
		while (trAleatorio < 1 | trAleatorio > 20) {
			trAleatorio = gerador.nextInt(20) + 2;
		}

		int tdAleatorio = gerador.nextInt(6) + 4;
		while (tdAleatorio < 4 | tdAleatorio > 6) {
			tdAleatorio = gerador.nextInt(6) + 4;
		}

		return "//*[@id='ctl00_cph_Form_tbl_PMG']/tbody/tr[" + trAleatorio + "]/td[" + tdAleatorio + "]";

	}

}
