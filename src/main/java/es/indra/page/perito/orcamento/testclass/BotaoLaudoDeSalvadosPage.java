package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class BotaoLaudoDeSalvadosPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();

	/**
	 * Espera a tela de Or�amento estar vis�vel, os processos do ajax serem
	 * finalizador e troca o foco da p�gina para o frame 'Agenda' na tela de
	 * Or�amento. Ap�s o foco ser trocado clica no bot�o 'Laudo de Salvados'.
	 * 
	 * @throws Exception
	 */

	public void clicaBotaoLaudoDeSalvados() throws Exception {

		try {

			//esperaTelaOrcamentoVoltarAEstarVisivel();
			esperaAjaxEIcone();

			frameAgenda();
			getDSL().clica(By.id(varPage.get_btnLaudoDeSalvadosOrcamentoId()));

			frameModalUm();
			getDSL().esperarElementoEstarVisivelId(varPage.get_abaQualificacaoLaudoDeSalvadosOrcamentoId());

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO LAUDO DE SALVADOS.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera a tela Laudo de Salvados carregar totalmente, troca o foco da p�gina
	 * para o frame 'Modal Um' e seleciona uma op��o em 'Qualifica��o'. Caso a
	 * seguradora seja 'Tokio', durante a troca de op��o em 'Qualifica��o' um alerta
	 * ir� aparecer e sera confirmado.
	 * 
	 * @param opcao
	 * @throws Exception
	 */

	public void selecionaOpcaoQualificacao(String opcao) throws Exception {

		try {

			esperaAjaxBotaoLaudoDeSalvados();
			frameModalUm();

			getDSL().selecionaComboTexto(By.id(varPage.get_comboQualificacaoLaudoDeSalvadosOrcamentoId()), opcao);

			if (getDSL().verificarAlertVisivel()) {
				getDSL().confirmarAlert();
			}

			frameModalUm();
			esperaAjaxBotaoLaudoDeSalvados();
			frameModalUm();

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR A OPCAO " + opcao + " em Qualificacao.\r\n" + e.getMessage());
		}
	}

	/**
	 * Seleciona uma situa��o na aba Qualifica��o.
	 * 
	 * @param opcao
	 * @param linhas
	 * @throws Exception
	 */

	public void selecionaSituacao(String opcao, int linhas) throws Exception {
		try {

			getDSL().selecionarOpcaoLaudoDeSalvados(varPage.get_tableQualificacaoLaudoDeSalvadosOrcamentoId(), linhas);

			esperaAjaxBotaoLaudoDeSalvados();
			frameModalUm();

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR UM SITUACAO EM '" + opcao + "'.\r\n" + e.getMessage());
		}
	}

	public boolean verificaOpcaoExiste(String opcao_texto) throws Exception {

		try {
			return getDSL().verificaOpcaoExisteComboId(varPage.get_comboQualificacaoLaudoDeSalvadosOrcamentoId(),
					opcao_texto);
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE A OPCAO EXISTE EM QUALIFICACAO.\r\n" + e.getMessage());
		}

	}

	public void fecharLaudoDeSalvados() throws Exception {

		try {

			framePrincipal();
			frameModalUm();

			getDSL().clica(By.id("ctl00_cph_Rodape_Table5"));

			if (getDSL().verificarAlertVisivel()) {
				getDSL().confirmarAlert();
			}

			//esperaTelaOrcamentoVoltarAEstarVisivel();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOT�O FECHAR EM LAUDO DE SALVADOS NA TELA OR�AMENTO. Selenium Erro: "
					+ e.toString());
		}
	}

	public void esperaAjaxBotaoLaudoDeSalvados() throws Exception {

		try {
			getDSL().waitForAjaxCalls();
			while (!(getDSL().esperaElementoNaoEstarVisivel(By.id("div_progresso")))) {
				Thread.sleep(100);
			}
			framePrincipal();
		} catch (Exception e) {
		}
	}

}
