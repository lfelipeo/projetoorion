package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.AbaManualPageAttributes;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class AbaManualPage extends BasePage {

	AbaManualPageAttributes varPage = new AbaManualPageAttributes();

	/**
	 * Troco o foco da p�gina para o frame 'Agenda', espera a p�gina carregar
	 * totalmente com Javascript e clica na aba 'Manual'. Ap�s clicar espera o ajax
	 * call finalizar.
	 * 
	 * @throws Exception
	 */

	public void clicaAbaManual() throws Exception {

		try {

			getDSL().esperaPaginaCarregarJs();
			esperaAjaxEIcone();

			frameAgenda();

			//getWait().until(getDSL().esperaElementoSerVisivelFW(By.id(varPage.getBtnAbaManualId())));
			getDSL().clica(By.id(varPage.getBtnAbaManualId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NA ABA MANUAL.\r\n" + e.getMessage());
		}
	}

	/**
	 * Troca o foco da p�gina para o frame 'Or�amento', espera a tabela principal
	 * estar vi�svel e a ajax call finalizar. Depois preenche todos os campos da aba
	 * 'Manual': Part Number, Descri��o, M�o-De-Obra, Tempo, Pre�o Pe�a e Desconto.
	 * 
	 * @param part_numb
	 * @param descricao
	 * @param maoDeObra
	 * @param preco
	 * @param desconto
	 * @throws Exception
	 */

	public void selecionaPecaSubstituicao(String part_numb, String descricao, String maoDeObra, String preco,
			String desconto) throws Exception {

		frameOrcamento();
		esperaAjaxAbaManual();

		partNumberSubstituicao(part_numb);
		descricaoSubstituicao(descricao);
		maoDeObraSubstituicao(maoDeObra);
		tempoSubstituicao();
		precoPecaSubstituicao(preco);
		descontoSubstituicao(desconto);

	}

	private void partNumberSubstituicao(String part_numb) throws Exception {

		try {

			frameOrcamento();

			//getDSL().esperaElemento(By.id(varPage.getCampoTextoPartNumbManualOrcamentoId()));
			getDSL().escreveJsId(varPage.getCampoTextoPartNumbManualOrcamentoId(), part_numb);
			getDSL().esperaValor(By.id(varPage.getCampoTextoPartNumbManualOrcamentoId()), part_numb);

			esperaAjaxAbaManual();
			frameOrcamento();

			getDSL().clica(By.id(varPage.getBtnPesquisaPartNumbManualOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO PESQUISAR PART NUMBER EM SUBSTITUICAO.\r\n" + e.getMessage());
		}
	}

	private void descricaoSubstituicao(String descricao) throws Exception {

		try {

			esperaAjaxAbaManual();
			frameOrcamento();

			getDSL().escreveJsId(varPage.getCampoTextoDescricaoManualOrcamentoId(), descricao);
			getDSL().esperaValor(By.id(varPage.getCampoTextoDescricaoManualOrcamentoId()), descricao);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER DESCRICAO EM SUBSTITUICAO.\r\n" + e.getMessage());
		}
	}

	private void maoDeObraSubstituicao(String maoDeObra) throws Exception {

		try {

			getDSL().selecionaComboTexto(By.id(varPage.getComboMaoDeObraManualId()), maoDeObra);

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR MAO DE OBRA EM SUBSTITUICAO.\r\n" + e.getMessage());
		}
	}

	private void tempoSubstituicao() throws Exception {

		try {

			//getDSL().esperaElemento(By.id(varPage.getCampoTextoTempoManualId()));
			//getDSL().limparCampoId(varPage.getCampoTextoTempoManualId());
			getDSL().escreveJsId(varPage.getCampoTextoTempoManualId(), "1,00");
			getDSL().esperaValor(By.id(varPage.getCampoTextoTempoManualId()), "1,00");

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER TEMPO EM SUBSTITUICAO.\r\n" + e.getMessage());
		}
	}

	private void precoPecaSubstituicao(String preco) throws Exception {

		try {
			//getDSL().esperaElemento(By.id(varPage.getCampoTextoPrecoPecaManualId()));
			//getDSL().limparCampoId(varPage.getCampoTextoPrecoPecaManualId());
			getDSL().escreveJsId(varPage.getCampoTextoPrecoPecaManualId(), preco);
			getDSL().esperaValor(By.id(varPage.getCampoTextoPrecoPecaManualId()), preco);
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER PRECO PECA EM SUBSTITUICAO.\r\n" + e.getMessage());
		}
	}

	private void descontoSubstituicao(String desconto) throws Exception {

		try {

			//getDSL().esperaElemento(By.id(varPage.getCampoTextoDescontoManualId()));
			getDSL().limparCampoId(varPage.getCampoTextoDescontoManualId());
			getDSL().escreveJsId(varPage.getCampoTextoDescontoManualId(), desconto);
			getDSL().esperaValor(By.id(varPage.getCampoTextoDescontoManualId()), desconto);
			getDSL().clica(By.id(varPage.getTablePrincipalManualOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER DESCONTO EM SUBSTITUICAO.\r\n" + e.getMessage());
		}
	}

	public String opcaoSelecionadaOrigem() {

		String[] opcoes = { "ctl00_cph_Form_rdb_Origem_Original", "Original Genu�na", "ctl00_cph_Form_rdb_Origem_CIA",
				"Fornecido pela Cia", "ctl00_cph_Form_rdb_Origem_MA", "Original Gen�rica" };

		WebElement btn;

		for (int i = 0; i <= opcoes.length; i += 2) {

			btn = getDriver().findElement(By.id(opcoes[i]));

			if (btn.isSelected()) {
				return opcoes[i + 1];
			}
		}

		return null;
	}

	public void confirmaPecaSubstituicao() throws Exception {

		try {

			getDSL().rolaTabelaTodaJsId(varPage.getTablePrincipalManualOrcamentoId());
			getDSL().clica(By.id(varPage.getBtnAddPecaManualId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO ADICIONAR EM SUBSTITUICAO.\r\n" + e.getMessage());
		}
	}

	public void verificaTabelaDescricaoSubstituicao(String descricao) throws Exception {

		try {

			esperaAjaxAbaManual();
			frameOrcamento();

			getDSL().verificaTabelaTextoIgual(varPage.getTableDescricaoManualOrcamentoId(), descricao, 2);

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A PECA ADICIONADA EM SUBSTITUICAO.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera o ajax call finalizar e troca o foco da p�gina para o frame
	 * 'Or�amento'. Depois clicano bot�o 'Repara��o' na Aba 'Manual' na tela de
	 * Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaReparacaoAbaManual() throws Exception {

		try {

			esperaAjaxAbaManual();
			frameOrcamento();

			getDSL().clica(By.id(varPage.getBtnReparacaoManualOrcamentoId()));

			esperaAjaxAbaManual();
			frameOrcamento();

			// // Verifica se apos o clique do bot�o o campo descri��o fica visivel
			// getDSL().esperarEstarVisivelFW(varPage.get_campoDescricaoReparacaoOrcamentoManualId());

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR EM REPARACAO.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera o ajax call finalziar e troca o foco da p�gina para o frame
	 * 'Or�amento'. Preenche os campos: Descri��o e Tempo, em repara��o na aba
	 * 'Manual'.
	 * 
	 * @param descricao
	 * @throws Exception
	 */

	public void selecionaPecaReparacao(String descricao) throws Exception {

		try {

			esperaAjaxAbaManual();
			frameOrcamento();

		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR A TELA DE REPARACAO.\r\n" + e.getMessage());
		}

		descricaoReparacao(descricao);
		tempoReparacao();

	}

	private void descricaoReparacao(String descricao) throws Exception {

		try {

			//getDSL().esperaElemento(By.id(varPage.get_campoDescricaoReparacaoOrcamentoManualId()));
			getDSL().escreveJsId(varPage.get_campoDescricaoReparacaoOrcamentoManualId(), descricao);
			getDSL().esperaValor(By.id(varPage.get_campoDescricaoReparacaoOrcamentoManualId()), descricao);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER DESCRICAO EM REPARACAO.\r\n" + e.getMessage());
		}
	}

	private void tempoReparacao() throws Exception {

		try {

			//getDSL().esperaElemento(By.id(varPage.get_campoTempoReparacaoManualOrcamentoId()));
			getDSL().escreveJsId(varPage.get_campoTempoReparacaoManualOrcamentoId(), "1");
			getDSL().esperaValor(By.id(varPage.get_campoTempoReparacaoManualOrcamentoId()), "1");

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER TEMPO EM REPARACAO.\r\n" + e.getMessage());
		}
	}

	public void confirmaPecaReparacao(String descricao) throws Exception {

		try {

			getDSL().clica(By.id(varPage.get_btnAdicionarReparacaoManualOrcamentoId()));

			esperaAjaxAbaManual();

			frameOrcamento();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO ADICIONA EM REPARACAO.\r\n" + e.getMessage());
		}
	}

	public void verificaTabelaDescricaoReparacao(String descricao) throws Exception {

		try {

			esperaAjaxAbaManual();
			frameOrcamento();

			getDSL().verificaTabelaTextoIgual(varPage.getTableDescricaoManualOrcamentoId(), descricao, 2);

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A PECA ADICIONADA EM REPARACAO NA ABA MANUAL.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera o ajax call finalizar e troca o foco da p�gina para o frame
	 * 'Or�amento'. Clica no bot�o 'Pintura' na aba Manual.
	 * 
	 * @throws Exception
	 */

	public void clicaPinturaAbaManual() throws Exception {

		try {

			esperaAjaxAbaManual();
			frameOrcamento();

			getDSL().clica(By.id(varPage.get_btnPinturaManualOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO PINTURA.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera o ajax call finalizar e depois troca o foco da p�gina para o frame
	 * 'Or�amento'. Preenche os campos: Descri��o, N�vel, Superf�cie M2 e %
	 * Superf�cie.
	 * 
	 * @param descricao
	 * @param nivel
	 * @param superficieMDois
	 * @param porcentagemSuperficie
	 * @throws Exception
	 */

	public void selecionarPecaPinturaManualOrcamento(String descricao, String nivel, String superficieMDois,
			String porcentagemSuperficie) throws Exception {

		try {

			esperaAjaxAbaManual();
			frameOrcamento();

		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR A TELA DE PINTURA.\r\n" + e.getMessage());
		}

		descricaoPintura(descricao);
		nivelPintura(nivel);
		superficieM2Pintura(superficieMDois);
		porcentagemSuperficiePintura(porcentagemSuperficie);
	}

	private void descricaoPintura(String descricao) throws Exception {

		try {
			//getDSL().esperaElemento(By.id(varPage.get_campoDescricaoPinturaManualOrcamentoId()));
			getDSL().escreveJsId(varPage.get_campoDescricaoPinturaManualOrcamentoId(), descricao);
			getDSL().esperaValor(By.id(varPage.get_campoDescricaoPinturaManualOrcamentoId()), descricao);
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHE DESCRICAO EM PINTURA.\r\n" + e.getMessage());
		}

	}

	private void nivelPintura(String nivel) throws Exception {

		try {

			getDSL().selecionaComboValor(By.id(varPage.get_comboNivelPinturaManualOrcamentoId()), nivel);
			getDSL().esperaValor(By.id(varPage.get_comboNivelPinturaManualOrcamentoId()), nivel);

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR NIVEL EM PINTURA.\r\n" + e.getMessage());
		}
	}

	private void superficieM2Pintura(String superficieMDois) throws Exception {

		try {

			//getDSL().esperaElemento(By.id(varPage.get_campoSuperficieMDoisPinturaManualOrcamentoId()));
			getDSL().escreveJsId(varPage.get_campoSuperficieMDoisPinturaManualOrcamentoId(), superficieMDois);
			getDSL().esperaValor(By.id(varPage.get_campoSuperficieMDoisPinturaManualOrcamentoId()), superficieMDois);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER SUPERFICIE M2 EM PINTURA.\r\n" + e.getMessage());
		}
	}

	private void porcentagemSuperficiePintura(String porcentagemSuperficie) throws Exception {
		try {

			//getDSL().esperaElemento(By.id(varPage.get_campoPorcentagemSuperficiePinturaManualOrcamentoId()));
			getDSL().escreveJsId(varPage.get_campoPorcentagemSuperficiePinturaManualOrcamentoId(),
					porcentagemSuperficie);
			getDSL().esperaValor(By.id(varPage.get_campoPorcentagemSuperficiePinturaManualOrcamentoId()),
					porcentagemSuperficie);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER PORCENTAGEM SUPERFICIE EM PINTURA.\r\n" + e.getMessage());
		}
	}

	public void confirmarPecaPinturaManualOrcamento() throws Exception {

		try {

			getDSL().clica(By.id(varPage.getBtnAdicionarPinturaIdAbaManual()));

			esperaAjaxAbaManual();
			frameOrcamento();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO ADICIONAR EM PINTURA.\r\n" + e.getMessage());
		}
	}

	public void verificarTabelaDescricaoPinturaManualOrcamento(String descricao) throws Exception {

		try {

			esperaAjaxAbaManual();
			frameOrcamento();

			getDSL().verificaTabelaTextoIgual(varPage.getTableDescricaoManualOrcamentoId(), descricao, 2);

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A PECA ADICIONADA EM PINTURA NA ABA MANUAL.\r\n" + e.getMessage());
		}
	}

	/*****************/

	public void esperaAjaxAbaManual() throws Exception {

		try {
			getDSL().waitForAjaxCalls();
			while (!(getDSL().esperaElementoNaoEstarVisivel(By.id("div_progresso")))) {
				Thread.sleep(100);
			}
			framePrincipal();
		} catch (Exception e) {
		}
	}
}
