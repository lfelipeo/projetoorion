package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class AbaFotosPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();
	private int numeroAtualDeFotos = 0;

	/**
	 * Espera o ajax call finalizar, troca o foco da p�gina para o frame 'Agenda' e
	 * clica na aba 'Fotos' na tela de Or�amento. Apos a tela de fotos carregar
	 * totalmente, conta o n�mero de fotos j� adicionadas e atribui a v�riavel
	 * 'numeroAtualDeFotos'.
	 * 
	 * @throws Exception
	 */

	public void clicaAbaFotos() throws Exception {

		try {

			//esperaTelaOrcamentoVoltarAEstarVisivel();

			esperaAjaxEIcone();

			frameAgenda();

			getDSL().clica(By.id(varPage.get_btnFotosOrcamentoId()));

			frameOrcamento();

			numeroAtualDeFotos = getDSL().contarQtnTdTabelaId(By.id(varPage.getTableFotosAdicionadasAbaFotosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NA ABA FOTOS.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera a p�gina carregar totalmente com Javascript, espera o ajax call
	 * finalizar, troca o foco da p�gina para o frame 'Or�amento' e clica no bot�o
	 * 'Selecionar'.
	 * 
	 * @throws Exception
	 */

	public void clicaSelecionar() throws Exception {

		try {

			esperaAjaxEIcone();
			frameOrcamento();
			getWait().until(getDSL().esperaElementoSerVisivelFW(By.id(varPage.get_btnSelecionarFotosId())));

			System.out.println("Chegou");
			//getDSL().clica(By.xpath("//object[@id='SWFUpload_0']"));
			getDSL().clica(By.id("mup_Upload_tbl_Upload2_Botoes"));
			System.out.println("Passou do clique");
			getDSL().esperaAlert();
			
		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO SELECIONAR.\r\n" + e.getMessage());
		}

	}

	/**
	 * Espera a tela de sele��o de fotos do windows aparecer e escreve o nome da
	 * foto, depois confirmar.
	 * 
	 * @param nome_foto
	 * @throws Exception
	 */

	public void selecionaFoto(String nome_foto) throws Exception {

		try {

			String fileName = "C:\\Users\\lfelipeo\\Pictures\\imgtest.png";  
			String autoITExecutable = "autoit\\PhotoSelect.exe" + fileName;
			
			getDSL().esperaAlert();
			Runtime.getRuntime().exec(autoITExecutable);

			Thread.sleep(250);
			//esperaTelaOrcamentoVoltarAEstarVisivel();

		} catch (Exception e) {
			throw new Exception("ERRO AO INTERAGIR COM A JANELA DE EXPORTACAO DE FOTO.\r\n" + e.getMessage());
		}
	}

	/**
	 * Troca o foco da p�gina para o frame 'Or�amento', clica no bot�o 'Importar' e
	 * espera a p�gina carregar.
	 * 
	 * @throws Exception
	 */

	public void clicaImportar() throws Exception {

		try {

			Thread.sleep(250);
			esperaTelaOrcamentoVoltarAEstarVisivel();

			getDSL().esperaElemento(By.id(varPage.get_btnFotosOrcamentoId()));

			frameOrcamento();

			getDSL().clica(By.id(varPage.get_btnImportarFotosId()));
			esperaAjaxEIcone();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO IMPORTAR.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera a p�gina carregar totalmente por Javascript, espera o ajax call
	 * finalizar e troca o foco da p�gina para o frame 'Or�amento'. Conta a
	 * quantidades de fotos e verifica se possui uma a mais.
	 * 
	 * @throws Exception
	 */

	public void verificaSeFotoFoiAdicionada() throws Exception {

		try {

			frameOrcamento();

			getDSL().esperaTabelaConterUmTdAMais(numeroAtualDeFotos, varPage.getTableFotosAdicionadasAbaFotosId());

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE A FOTO FOI ADICIONADA.\r\n" + e.getMessage());
		}
	}
}
