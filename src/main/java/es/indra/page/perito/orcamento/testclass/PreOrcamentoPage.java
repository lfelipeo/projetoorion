package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import es.indra.base.BaseAction;
import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class PreOrcamentoPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();
	// private static String nomePecaPreOrcamento;

	public void abrePreOrcamento() throws Exception {

		try {

			esperaTelaOrcamentoVoltarAEstarVisivel();
			esperaAjaxEIcone();
			frameAgenda();

			getDSL().clica(By.id(varPage.get_btnPreOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO PRE ORCAMENTO.\r\n" + e.getMessage());
		}
	}

	public void escolheRegiaoPreOrcamento() throws Exception {

		try {

			esperaAjaxEIcone();
			frameModalUm();

			getDSL().clica(By.id(varPage.get_btnRegiaoDianteiraPreOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NA REGIAO DO CARRO.\r\n" + e.getMessage());
		}
	}

	public void clicaBtnCalcularPreOrcamento() throws Exception {

		try {

			getDSL().clica(By.id(varPage.get_btnCalcularPreOrcamentoId()));

		} catch (Exception e) {
			throw new Exception(
					"ERRO AO CLICAR NO BOTAO CALCULAR PRE ORCAMENTO EM PRE ORCAMENTO.\r\n" + e.getMessage());
		}
	}

	public void escolhePecaTelaPreOrcamento() throws Exception {

		try {

			esperaAjaxEIcone();
			frameModalUm();

			getDSL().clica(By.id("ctl00_cph_Form_chk_1"));

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR A OPCAO DE PECA NA TELA PRE ORCAMENTO.\r\n" + e.getMessage());
		}

	}

	public void clicaBtnAdicionarTelaPreOrcamento() throws Exception {

		try {

			esperaAjaxEIcone();
			frameModalUm();

			getDSL().clica(By.id(varPage.get_btnAdicionarPecasPreOrcamentoId()));

		} catch (Exception e) {
			throw new Exception(
					"ERRO AO CLICAR NO BOTAO ADICIONAR EM PECAS NA TELA PRE ORCAMENTO.\r\n" + e.getMessage());
		}
	}

	public void clicaBtnAtualizarTelaPreOrcamento() throws Exception {

		try {

			// Troca o foco para o frame Modal Um
			frameModalUm();

			getDSL().esperaElementoNaoEstarVisivel(By.id("tbl_Titulo_2"));

			// Espera o bot�o 'Atualizar' estar visivel
			getDSL().clica(By.id(varPage.get_btnAtualizarPreOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO ATUALIZAR.\r\n" + e.getMessage());
		}

		try {

			String preSucess = "O pr�-or�amento foi realizado com sucesso.";

			// Espera a pagina carregar totalmente
			getDSL().esperarAlert();

			while (!getDSL().pegarTextoAlert().equalsIgnoreCase(preSucess)) {
				getDSL().esperarAlert();
				// String texto = getDSL().pegarTextoAlert();
				BaseAction.tirarPrint(
						getDSL().removeCaracterEspecial(getDSL().atePrimeiroPonto(getDSL().pegarTextoAlert())));
				getDSL().apertarEnterAlert();
			}

		} catch (Exception e) {
			throw new Exception("ERRO AO CONFIRMAR O ALERTA APOS CLICAR NO BOTAO ATUALIZAR. Erro: " + e.getMessage());
		}
	}

	public void selecionaRegiaoCarroTelaPecasPreOrcamento() throws Exception {

		try {

			frameModalDois();
			getDSL().clicarPecaCarro();

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR A REGIAO DO CARRO NA TELA PECAS.\r\n" + e.getMessage());
		}
	}

	public void selecionaListaPecasServicosPreOrcamento() throws Exception {

		try {

			esperaAjaxEIcone();
			frameModalDois();

			// Escolhe de forma aleat�ria da Pe�a em 'Lista'
			getDSL().clicarOpcaoListaPeca();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NA LISTA DE PECAS_SERVICOS.\r\n" + e.getMessage());
		}
	}

	// public void clicaBtnAdicionarListaPecasPreOrcamento() throws Exception {
	//
	// try {
	//
	// esperaAjaxEIcone();
	// frameModalDois();
	//
	// // Espera o bot�o 'Adicionar' estar visivel na tela pe�as
	// getDSL().clica(By.id(varPage.get_btnAdicionarOrcamentoId()));
	//
	//// esperaAjaxEIcone();
	////
	//// if (getDSL().verificarAlertVisivel()) {
	//// getDSL().confirmarAlert();
	//// }
	//
	// esperaAjaxEIcone();
	// frameModalDois();
	//
	// // Enquanto houver alert na tla
	// while (getDSL().verificarAlertVisivel()) {
	// // Confirma o alert
	// getDSL().apertarEnterAlert();
	// getDSL().waitForAjaxCalls();
	// }
	//
	// acaoAposBotaoAdicionar();
	//
	// } catch (Exception e) {
	// throw new Exception("ERRO AO CLICA NO BOTAO ADICIONAR.\r\n" +
	// e.getMessage());
	// }
	//
	// }

	public void clicaBtnAdicionarListaPecasPreOrcamento() throws Exception {

		try {

			frameModalDois();
			getDSL().clica(By.id(varPage.getBtnAdicionarOrcamentoId()));

			getDSL().esperaPaginaCarregarJs();
			esperaAjaxEIcone();

			getDSL().verificarAlertVisivel();

			esperaAlertOuElemento();

			acaoAposBotaoAdicionar();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO ADICIONAR.\r\n" + e.getMessage());
		}

	}

	/***************************/

	public void acaoAposBotaoAdicionar() throws Exception {

		try {

			if (getDSL().verificarAlertVisivel()) {
				while (getDSL().verificarAlertVisivel()) {
					getDSL().apertarEnterAlert();
				}
			}

			else if (getDSL().verificaVisibilidadeBoolean(By.id(varPage.get_tablePrecoPecaBaremoOrcamentoId()))) {

				esperaAjaxEIcone();
				frameModalDois();

				getDSL().clicarPrecoPeca(varPage.get_tablePrecoPecaBaremoOrcamentoId());
				getDSL().clica(By.id(varPage.get_btnConfirmarPrecoPecaBaremoOrcamentoId()));

			}

			getDSL().esperaPaginaCarregarJs();
			esperaAjaxEIcone();

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR O PRECO DA PECA.\r\n" + e.getMessage());
		}
	}

	// private void acaoAposBotaoAdicionar() throws Exception {
	//
	// /*
	// * Executa as a��es que forem necess�rias depois de clicar no bot�o
	// 'Adicionar'.
	// */
	//
	// try {
	//
	// esperaAjaxEIcone();
	//
	// // Enquanto houver alert na tla
	// while (getDSL().verificarAlertVisivel()) {
	// // Confirma o alertS
	// getDSL().apertarEnterAlert();
	// }
	// } catch (Exception e) {
	// throw new Exception("ERRO AO CONFIRMAR O ALERT APOS O BOTAO ADICIONAR.\r\n" +
	// e.getMessage());
	// }
	//
	// try {
	// esperaAjaxEIcone();
	// } catch (Exception e) {
	// throw new Exception("ERRO AO VERIFICAR SE A TELA DE PRECOS DA PECA ESTA
	// VISIVEL.\r\n" + e.getMessage());
	// }
	//
	// try {
	//
	// // Verifica se a tela 'Pre�o da Pe�a' esta visivel
	// if
	// (getDSL().verificaVisibilidadeBoolean(By.id(varPage.get_popupPrecoPecaOrcamentoId())))
	// {
	// voltaTempoDeEspera();
	//
	// frameModalDois();
	// // Verifica se a tabela principal esta visivel
	// if
	// (getDSL().verificaVisibilidadeBoolean(By.id(varPage.get_tablePrecoPecaBaremoOrcamentoId())))
	// {
	// voltaTempoDeEspera();
	// getDSL().clicarPrecoPeca(varPage.get_tablePrecoPecaBaremoOrcamentoId());
	// // Espera o bot�o 'Confirmar' em 'Pre�o Pe�a' estar visivel
	// getDSL().clica(By.id(varPage.get_btnConfirmarPrecoPecaBaremoOrcamentoId()));
	// }
	// }
	// } catch (Exception e) {
	// throw new Exception("ERRO AO SELECIONAR O PRECO DA PECA.\r\n" +
	// e.getMessage());
	// }
	// }

	/*****************************************/

	public void esperaAlertOuElemento() throws Exception {

		try {
			getWait().until(ExpectedConditions.or(esperaJanelaPrecos(), ExpectedConditions.alertIsPresent()));
		} catch (Exception e) {
			throw new Exception("OCORREU UM ERRO NA ESPERA DO ALERT OU DA JANELA DE PRECOS.\r\n" + e.getMessage());
		}
	}

	public ExpectedCondition<WebElement> esperaJanelaPrecos() {

		return new ExpectedCondition<WebElement>() {

			@Override
			public WebElement apply(WebDriver driver) {
				try {
					frameModalDois();
					return getDSL().verificaVisibilidade(By.id("ctl00_cph_Form_div_Lista"));
				} catch (Exception e) {
					return null;
				}
			}
		};
	}

	public boolean verificarAlertVisivel() {
		return getDSL().verificarAlertVisivel();
	}

	public void confirmarAlert() throws Exception {
		getDSL().apertarEnterAlert();
	}
}
