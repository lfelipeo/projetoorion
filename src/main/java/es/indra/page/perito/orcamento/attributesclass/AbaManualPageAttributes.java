package es.indra.page.perito.orcamento.attributesclass;

public class AbaManualPageAttributes {

	private final String BTN_MANUAL_ORCAMENTO_ID = "tab_Orcamento_1";
	private final String TABLE_PRINCIPAL_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_div_Substituicao";
	private final String TABLE_DESCRICAO_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_gvw_Padrao";

	private final String CAMPO_PART_NUMB_MANUAL_ID = "ctl00_cph_Form_txt_Cd_Part_Number";
	private final String BTN_PESQUISAR_PART_NUMB_MANUAL_ID = "ctl00_cph_Form_img_VerificarPN";
	private final String CAMPO_DESCRICAO_MANUAL_ID = "ctl00_cph_Form_txt_Ds_Peca_S";
	private final String COMBO_MAO_DE_OBRA_MANUAL_ID = "ctl00_cph_Form_ddl_Tp_Mao_Obra";
	private final String CAMPO_TEMPO_MANUAL_ID = "ctl00_cph_Form_txt_Qt_Tempo_S";
	private final String CAMPO_PRECO_PECA_MANUAL_ID = "ctl00_cph_Form_txt_Vl_Preco_Bruto";
	private final String CAMPO_DESCONTO_MANUAL_ID = "ctl00_cph_Form_txt_Pc_Desconto";
	private final String BTN_ADICIONAR_PECA_MANUAL_ID = "ctl00_cph_Form_ibt_Adicionar_Substituicao";

	private final String BTN_REPARACAO_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_btn_Reparacao"; // "ctl00_cph_FormBTN_Reparacao";
	private final String CAMPO_DESCRICAO_REPARACAO_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_txt_Ds_Peca_R";
	private final String CAMPO_TEMPO_REPARACAO_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_txt_Qt_Tempo_R";
	private final String BTN_ADICIONAR_REPARACAO_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_ibt_Adicionar_Reparacao";
	private final String TABLE_PECAS_ADICIONADAS_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_gvw_Padrao";

	private final String BTN_PINTURA_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_btn_Pintura"; // "ctl00_cph_FormBTN_Pintura";
	private final String CAMPO_DESCRICAO_PINTURA_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_txt_Ds_Peca_P";
	private final String COMBO_NIVEL_PINTURA_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_ddl_Cd_Nivel_Pint";
	private final String CAMPO_SUPERFICIE_MDOIS_PINTURA_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_txt_Qt_Superficie";
	private final String CAMPO_PORCENTAGEM_SUPERFICIE_PINTURA_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_txt_Pc_Superficie";
	private final String BTN_ADICIONAR_PINTURA_MANUAL_ORCAMENTO_ID = "ctl00_cph_Form_ibt_Adicionar_Pintura";

	public String getBtnAbaManualId() {
		return BTN_MANUAL_ORCAMENTO_ID;
	}

	public String getTableDescricaoManualOrcamentoId() {
		return TABLE_DESCRICAO_MANUAL_ORCAMENTO_ID;
	}

	public String getCampoTextoPartNumbManualOrcamentoId() {
		return CAMPO_PART_NUMB_MANUAL_ID;
	}

	public String getBtnPesquisaPartNumbManualOrcamentoId() {
		return BTN_PESQUISAR_PART_NUMB_MANUAL_ID;
	}

	public String getCampoTextoDescricaoManualOrcamentoId() {
		return CAMPO_DESCRICAO_MANUAL_ID;
	}

	public String getComboMaoDeObraManualId() {
		return COMBO_MAO_DE_OBRA_MANUAL_ID;
	}

	public String getCampoTextoTempoManualId() {
		return CAMPO_TEMPO_MANUAL_ID;
	}

	public String getCampoTextoPrecoPecaManualId() {
		return CAMPO_PRECO_PECA_MANUAL_ID;
	}

	public String getCampoTextoDescontoManualId() {
		return CAMPO_DESCONTO_MANUAL_ID;
	}

	public String getBtnAddPecaManualId() {
		return BTN_ADICIONAR_PECA_MANUAL_ID;
	}

	public String getTablePrincipalManualOrcamentoId() {
		return TABLE_PRINCIPAL_MANUAL_ORCAMENTO_ID;
	}

	public String getBtnReparacaoManualOrcamentoId() {
		return BTN_REPARACAO_MANUAL_ORCAMENTO_ID;
	}

	public String get_campoDescricaoReparacaoOrcamentoManualId() {
		return CAMPO_DESCRICAO_REPARACAO_MANUAL_ORCAMENTO_ID;
	}

	public String get_campoTempoReparacaoManualOrcamentoId() {
		return CAMPO_TEMPO_REPARACAO_MANUAL_ORCAMENTO_ID;
	}

	public String get_btnAdicionarReparacaoManualOrcamentoId() {
		return BTN_ADICIONAR_REPARACAO_MANUAL_ORCAMENTO_ID;
	}

	public String get_tablePecasAdicionadasManualOrcamentoId() {
		return TABLE_PECAS_ADICIONADAS_MANUAL_ORCAMENTO_ID;
	}

	public String get_btnPinturaManualOrcamentoId() {
		return BTN_PINTURA_MANUAL_ORCAMENTO_ID;
	}

	public String get_campoDescricaoPinturaManualOrcamentoId() {
		return CAMPO_DESCRICAO_PINTURA_MANUAL_ORCAMENTO_ID;
	}

	public String get_comboNivelPinturaManualOrcamentoId() {
		return COMBO_NIVEL_PINTURA_MANUAL_ORCAMENTO_ID;
	}

	public String get_campoSuperficieMDoisPinturaManualOrcamentoId() {
		return CAMPO_SUPERFICIE_MDOIS_PINTURA_MANUAL_ORCAMENTO_ID;
	}

	public String get_campoPorcentagemSuperficiePinturaManualOrcamentoId() {
		return CAMPO_PORCENTAGEM_SUPERFICIE_PINTURA_MANUAL_ORCAMENTO_ID;
	}

	public String getBtnAdicionarPinturaIdAbaManual() {
		return BTN_ADICIONAR_PINTURA_MANUAL_ORCAMENTO_ID;
	}

}
