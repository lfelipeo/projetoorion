package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class BotaoFotosPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();

	/**
	 * Espera o ajax call finalizar, troco o foco da p�gina para o frame 'Agenda' e
	 * clica no bot�o 'Fotos' na tela de Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaFotosBotao() throws Exception {

		try {

			esperaTelaOrcamentoVoltarAEstarVisivel();
			esperaAjaxEIcone();

			frameAgenda();

			getDSL().clica(By.id(varPage.get_btnFotosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO FOTOS.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera o ajax call finalizar e troca de janela.
	 * 
	 * @throws Exception
	 */

	public void trocaJanelaFotos() throws Exception {

		try {

			esperaAjaxEIcone();
			getDSL().trocarJanelaFocoCompleto("http://valida.orionbr.com.br/tokio.orcamentos.v6/Orcamento/Fotos/");
			getDSL().esperaPaginaCarregarJs();

		} catch (Exception e) {
			throw new Exception("ERRO AO TROCAR DE JANELA PARA A JANELA DE FOTOS.\r\n" + e.getMessage());
		}
	}

	/**
	 * Pega a identifica��o da janela.
	 * 
	 * @return id window
	 * @throws Exception
	 */

	public String pegaIdJanela() throws Exception {
		try {
			return getDSL().pegaWindowHandle();
		} catch (Exception e) {
			throw new Exception("ERRO AO PEGAR O ID DA JANELA.\r\n" + e.getMessage());
		}
	}

	/**
	 * Fecha a segunda janela e volta o foco para a janela principal.
	 * 
	 * @param janela_principal
	 * @throws Exception
	 */

	public void fechaSegundaTela(String janela_principal) throws Exception {
		try {

			getDriver().close();
			getDriver().switchTo().window(janela_principal);

		} catch (Exception e) {
			throw new Exception("ERRO AO FECHAR A SEGUNDA JANELA.\r\n" + e.getMessage());
		}
	}
}
