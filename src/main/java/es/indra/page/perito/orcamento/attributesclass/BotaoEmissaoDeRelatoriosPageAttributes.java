package es.indra.page.perito.orcamento.attributesclass;

public class BotaoEmissaoDeRelatoriosPageAttributes {

	private final String BTN_EMISSAO_RELATORIOS_ID = "ctl00_cph_Botoes_img_Relatorios";
	
	private final String BTN_ORDENAR_POR_DESCRICAO_EMISSAO_DE_RELATORIOS_ID = "ctl00_cph_Rodape_ibt_OrdemDesc"; // "ctl00_cph_Rodape_ibt_OrdemDesc";
	private final String BTN_SERVICO_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_Servico";// "ctl00_cph_RodapeBTN_Servico";
	private final String BTN_SALVADO_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_Salvado";// "ctl00_cph_RodapeBTN_Salvado";
	private final String BTN_PMG_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_PMG";// "ctl00_cph_RodapeBTN_PMG";
	
	private final String BTN_FOTO_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_Foto";// "ctl00_cph_RodapeBTN_Foto";
	private final String TABLE_FOTO_EMISSAO_RELATORIOS_ID = "ctl00_upn_Form";
	private final String BTN_FECHA_FOTO_EMISSAO_RELATORIOS_ID = "img_Fechar5";

	private final String BTN_HISTORICO_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_Historico";// "ctl00_cph_RodapeBTN_Historico";
	private final String BTN_REVISAO_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_Revisao";// "ctl00_cph_RodapeBTN_Revisao";
	private final String BTN_CONFIRMAR_REVISAO_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_Confirmar";// "ctl00_cph_RodapeBTN_Confirmar";
	private final String BTN_ACESSO_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_Acesso";// "ctl00_cph_RodapeBTN_Acesso";
	private final String BTN_CONNECT_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_Connect";// "ctl00_cph_RodapeBTN_Connect";
	private final String BTN_AVARIAS_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_Avarias";// "ctl00_cph_RodapeBTN_Avarias";
	private final String BTN_FOTOS_AVARIAS_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_FotosAvarias";// "ctl00_cph_RodapeBTN_FotosAvarias";

	private final String BTN_PECA_EMISSAO_RELATORIOS_ID = "ctl00_cph_Rodape_btn_Peca";// "ctl00_cph_RodapeBTN_Peca";
	private final String BTN_FECHAR_EMISSAO_RELATORIOS_ID = "img_Fechar";
	private final String TITLE_RELATORIOS_ID = "lbl_TituloPagina_1";

	public String getBtnEmissaoRelatoriosId() {
		return BTN_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnOrdenarPorDescricaoEmissaoDeRelatoriosId() {
		return BTN_ORDENAR_POR_DESCRICAO_EMISSAO_DE_RELATORIOS_ID;
	}

	public String getBtnFecharEmissaoRelatoriosId() {
		return BTN_FECHAR_EMISSAO_RELATORIOS_ID;
	}

	public String get_titleRelatoriosId() {
		return TITLE_RELATORIOS_ID;
	}

	public String getBtnPecaEmissaoRelatoriosId() {
		return BTN_PECA_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnServicoEmissaoRelatoriosId() {
		return BTN_SERVICO_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnSalvadoEmissaoRelatoriosId() {
		return BTN_SALVADO_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnPmgEmissaoRelatoriosId() {
		return BTN_PMG_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnFotoEmissaoRelatoriosId() {
		return BTN_FOTO_EMISSAO_RELATORIOS_ID;
	}

	public String getTableFotoEmissaoRelatoriosId() {
		return TABLE_FOTO_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnFechaFotoEmissaoRelatoriosId() {
		return BTN_FECHA_FOTO_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnHistoricoEmissaoRelatoriosId() {
		return BTN_HISTORICO_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnRevisaoEmissaoRelatoriosId() {
		return BTN_REVISAO_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnConfirmarRevisaoEmissaoRelatoriosId() {
		return BTN_CONFIRMAR_REVISAO_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnAcessoEmissaoRelatoriosId() {
		return BTN_ACESSO_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnConnectEmissaoRelatoriosId() {
		return BTN_CONNECT_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnAvariasEmissaoRelatoriosId() {
		return BTN_AVARIAS_EMISSAO_RELATORIOS_ID;
	}

	public String getBtnFotosAvariasEmissaoRelatoriosId() {
		return BTN_FOTOS_AVARIAS_EMISSAO_RELATORIOS_ID;
	}

}
