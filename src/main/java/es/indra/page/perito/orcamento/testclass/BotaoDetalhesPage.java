package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class BotaoDetalhesPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();

	public void clicaBotaoDetalhes() throws Exception {

		try {

			//esperaAjaxEIcone();
			//esperaTelaOrcamentoVoltarAEstarVisivel();

			esperaAjaxEIcone();
			frameAgenda();

			getWait().until(getDSL().esperaElementoSerVisivelFW(By.id(varPage.get_btnDetalhesOrcamentoId())));
			getDSL().clica(By.id(varPage.get_btnDetalhesOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO DETALHES.\r\n" + e.getMessage());
		}

	}

	public void verificaPlacaDetalhesOrcamento(String placa) throws Exception {

		try {

			frameModalUm();
			getDSL().esperaElemento(By.id(varPage.get_telaTextoDetalhesOrcamentoId()));

			String partesPlaca[] = placa.split("-");
			String placaFull = "" + partesPlaca[0] + "" + partesPlaca[1];

			getDSL().verificarCampoContemTextoId(varPage.get_telaTextoDetalhesOrcamentoId(), placaFull);

		} catch (Exception e) {
			throw new Exception(
					"ERRO AO VERIFICAR SE A PLACA (" + placa + ") ENCONTRA-SE EM DETALHES.\r\n" + e.getMessage());
		}
	}

	public void fechaDetalhesOrcamento() throws Exception {

		try {

			esperaAjaxEIcone();
			framePrincipal();
			getDSL().clica(By.id(varPage.get_btnFecharDetalhesOrcamentoId()));

			esperaTelaOrcamentoVoltarAEstarVisivel();
		} catch (Exception e) {
			throw new Exception("ERRO AO FECHAR DETALHES. Selenium Erro: " + e.getMessage());
		}
	}
}
