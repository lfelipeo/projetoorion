package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class BotaoListaDePecasPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();

	/**
	 * Troca o foco da p�gina para o frame 'Agenda' e clica no bot�o 'Lista de
	 * Pe�as'.
	 * 
	 * @throws Exception
	 */

	public void abreListaDePecas() throws Exception {
		try {

			//esperaTelaOrcamentoVoltarAEstarVisivel();
			esperaAjaxEIcone();

			frameAgenda();

			getDSL().clica(By.id(varPage.get_btnPecasOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO LISTA DE PECAS.\r\n" + e.getMessage());
		}
	}

	/**
	 * Verifica se as pe�a(s) escolhida na aba 'Manual' foi adicionada com sucesso e
	 * est� na 'Lista de Pe�as'. Procura a pe�a na coluna de indice 9 (Descri��o)
	 * 
	 * @param descricao
	 * @throws Exception
	 */

	public void verificaPecaManual(String descricao) throws Exception {

		try {

			frameModalUm();
			getDSL().verificaTabelaContemTextoId(varPage.get_tableListaPecasId(), descricao, 9);

		} catch (Exception e) {
			throw new Exception(
					"ERRO AO VERIFICAR SE A LISTA DE PECAS CONTEM A PECA (" + descricao + ").\r\n" + e.getMessage());
		}
	}

	public void verificaPecaBaremo(String descricao) throws Exception {

		try {

			frameModalUm();
			getDSL().comparaTextoComTextoDaTabela(varPage.get_tableListaPecasId(), descricao, 9);

		} catch (Exception e) {
			throw new Exception(
					"ERRO AO VERIFICAR SE A LISTA DE PECAS CONTEM (" + descricao + ").\r\n" + e.getMessage());
		}

	}

	public void fecharListaDePecasOrcamento() throws Exception {

		try {

			framePrincipal();
			
			getDSL().clica(By.xpath("//*[@id='img_Fechar']"));

			esperaTelaOrcamentoVoltarAEstarVisivel();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO FECHAR.\r\n" + e.getMessage());
		}
	}

	public boolean verificarTelaOrcamentoVisivel() throws Exception {

		try {

			framePrincipal();
			getDSL().esperarElementoEstarVisivelId("frm_Main");
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
