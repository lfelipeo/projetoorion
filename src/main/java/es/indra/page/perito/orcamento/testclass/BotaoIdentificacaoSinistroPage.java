package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class BotaoIdentificacaoSinistroPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();

	public void clicaBotaoIdentificacaoSinistro() throws Exception {

		try {

			//esperaTelaOrcamentoVoltarAEstarVisivel();
			esperaAjaxEIcone();

			frameAgenda();

			getDSL().esperaElemento(By.id(varPage.get_btnIdentificacaoSinistroId()));
			getDSL().clica(By.id(varPage.get_btnIdentificacaoSinistroId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO DE IDENTIFICAÇÃO DE SINISTRO.\r\n" + e.getMessage());
		}
	}

	public void verificarPlaca(String placa) throws Exception {

		try {

			esperaAjaxEIcone();
			frameModalUm();

			getDSL().esperaTexto(By.id(varPage.get_campoTextoPlacaIdentificacaoSinistroId()), placa);

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A PLACA EM IDENTIFICACAO DO SINISTRO.\r\n" + e.getMessage());
		}
	}

	public void fechaIdentificacaoSinistro() throws Exception {

		try {

			esperaAjaxEIcone();
			framePrincipal();
			getDSL().clica(By.id("img_Fechar"));

			esperaTelaOrcamentoVoltarAEstarVisivel();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO FECHAR.\r\n" + e.getMessage());
		}
	}

}
