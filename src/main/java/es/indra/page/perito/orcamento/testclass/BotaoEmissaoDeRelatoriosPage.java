package es.indra.page.perito.orcamento.testclass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import es.indra.atributos.TestAttributes;
import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.BotaoEmissaoDeRelatoriosPageAttributes;
import es.indra.step.perito.orcamento.BotaoEmissaoDeRelatoriosStep;

public class BotaoEmissaoDeRelatoriosPage extends BasePage {

	private BotaoEmissaoDeRelatoriosPageAttributes varPage = new BotaoEmissaoDeRelatoriosPageAttributes();
	private TestAttributes testAt = new TestAttributes();

	/**
	 * Espera o ajax call finalizar, troca o foco da p�gina para o frame 'Agenda' e
	 * clica no bot�o 'Emiss�o de Rel�torios', depois espera a tela abrir.
	 * 
	 * @throws Exception
	 */

	public void clicaEmissaoDeRelatorios() throws Exception {

		try {

			//esperaTelaOrcamentoVoltarAEstarVisivel();
			esperaAjaxEIcone();

			frameAgenda();

			getDSL().clica(By.id(varPage.getBtnEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO EMISSAO DE RELATORIOS.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'Ordenar por Descri��o' em Emiss�o de Rel�torios
	 * na tela de Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaOrdenarPorDescricao() throws Exception {

		try {

			esperaAjaxEIcone();
			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnOrdenarPorDescricaoEmissaoDeRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO ORDENAR POR DESCRICAO.\r\n" + e.getMessage());
		}
	}

	/**
	 * Verifica na tabela de pe�as 'Substitui��o' se contem a pe�a adicionada.
	 * 
	 * @param peca
	 * @return
	 * @throws Exception
	 */

	public boolean verificaSubstituicaoOrdenarPorDescricao(String peca) throws Exception {

		try {

			/**
			 * Caso a seguradora seja Mitsui, a tabela 'Substitui��o' se encontra no indice
			 * 9.
			 */

			if (testAt.getSeguradora().equalsIgnoreCase("Mitsui"))
				return procuraDentroDaTabelaEmEmissaoDeRelatorios(9, peca, 1);

			/**
			 * Caso a seguradora seja Tokio, a tabela 'Substitui��o' se encontra no indice
			 * 11.
			 */

			else if (testAt.getSeguradora().equalsIgnoreCase("Tokio"))
				return procuraDentroDaTabelaEmEmissaoDeRelatorios(11, peca, 1);

			else
				return false;

		} catch (Exception e) {
			throw new Exception(
					"ERRO AO VERIFICAR A PECA EM SUBSTITUICAO NA JANELA ORDENAR POR DESCRICAO.\r\n" + e.getMessage());
		}
	}

	public boolean verificaDesmontagemMontagemOrdenarPorDescricao(String peca) throws Exception {
		try {

			if (testAt.getSeguradora().equalsIgnoreCase("Mitsui"))
				return procuraDentroDaTabelaEmEmissaoDeRelatorios(10, peca, 1);

			else if (testAt.getSeguradora().equalsIgnoreCase("Tokio"))
				return procuraDentroDaTabelaEmEmissaoDeRelatorios(12, peca, 1);

			else
				return false;

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A PECA EM DESMONTAGEM MONTAGEM NA JANELA ORDENAR POR DESCRICAO.\r\n"
					+ e.getMessage());
		}
	}

	public boolean verificaReparacaoOrdenarPorDescricao(String peca) throws Exception {
		try {

			if (testAt.getSeguradora().equalsIgnoreCase("Mitsui"))
				return procuraDentroDaTabelaEmEmissaoDeRelatorios(11, peca, 1);

			else if (testAt.getSeguradora().equalsIgnoreCase("Tokio"))
				return procuraDentroDaTabelaEmEmissaoDeRelatorios(13, peca, 1);

			else
				return false;

		} catch (Exception e) {
			throw new Exception(
					"ERRO AO VERIFICAR A PECA EM REPARACAO NA JANELA ORDENAR POR DESCRICAO.\r\n" + e.getMessage());
		}
	}

	public boolean verificaPinturaOrdenarPorDescricao(String peca) throws Exception {
		try {

			if (testAt.getSeguradora().equalsIgnoreCase("Mitsui"))
				return procuraDentroDaTabelaEmEmissaoDeRelatorios(12, peca, 1);

			else if (testAt.getSeguradora().equalsIgnoreCase("Tokio"))
				return procuraDentroDaTabelaEmEmissaoDeRelatorios(14, peca, 1);

			else
				return false;

		} catch (Exception e) {
			throw new Exception(
					"ERRO AO VERIFICAR A PECA EM REPARACAO NA JANELA ORDENAR POR DESCRICAO.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'Pe�a' em Emiss�o de Rel�torios na tela de
	 * Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaPeca() throws Exception {

		try {

			getDSL().esperaPaginaCarregarJs();
			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnPecaEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO PECA.\r\n" + e.getMessage());
		}
	}

	public void trocaParaJanelaPeca(String url_janela) throws Exception {

		try {
			getDSL().trocarJanelaFocoCompleto(url_janela);
		} catch (Exception e) {
			throw new Exception("ERRO AO TROCAR PARA A JANELA DE PECAS.\r\n" + e.getMessage());
		}
	}

	public boolean verificaOriginalGenuinaPeca(String peca) throws Exception {

		try {
			return procuraDentroDaTabelaEmEmissaoDeRelatorios(8, peca, 1);
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A PECA EM ORIGINAL GENUICA NA JANELA PECA.\r\n" + e.getMessage());
		}
	}

	public boolean verificaFornecidoPelaCiaPeca(String peca) throws Exception {

		try {
			return procuraDentroDaTabelaEmEmissaoDeRelatorios(9, peca, 1);
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A PECA EM FORNECIDO PELA CIA NA JANELA PECA.\r\n" + e.getMessage());
		}
	}

	public boolean verificaOriginalGenericaPeca(String peca) throws Exception {
		try {
			return procuraDentroDaTabelaEmEmissaoDeRelatorios(10, peca, 1);
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A PECA EM ORIGINAL GENERICA NA JANELA PECA.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'Servi�o' em Emiss�o de Rel�torios na tela de
	 * Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaServico() throws Exception {

		try {

			getDSL().esperaPaginaCarregarJs();
			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnServicoEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO SERVICO.\r\n" + e.getMessage());
		}
	}

	public void trocaParaJanelaServico(String url_janela) throws Exception {

		try {
			getDSL().trocarJanelaFocoCompleto(url_janela);
		} catch (Exception e) {
			throw new Exception("ERRO AO TROCAR PARA A JANELA DE PECAS.\r\n" + e.getMessage());
		}
	}

	public boolean verificaPecaServico(String peca) throws Exception {

		try {
			return procuraDentroDaTabelaEmEmissaoDeRelatorios(6, peca, 2);
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A PECA NA COLUNA PECA NA JANELA SERVICO.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'Salvado' em Emiss�o de Rel�torios na tela de
	 * Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaSalvado() throws Exception {

		try {

			getDSL().esperaPaginaCarregarJs();
			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnSalvadoEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO SALVADO.\r\n" + e.getMessage());
		}
	}

	public boolean verificaNumeroPecaSinistroSalvado(String placa) {
		try {
			return procuraDentroDaTabelaEmEmissaoDeRelatorios(4, placa, 1);
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'PMG' em Emiss�o de Rel�torios na tela de
	 * Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaPmg() throws Exception {

		try {

			getDSL().esperaPaginaCarregarJs();
			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnPmgEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO PMG.\r\n" + e.getMessage());
		}
	}

	public boolean verificaNumeroPlacaSinistroPmg(String placa) {

		try {
			return procuraDentroDaTabelaEmEmissaoDeRelatorios(1, placa, 2);
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'Foto' em Emiss�o de Rel�torios na tela de
	 * Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaFoto() throws Exception {

		try {

			getDSL().esperaPaginaCarregarJs();
			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnFotoEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO FOTO.\r\n" + e.getMessage());
		}
	}

	public void verificaTableFoto() throws Exception {

		try {

			frameModalCinco();
			getDSL().esperaElemento(By.id(varPage.getTableFotoEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A TABELA DE FOTO.\r\n" + e.getMessage());
		}
	}

	public void fechaFoto() throws Exception {

		try {

			framePrincipal();
			getDSL().clica(By.id(varPage.getBtnFechaFotoEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR EM FECHAR.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'Hist�rico' em Emiss�o de Rel�torios na tela de
	 * Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaHistorico() throws Exception {

		try {

			getDSL().esperaPaginaCarregarJs();
			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnHistoricoEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO FOTO.\r\n" + e.getMessage());
		}
	}

	public void verificaJanelaOuAlert(String url) throws Exception {

		String[] janelas = getDriver().getWindowHandle().split(",");
		int qtnJanelas = janelas.length + 1;

		getWait().until(ExpectedConditions.or(ExpectedConditions.alertIsPresent(),
				ExpectedConditions.numberOfWindowsToBe(qtnJanelas)));

		if (getDSL().verificarAlertVisivel()) {
			BotaoEmissaoDeRelatoriosStep.condHistorico = false;
			getDSL().confirmarAlert();
		}

		else {
			getDSL().trocarJanelaFocoCompleto(url);
			BotaoEmissaoDeRelatoriosStep.condHistorico = true;
		}
	}

	public void verificaNumeroPlacaSinistroHistorico(String placa) throws Exception {

		try {

			if (BotaoEmissaoDeRelatoriosStep.condHistorico)
				procuraDentroDaTabelaEmEmissaoDeRelatorios(1, placa, 1);

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR O NUMERO DA PLACA NA JANELA HISTORICO.\r\n" + e.getMessage());
		}

	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'Revisao' em Emiss�o de Rel�torios na tela de
	 * Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaRevisao() {

		try {

			try {

				getDSL().esperaPaginaCarregarJs();
				frameModalUm();

				getDSL().clica(By.id(varPage.getBtnRevisaoEmissaoRelatoriosId()));

			} catch (Exception e) {
				throw new Exception("ERRO AO CLICAR NO BOTAO FOTO.\r\n" + e.getMessage());
			}

		} catch (Exception e) {

		}
	}

	public void verificaPopUpRevisao() throws Exception {

		try {

			esperaPopUpRevisaoOuAlert();

			if (getDSL().verificarAlertVisivel()) {
				BotaoEmissaoDeRelatoriosStep.condRevisao = false;
				getDSL().apertarEnterAlert();
			}

			else {
				BotaoEmissaoDeRelatoriosStep.condRevisao = true;
			}

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE O ALERT OU O POP-UP REVISAO ESTA VISIVEL.\r\n" + e.getMessage());
		}
	}

	public void clicaConfirmarRevisao() throws Exception {

		try {

			frameModalCinco();
			getDSL().clica(By.id(varPage.getBtnConfirmarRevisaoEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO CONFIRMAR.\r\n" + e.getMessage());
		}
	}

	private void esperaPopUpRevisaoOuAlert() throws Exception {

		try {
			getWait().until(ExpectedConditions.or(verificaPopUpRevisoes(), ExpectedConditions.alertIsPresent()));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ALERTA OU O POP-UP DE REVISOES.\r\n" + e.getMessage());
		}
	}

	public void verificaNumeroPlacaSinistroRevisao(String placa) throws Exception {

		try {
			procuraDentroDaTabelaEmEmissaoDeRelatorios(3, placa, 1);
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR O NUMERO DA PLACA NA JANELA HISTORICO.\r\n" + e.getMessage());
		}

	}

	private ExpectedCondition<WebElement> verificaPopUpRevisoes() {

		return new ExpectedCondition<WebElement>() {

			@Override
			public WebElement apply(WebDriver driver) {
				try {
					frameModalCinco();
					return getDSL().verificaVisibilidade(By.id("ctl00_cph_Rodape_btn_Confirmar"));
				} catch (Exception e) {
					return null;
				}
			}
		};
	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'Acesso' em Emiss�o de Rel�torios na tela de
	 * Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaAcesso() throws Exception {

		try {

			getDSL().esperaPaginaCarregarJs();
			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnAcessoEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO ACESSO.\r\n" + e.getMessage());
		}
	}

	public boolean verificaNumeroPlacaSinistroAcesso(String placa) {

		try {
			return procuraDentroDaTabelaEmEmissaoDeRelatorios(3, placa, 1);
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'Connect' em Emiss�o de Rel�torios na tela de
	 * Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaConnect() throws Exception {
		try {

			getDSL().esperaPaginaCarregarJs();
			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnConnectEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO CONNECT.\r\n" + e.getMessage());
		}
	}

	public boolean verificaNumeroPlacaSinistroConnect(String placa) {
		try {
			return procuraDentroDaTabelaEmEmissaoDeRelatorios(3, placa, 1);
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'Avarias' em Emiss�o de Rel�torios na tela de
	 * Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaAvarias() throws Exception {

		try {
			getDSL().esperaPaginaCarregarJs();
			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnAvariasEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO AVARIAS.\r\n" + e.getMessage());
		}
	}

	public boolean verificaNumeroPlacaSinistroAvarias(String placa) {
		try {
			return procuraDentroDaTabelaEmEmissaoDeRelatorios(4, placa, 1);
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Espera a p�gina carregar totalmente, troca o foco da p�gina para o frame
	 * 'Modal Um' e clica no bot�o 'Fotos Avarias' em Emiss�o de Rel�torios na tela
	 * de Or�amento.
	 * 
	 * @throws Exception
	 */

	public void clicaFotosAvariadas() throws Exception {

		try {
			getDSL().esperaPaginaCarregarJs();
			frameModalUm();

			getDSL().clica(By.id(varPage.getBtnFotosAvariasEmissaoRelatoriosId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO FOTOS AVARIAS.\r\n" + e.getMessage());
		}
	}

	public boolean verificaNumeroPlacaSinistroFotosAvarias(String placa) {
		try {
			return procuraDentroDaTabelaEmEmissaoDeRelatorios(4, placa, 1);
		} catch (Exception e) {
			return false;
		}
	}

	////////////////////////////////////

	public void fechaEmissaoDeRelatorios() throws Exception {

		try {

			frameModalUm();
			getDSL().clica(By.id("ctl00_cph_Rodape_btn_Cancelar"));
			// getDSL().clica(By.id(varPage.get_fecharEmissaoRelatoriosId()));

			esperaTelaOrcamentoVoltarAEstarVisivel();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR EM FECHAR.\r\n" + e.getMessage());
		}
	}

	private boolean procuraDentroDaTabelaEmEmissaoDeRelatorios(int indice, String elemento_busca, int indice_coluna)
			throws Exception {

		getDSL().esperaElemento(By.xpath("//table[" + indice + "]"));

		List<WebElement> linhas = getDriver()
				.findElements(By.xpath("//table[" + indice + "]//tbody/tr/td[" + indice_coluna + "]"));

		System.out.println("Texto que eu quero: " + elemento_busca);

		for (int i = 0; i <= linhas.size(); i++) {

			System.out.println("Texto achado: " + linhas.get(i).getText());

			if (linhas.get(i).getText().contains(elemento_busca)
					| linhas.get(i).getText().equalsIgnoreCase(elemento_busca)
					| elemento_busca.equalsIgnoreCase(linhas.get(i).getText())
					| elemento_busca.contains(linhas.get(i).getText())) {

				System.out.println("Achouuuuu!");
				return true;
			}
		}

		return false;
	}

	public String pegarIdJanela() throws Exception {
		try {
			return getDSL().pegaWindowHandle();
		} catch (Exception e) {
			throw new Exception(
					"ERRO AO TENTAR PEGAR O ID DA JANELA EMISSAO RELATORIOS EM OR�AMENTO. Erro: " + e.toString());
		}
	}

	public void fecharSegundaTela(String janela_principal) throws Exception {
		try {
			getDriver().close();
			getDriver().switchTo().window(janela_principal);
		} catch (Exception e) {
			throw new Exception("ERRO AO FECHAR A SEGUNDA JANELA. Erro: " + e.toString());
		}
	}

	public boolean verificarTelaOrcamentoVisivel() throws Exception {

		/*
		 * Verifica se a tela 'Or�amento' � a tela atual.
		 */

		try {
			// Troca o foco para o frame principal
			framePrincipal();
			// Espera o elemento estar visivel
			getDSL().esperarElementoEstarVisivelId("frm_Main");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void trocaParaJanela(String url_janela) throws Exception {

		try {
			getDSL().trocarJanelaFocoCompleto(url_janela);
		} catch (Exception e) {
			throw new Exception("ERRO AO TROCAR DE JANELA.\r\n" + e.getMessage());
		}

	}

}
