package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class BotaoMaoDeObraPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();

	public void clicaMaoDeObra() throws Exception {

		try {

			esperaTelaOrcamentoVoltarAEstarVisivel();

			esperaAjaxEIcone();
			frameAgenda();

			getDSL().clica(By.id(varPage.get_btnMaoDeObraOrcamentoId()));

			frameModalUm();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO MAO DE OBRA NA TELA ORCAMENTO.\r\n" + e.getMessage());
		}
	}

	public void preencheValorMecanicaMaoDeObra(String valor_mecanica) throws Exception {

		try {
			if (getDSL().verificaElementoExiste(By.id(varPage.get_campoMecanicaMaoDeObraOrcamentoId()))) {
				getDSL().escreveJsId(varPage.get_campoMecanicaMaoDeObraOrcamentoId(), valor_mecanica);
				getDSL().esperaValor(By.id(varPage.get_campoMecanicaMaoDeObraOrcamentoId()), valor_mecanica);
				voltaTempoDeEspera();
			}

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER O CAMPO MECANICA EM MAO-DE-OBRA.\r\n" + e.getMessage());
		}

	}

	public void preencheValorFunilariaMaoDeObra(String valor_funilaria) throws Exception {

		try {
			if (getDSL().verificaElementoExiste(By.id(varPage.get_campoFunilariaMaoDeObraOrcamentoId()))) {
				getDSL().escreveJsId(varPage.get_campoFunilariaMaoDeObraOrcamentoId(), valor_funilaria);
				getDSL().esperaValor(By.id(varPage.get_campoFunilariaMaoDeObraOrcamentoId()), valor_funilaria);
				voltaTempoDeEspera();
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER O CAMPO FUNILARIA EM MAO-DE-OBRA.\r\n" + e.getMessage());
		}

	}

	public void preencheValorPinturaMaoDeObra(String valor_pintura) throws Exception {

		try {
			if (getDSL().verificaElementoExiste(By.id(varPage.get_campoPinturaMaoDeObraOrcamentoId()))) {
				getDSL().escreveJsId(varPage.get_campoPinturaMaoDeObraOrcamentoId(), valor_pintura);
				getDSL().esperaValor(By.id(varPage.get_campoPinturaMaoDeObraOrcamentoId()), valor_pintura);
				voltaTempoDeEspera();
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER O CAMPO PINTURA EM MAO-DE-OBRA.\r\n" + e.getMessage());
		}
	}

	public void preencheValorEletricaMaoDeObra(String valor_eletrica) throws Exception {

		try {
			if (getDSL().verificaElementoExiste(By.id(varPage.get_campoEletricaMaoDeObraOrcamentoId()))) {
				getDSL().escreveJsId(varPage.get_campoEletricaMaoDeObraOrcamentoId(), valor_eletrica);
				getDSL().esperaValor(By.id(varPage.get_campoEletricaMaoDeObraOrcamentoId()), valor_eletrica);
				voltaTempoDeEspera();
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER O CAMPO ELETRICA EM MAO-DE-OBRA.\r\n" + e.getMessage());
		}

	}

	public void preencheValorTapecariaMaoDeObra(String valor_tapecaria) throws Exception {

		// Preenche/Altera o valor de 'Tapešaria'

		try {
			if (getDSL().verificaElementoExiste(By.id(varPage.get_campoTapecariaMaoDeObraOrcamentoId()))) {
				getDSL().escreveJsId(varPage.get_campoTapecariaMaoDeObraOrcamentoId(), valor_tapecaria);
				getDSL().esperaValor(By.id(varPage.get_campoTapecariaMaoDeObraOrcamentoId()), valor_tapecaria);
				voltaTempoDeEspera();
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER O CAMPO PINTURA EM MAO-DE-OBRA");
		}
	}

	public void preencheValorVidracariaMaoDeObra(String valor_vidracaria) throws Exception {

		try {
			if (getDSL().verificaElementoExiste(By.id(varPage.get_campoVidracariaMaoDeObraOrcamentoId()))) {
				getDSL().escreveJsId(varPage.get_campoVidracariaMaoDeObraOrcamentoId(), valor_vidracaria);
				getDSL().esperaValor(By.id(varPage.get_campoVidracariaMaoDeObraOrcamentoId()), valor_vidracaria);
				voltaTempoDeEspera();
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER O CAMPO VIDRACARIA EM MAO-DE-OBRA");
		}
	}

	public void preencheValorReparacaoMaoDeObra(String valor_reparacao) throws Exception {

		try {
			if (getDSL().verificaElementoExiste(By.id(varPage.get_campoReparacaoMaoDeObraOrcamentoId()))) {
				getDSL().escreveJsId(varPage.get_campoReparacaoMaoDeObraOrcamentoId(), valor_reparacao);
				getDSL().esperaValor(By.id(varPage.get_campoReparacaoMaoDeObraOrcamentoId()), valor_reparacao);
				voltaTempoDeEspera();
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER O CAMPO REPARACAO EM MAO-DE-OBRA");
		}
	}

	public void clicaConfirmar() throws Exception {

		try {

			getDSL().clica(By.id(varPage.get_btnConfirmarMaoDeObraOrcamentoId()));

			esperaTelaOrcamentoVoltarAEstarVisivel();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO CONFIRMAR EM MAO DE OBRA.\r\n" + e.getMessage());
		}
	}

}
