package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import es.indra.atributos.TestAttributes;
import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;
import es.indra.step.perito.orcamento.AbaBaremoStep;

/**
 * Classe do tipo page respons�vel por organizar as a��es do teste e interagir
 * diretamente com o framework.
 * 
 * @author lfelipeo
 *
 */

public class AbaBaremoPage extends BasePage {

	OrcamentoPageVar varPage = new OrcamentoPageVar();
	TestAttributes testAt = new TestAttributes();

	/**
	 * Espera o ajax call finalizar, troca o foco da p�gina para o frame 'Or�amento'
	 * e seleciona uma regi�o do carro de forma aleat�ria.
	 * 
	 * @throws Exception
	 */

	public void selecionaRegiaoCarroBaremo() throws Exception {

		try {
			
			//esperaTelaOrcamentoVoltarAEstarVisivel();
			
			esperaAjaxEIcone();
			
			frameOrcamento();
			
			getDSL().clicarPecaCarro();

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR A REGIAO DO CARRO.\r\n" + e.getMessage());
		}
	}

	/**
	 * Espera o ajax call finalizar, troca o foco da p�gina para o frame
	 * 'Or�amento'. Seleciona uma op��o em 'Pe�a' e depois seleciona uma op��o de
	 * origem, ambas de forma aleat�ria.
	 * 
	 * @throws Exception
	 */

	public void selecionaPecaEOrigemBaremo() throws Exception {

		try {

			esperaAjaxEIcone();
			frameOrcamento();

			getDSL().clicarOpcaoListaPeca();

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR PECA.\r\n" + e.getMessage());
		}

		try {

			esperaAjaxEIcone();
			frameOrcamento();

			getDSL().clicaOrigemPeca();

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR A ORIGEM DA PECA.\r\n" + e.getMessage());
		}

	}

	/**
	 * Verifica pelo texto, se � uma pe�a ou um servi�o.
	 */

	public void verificaSeEUmaPecaOuServico() {

		if (AbaBaremoStep.pecaBaremo.substring(0, 4).contains("-")) {
			AbaBaremoStep.eUmaPeca = true;
			AbaBaremoStep.pecaBaremo = AbaBaremoStep.pecaBaremo.substring(4);
		}

		else {
			AbaBaremoStep.eUmaPeca = false;
		}
	}

	/**
	 * Clica no bot�o 'Adicionar' em Baremo e espera o ajax call finalizar. Espera
	 * ap�s o clica no bot�o 'Adicionar' at� aparecer um alerta aviasando que a pe�a
	 * j� foi adicionada ou a tela de pre�os caso a pe�a ainda n�o tenha sido
	 * adicionada.
	 * 
	 * @throws Exception
	 */

	public void clicaAdicionarBaremo() throws Exception {

		try {

			getDSL().clica(By.id(varPage.getBtnAdicionarOrcamentoId()));

			getDSL().esperaPaginaCarregarJs();
			esperaAjaxEIcone();

			if (testAt.getSeguradora().equalsIgnoreCase("Mitsui")) {
				getDSL().verificarAlertVisivel();
				esperaAlertOuElemento();
				acaoAposBotaoAdicionar();
			}

			else if (testAt.getSeguradora().equalsIgnoreCase("Tokio")) {
				acaoAposBotaoAdicionarTokio();
			}

			//esperaTelaOrcamentoVoltarAEstarVisivel();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO ADICIONAR.\r\n" + e.getMessage());
		}

	}

	/**
	 * Execute as a��es que forem necess�rias depois de clicar no bot�o 'Adicionar'.
	 * Caso ap�s o clique apare�a um alerta avisando que a pe�a foi adicionada o
	 * alerta ser� confirmado, caso a pe�a possuir mais de uma op��o de pre�o a
	 * tabela 'Pre�o da Pe�a' ir� aparecer na tela, ser� selecionada uma op��o de
	 * forma aleat�ria e clica no bot�o 'Confirmar'.
	 * 
	 * @param id_origem
	 * @throws Exception
	 */

	public void acaoAposBotaoAdicionar() throws Exception {

		try {

			if (getDSL().verificarAlertVisivel()) {
				while (getDSL().verificarAlertVisivel()) {
					getDSL().apertarEnterAlert();
				}
			}

			else if (getDSL().verificaVisibilidadeBoolean(By.id(varPage.get_tablePrecoPecaBaremoOrcamentoId()))) {

				esperaAjaxEIcone();
				frameModalDois();

				getDSL().clicarPrecoPeca(varPage.get_tablePrecoPecaBaremoOrcamentoId());
				getDSL().clica(By.id(varPage.get_btnConfirmarPrecoPecaBaremoOrcamentoId()));
			}

			getDSL().esperaPaginaCarregarJs();
			esperaAjaxEIcone();

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR O PRECO DA PECA.\r\n" + e.getMessage());
		}
	}

	public void acaoAposBotaoAdicionarTokio() {

		try {

			frameModalDois();
			if (getDSL().verificaVisibilidadeBoolean(By.id("ctl00_upn_Form"))) {


				if (getDSL().contaQtnTrTabelaId(By.id("ctl00_cph_Form_tbl_Lista")) > 1) {

					esperaAjaxEIcone();
					frameModalDois();

					getDSL().clicarPrecoPeca(varPage.get_tablePrecoPecaBaremoOrcamentoId());
					getDSL().clica(By.id(varPage.get_btnConfirmarPrecoPecaBaremoOrcamentoId()));

				}
			}

			if (getDSL().verificarAlertVisivel()) {
				getDSL().confirmarAlert();
			}

		} catch (Exception e) {

		}

	}

	public boolean verificarTelaOrcamentoVisivel() throws Exception {

		try {
			getDSL().esperaPaginaCarregarJs();
			esperaAjaxEIcone();
			framePrincipal();
			getDSL().esperarElementoEstarVisivelId("frm_Main");

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String opcaoSelecionadaOrigem() {

		String[] opcoes = { "ctl00_cph_Form_rdb_Origem_Original", "Original Genu�na", "ctl00_cph_Form_rdb_Origem_CIA",
				"Fornecido pela Cia", "ctl00_cph_Form_rdb_Origem_MA", "Original Gen�rica" };

		WebElement btn;

		for (int i = 0; i <= opcoes.length; i += 2) {

			btn = getDriver().findElement(By.id(opcoes[i]));

			if (btn.isSelected()) {
				return opcoes[i + 1];
			}
		}

		return null;
	}

	public void esperaAlertOuElemento() throws Exception {

		try {

			getWait().until(ExpectedConditions.or(esperaJanelaPrecos(), ExpectedConditions.alertIsPresent()));

		} catch (Exception e) {
			throw new Exception("OCORREU UM ERRO NA ESPERA DO ALERT OU DA JANELA DE PRECOS.\r\n" + e.getMessage());
		}
	}

	public ExpectedCondition<WebElement> esperaJanelaPrecos() {

		return new ExpectedCondition<WebElement>() {

			@Override
			public WebElement apply(WebDriver driver) {
				try {
					frameModalDois();
					return getDSL().verificaVisibilidade(By.id("ctl00_upn_Form"));
				} catch (Exception e) {
					return null;
				}
			}
		};
	}
}
