package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

public class BotaoFecharEsteSinistroPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();

	/**
	 * Espera o ajax call finalizar, a espera do Javascript finalizar, troca o foco
	 * da p�gina para o frame 'Agenda' e clica no bot�o 'Fechar Este Sinistro'. Caso
	 * algum alerta apare�a na tela ser� confirmado.
	 * 
	 * @throws Exception
	 */

	public void clicaBotaoFecharEsteSinistro() throws Exception {

		try {

			esperaTelaOrcamentoVoltarAEstarVisivel();
			esperaAjaxEIcone();

			frameAgenda();

			getDSL().clica(By.id(varPage.get_btnFecharEsteOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO FECHAR ESTE SINISTRO.\r\n" + e.getMessage());
		}
		try {
			if (getDSL().verificarAlertVisivel()) {
				getDSL().fecharTodosAlert();
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO FECHAR OS ALERTAS DE AVISO APOS CLICAR NO BOTAO FECHAR ESTE SINISTRO.\r\n"
					+ e.getMessage());
		}
	}

	/**
	 * Espera o ajax call finalizar, troca o foco da p�gina para o frame 'Modal Um'
	 * e seleciona uma op��o em 'Carimbo do Processo'.
	 * 
	 * @param texto_carimbo
	 * @throws Exception
	 */

	public void selecionaCarimboDoProcesso(String texto_carimbo) throws Exception {

		try {

			esperaAjaxEIcone();
			frameModalUm();

			getDSL().selecionaComboTexto(By.id(varPage.get_comboCarimboFinalizarSinistroId()), texto_carimbo);

			esperaAjaxEIcone();

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR O CARIMBO DO PROCESSO.\r\n" + e.getMessage());
		}
	}

	/**
	 * Escreve no campo 'Observa��o'.
	 * 
	 * @param texto_observacao
	 * @throws Exception
	 */

	public void escreveObservacao(String texto_observacao) throws Exception {

		try {

			esperaAjaxEIcone();
			frameModalUm();

			getDSL().escreveJsId(varPage.get_campoObservacaoFinalizarSinistroId(), texto_observacao);
			getDSL().esperaTexto(By.id(varPage.get_campoObservacaoFinalizarSinistroId()), texto_observacao);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER O CAMPO OBSERVACAO.\r\n" + e.getMessage());
		}
	}

	public void clicaCancelar() throws Exception {

		try {
			getDSL().clica(By.id(varPage.get_btnCancelarFinalizarSinistroId()));
		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO CANCELAR.\r\n" + e.getMessage());
		}
	}

	public void clicaConfirmar() throws Exception {

		try {

			frameModalUm();
			
			getDSL().clica(By.id(varPage.get_btnConfirmarFinalizarSinistroId()));
			esperaAjaxEIcone();

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO BOTAO CONFIRMAR.\r\n" + e.getMessage());
		}
	}

	public void verificaJanelaPosConfirmacao(String url) throws Exception {

		try {
			getDSL().trocarJanelaFocoCompleto(url);
			Thread.sleep(300);
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR A SEGUNDA JANELA.\r\n" + e.getMessage());
		}
	}

	public String pegarIdJanela() throws Exception {
		try {
			return getDSL().pegaWindowHandle();
		} catch (Exception e) {
			throw new Exception(
					"ERRO AO TENTAR PEGAR O ID DA JANELA EMISSAO RELATORIOS EM OR�AMENTO. Erro: " + e.toString());
		}
	}

	public void fecharSegundaTela(String janela_principal) throws Exception {
		try {
			getDriver().close();
			getDriver().switchTo().window(janela_principal);
		} catch (Exception e) {
			throw new Exception("ERRO AO FECHAR A SEGUNDA JANELA. Erro: " + e.toString());
		}
	}

}
