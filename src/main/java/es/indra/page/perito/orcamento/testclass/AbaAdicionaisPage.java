package es.indra.page.perito.orcamento.testclass;

import org.openqa.selenium.By;

import es.indra.base.BasePage;
import es.indra.page.perito.orcamento.attributesclass.OrcamentoPageVar;

/**
 * 
 * Classe do tipo page respons�vel por organizar as a��es do teste e interagir
 * diretamente com o framework.
 * 
 * @author lfelipeo
 *
 */

public class AbaAdicionaisPage extends BasePage {

	private OrcamentoPageVar varPage = new OrcamentoPageVar();

	/**
	 * Troca o foco da p�gina para o frame 'Agenda' na tela de Or�amento, espera a
	 * aba 'Adicionais' estar v�sivel e clica.
	 * 
	 * @throws Exception
	 */

	public void clicaAbaAdicionaisOrcamento() throws Exception {

		try {

			//esperaTelaOrcamentoVoltarAEstarVisivel();
			esperaAjaxEIcone();

			frameAgenda();

			//getWait().until(getDSL().esperaElementoSerVisivelFW(By.id(varPage.getBtnAdicionaisOrcamentoId())));
			getDSL().clica(By.id(varPage.getBtnAdicionaisOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NA ABA ADICIONAIS.\r\n" + e.getMessage());
		}

	}

	/**
	 * Troca o foco da p�gina para o frame 'Or�amento' na tela de Or�amento,
	 * seleciona uma op��o em 'Itens' e espera o ajax call finalizar.
	 * 
	 * @throws Exception
	 */

	public void selecionaItens() throws Exception {

		try {

			frameOrcamento();

			getDSL().selecionaItensAbaAdicionais(varPage.getSelectItensAbaAdicionaisOrcamentoId());

			esperaAjaxEIcone();

		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR UMA OPCAO EM ITENS.\r\n" + e.getMessage());
		}
	}

	/**
	 * Troca o foco da p�gina para o frame 'Or�amento' na tela de Or�amento, pega o
	 * texto da op��o selecionada em 'Itens' e realiza determinada a��o dependendo
	 * da op��o escolhida.
	 * 
	 * @throws Exception
	 */

	public void preencheInfoValorOuHora() throws Exception {

		String item = null;

		try {

			frameOrcamento();

			item = getDSL().pegaOpcaoSelecionadaSelect(By.id(varPage.getSelectItensAbaAdicionaisOrcamentoId()));

		} catch (Exception e) {
			throw new Exception("ERRO AO PEGAR O ITEM SELECIONADO EM ITENS.\r\n" + e.getMessage());
		}

		/*
		 * Condi��o que verifica se o item selecionado foi 'Bancada' ou 'Outros', devido
		 * o Bancada s� permitir altera��o no campo hora e Outros s� permitir
		 * altera��o no campo valor, a condi��o limita essas duas op��es a s� alterarem
		 * os campos que s�o possiveis. Caso n�o seja nenhuma das duas, ir� alterar de
		 * forma aleat�ria.
		 */

		if (item.equalsIgnoreCase("BANCADA")) {
			preencheInfoHora();
		}

		else if (item.equalsIgnoreCase("OUTROS")) {
			getDSL().escreveJsId(varPage.getCampoServicoAbaAdicionaisId(), "Teste Automatizado");
			preencheInfoValor();
		}

		else {

			int ale = Integer.parseInt(getDSL().valorAleatorio(4));

			if (ale == 1 | ale == 3) {
				preencheInfoValor();
			}

			else if (ale == 2 | ale == 4) {
				preencheInfoHora();
			}
		}
	}

	/**
	 * 
	 * Troca o foco da p�gina para o frame 'Or�amento' na tela de Or�amento, espera
	 * o bot�o 'Adicionar' estar vis�vel e clica. Espera o ajax call finalizar, caso
	 * tiver algum alert vis�vel ser� fechado.
	 * 
	 * @throws Exception
	 */

	public void clicaAdicionar() throws Exception {

		try {

			frameOrcamento();
			getDSL().clica(By.id(varPage.getBtnAdicionarAbaAdicionaisId()));

			esperaAjaxEIcone();


			while (getDSL().verificarAlertVisivel()) {
				getDSL().apertarEnterAlert();
			}

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR ADICIONAR.\r\n" + e.getMessage());
		}
	}

	/**
	 * Troca o foco da p�gina para o frame 'Or�amento' na tela de Or�amento, gera-se
	 * um n�mero aleat�rio de 1 a 100 que ser� usado no campo 'Valor', espera o
	 * campo 'Valor' estar vis�vel e escreve o valor gerado aleat�rio.
	 * 
	 * @throws Exception
	 */

	private void preencheInfoValor() throws Exception {

		try {

			frameOrcamento();

			String valor = getDSL().valorAleatorio(100);

			getDSL().escreveJsId(varPage.getCampoValorAbaAdicionaisId(), valor);
			getDSL().esperaValor(By.id(varPage.getCampoValorAbaAdicionaisId()), valor);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER O CAMPO VALOR.\r\n" + e.getMessage());
		}
	}

	/**
	 * Troca o foco da p�gina para o frame 'Or�amento' na tela de Or�amento, gera-se
	 * um n�mero aleat�rio de 1 a 3 que ser� usado no campo 'Hora', espera o campo
	 * 'Hora' estar vis�vel e escreva o valor gerado aleat�rio.
	 * 
	 * @throws Exception
	 */

	private void preencheInfoHora() throws Exception {

		try {

			frameOrcamento();

			String valor = getDSL().valorAleatorio(3);

			getDSL().escreveJsId(varPage.getCampoHoraAbaAdicionaisId(), valor);
			getDSL().esperaValor(By.id(varPage.getCampoHoraAbaAdicionaisId()), valor);

		} catch (Exception e) {
			throw new Exception("ERRO AO PREENCHER O CAMPO HORA.\r\n" + e.getMessage());
		}
	}


	public void esperaAjaxAbaManual() throws Exception {

		try {
			getDSL().waitForAjaxCalls();
			while (!(getDSL().esperaElementoNaoEstarVisivel(By.id("div_progresso")))) {
				Thread.sleep(100);
			}
			framePrincipal();
		} catch (Exception e) {
		}
	}

	
}
