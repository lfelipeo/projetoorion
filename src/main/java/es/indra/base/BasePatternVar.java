package es.indra.base;

import es.indra.atributos.TestAttributes;

public class BasePatternVar {

	private TestAttributes testAt = new TestAttributes();

	private final String SEGURADORA = testAt.getSeguradora().toLowerCase();
	private final String ELEMENTO_BUSCA = testAt.getElementoBusca();

	public String getSeguradora() {
		return SEGURADORA;
	}

	public String getElementoBusca() {
		return ELEMENTO_BUSCA;
	}
}
