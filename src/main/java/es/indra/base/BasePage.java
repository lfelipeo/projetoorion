package es.indra.base;

import org.openqa.selenium.By;

import es.indra.core.DriverFactory;

public class BasePage extends DriverFactory {

	private BasePageVar varBasePage = new BasePageVar();

	/**
	 * Abre o navegador e carrega a p�gina da seguradora escolhida.
	 * 
	 * @throws Exception
	 */

	public void iniciarSistema() throws Exception {
		getDSL().carregarPagina(varBasePage.getUrl());
	}

	/**
	 * Espera o campo de Login estar vis�vel na tela e preenche.
	 * 
	 * @param login
	 * @throws Exception
	 */

	public void preencheCampoLogin(String login) throws Exception {
		getDSL().escreveJsId(varBasePage.getLoginId(), login);
		getDSL().esperaValor(By.id(varBasePage.getLoginId()), login);
	}

	/**
	 * Espera o campo de Senha estar vis�vel na tela e preenche.
	 * 
	 * @param senha
	 * @throws Exception
	 */

	public void preencheCampoSenha(String senha) throws Exception {
		getDSL().escreveJsId(varBasePage.getSenhaId(), senha);
		getDSL().esperaValor(By.id(varBasePage.getSenhaId()), senha);
	}

	/**
	 * Clica no bot�o 'Ok'.
	 * 
	 * @throws Exception
	 */

	public void clicaBtnOk() throws Exception {
		getDSL().clica(By.id(varBasePage.getBtnEntrarId()));
	}

	/**
	 * Volta o foco da p�gina para o frame principal.
	 * 
	 * @throws Exception
	 */

	public void framePrincipal() throws Exception {
		try {
			getDSL().focoPrincipal();
		} catch (Exception e) {
			throw new Exception("ERRO AO VOLTAR O FOCO PARA O PRINCIPAL. Erro: " + e.toString());
		}
	}

	/**
	 * Troca o foco da p�gina para o frame 'Agenda'.
	 * 
	 * @throws Exception
	 */

	public void frameAgenda() throws Exception {
		try {
			getDSL().focoPrincipal();
			getDSL().esperaETrocaFrame(7);
		} catch (Exception e) {
			throw new Exception("ERRO AO TROCAR O FOCO PARA O FRAME AGENDA. Erro: " + e.toString());
		}
	}

	/**
	 * Troca o foco da p�gina para o frame 'Modal Um'.
	 * 
	 * @throws Exception
	 */

	public void frameModalUm() throws Exception {
		try {
			getDSL().focoPrincipal();
			getDSL().esperaETrocaFrame(1);
		} catch (Exception e) {
			throw new Exception("ERRO AO TROCAR O FOCO PARA O FRAME MODAL UM. Erro: " + e.toString());
		}
	}

	/**
	 * Troca o foco da p�gina para o frame 'Modal Dois'.
	 * 
	 * @throws Exception
	 */

	public void frameModalDois() throws Exception {
		try {
			getDSL().focoPrincipal();
			getDSL().esperaETrocaFrame(2);
		} catch (Exception e) {
			throw new Exception("ERRO AO TROCAR O FOCO PARA O FRAME MODAL DOIS. Erro: " + e.toString());
		}
	}

	public void frameModalCinco() throws Exception {
		try {
			getDSL().focoPrincipal();
			getDSL().esperaETrocaFrame(5);
		} catch (Exception e) {
			throw new Exception("ERRO AO TROCAR O FOCO PARA O FRAME MODAL CINCO. Erro: " + e.toString());
		}
	}

	/**
	 * Troca o foco da p�gina para o frame 'Analise'.
	 * 
	 * @throws Exception
	 */

	public void frameAnalise() throws Exception {
		try {
			getDSL().focoPrincipal();
			getDSL().esperaETrocaFrame(8);
		} catch (Exception e) {
			throw new Exception("ERRO AO TROCAR O FOCO PARA O FRAME ANALISE. Erro: " + e.toString());
		}
	}

	/**
	 * Primeiro troca o foco da p�gina para o frame 'Agenda', e dentro do frame
	 * 'Agenda' troca o foco para o frame 'Or�amento'.
	 * 
	 * @throws Exception
	 */

	public void frameOrcamento() throws Exception {
		try {
			getDSL().focoPrincipal();
			getDSL().esperaETrocaFrame(7, 7);
		} catch (Exception e) {
			throw new Exception("ERRO AO TROCAR O FOCO PARA O FRAME OR�AMENTO. Erro: " + e.toString());
		}
	}

	/**
	 * Troco o foco da p�gina para o frame 'Agenda', espera o ajax call finalizar e
	 * espera o icone de loading da tela n�o estar mais vis�vel.
	 * 
	 * @throws Exception
	 */

	public void esperaAjaxEIcone() throws Exception {

		try {
			frameAgenda();
			getDSL().waitForAjaxCalls();
			while (!(getDSL().esperaElementoNaoEstarVisivel(By.id("div_progresso")))) {
				Thread.sleep(100);
			}
			framePrincipal();
		} catch (Exception e) {
		}
	}

	/**
	 * Espera o banner de t�tula da tela de Or�amento estar vis�vel e conter o texto
	 * 'Or�amento'.
	 */

	public void esperaTelaOrcamentoVoltarAEstarVisivel() {

		try {

			Thread.sleep(150);
			frameAgenda();

			getDSL().waitForAjaxCalls();
			while (!(getDSL().esperaElementoNaoEstarVisivel(By.id("div_progresso")))) {
				Thread.sleep(100);
			}

			getDSL().esperaPaginaCarregarJs();
			frameAgenda();

			getDSL().esperaElemento(By.id("ctl00_lbl_TituloPagina2"));
			getDSL().esperaElemento(By.id("ctl00_cph_Botoes_img_Fotos"));
			getDSL().esperaElemento(By.id("ctl00_cph_Botoes_img_Pecas"));
			getDSL().esperaElemento(By.id("ctl00_cph_Botoes_img_Relatorios"));
			getDSL().esperaElemento(By.id("ctl00_cph_Botoes_img_Salvados"));
			getDSL().esperaElemento(By.id("ctl00_cph_Botoes_img_PMG"));
			getDSL().esperaElemento(By.id("ctl00_cph_Botoes_img_MaoObra"));
			getDSL().esperaElemento(By.id("ctl00_cph_Botoes_img_Veiculo"));
			getDSL().esperaElemento(By.id("ctl00_cph_Botoes_img_Detalhes"));
			getDSL().esperaElemento(By.id("ctl00_cph_Botoes_img_IdentificacaoSinisto"));
			getDSL().esperaElemento(By.id("ctl00_cph_Botoes_img_Encerrar"));
			getDSL().esperaElemento(By.id("tab_Orcamento_1"));

		} catch (Exception e) {

		}
	}
}
