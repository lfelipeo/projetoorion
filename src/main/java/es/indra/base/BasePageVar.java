package es.indra.base;

import es.indra.atributos.TestAttributes;

public class BasePageVar {

	private TestAttributes testAt = new TestAttributes();

	private final String URL = "http://valida.orionbr.com.br/" + getSeguradora() + ".orcamentos.v6/";

	public static String _fluxo = "";
	public static String _cliente = "";

	private final String LOGIN_ID = "txt_Nm_Login";
	private final String SENHA_ID = "txt_Cd_Senha";
	private final String BTN_ENTRAR_ID = "ibt_Entrar";

	/**********************************************************************/
	/******************************* GETTERS ******************************/
	/**********************************************************************/

	public String getUrl() {
		return URL;
	}

	public String getSeguradora() {
		return testAt.getSeguradora();
	}

	public void set_fluxo(String fluxo) {
		_fluxo = fluxo;
	}

	public static String get_fluxo() {
		return _fluxo;
	}

	public void set_cliente(String cliente) {
		_cliente = cliente;
	}

	public static String get_cliente() {
		return _cliente;
	}

	public String getLoginId() {
		return LOGIN_ID;
	}

	public String getSenhaId() {
		return SENHA_ID;
	}

	public String getBtnEntrarId() {
		return BTN_ENTRAR_ID;
	}

}
