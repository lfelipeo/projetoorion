package es.indra.base;

public class BasePatternPage {

	private BasePageVar varBase = new BasePageVar();

	public void antesDeTudo(String fluxo, String cliente) throws Exception {

		/*
		 * Inicia todas as variaveis necess�rias para executar os testes.
		 */

		try {

			// Set na variavel fluxo, com o fluxo selecionado
			varBase.set_fluxo(fluxo);
			// Set na variavel cliente, com o cliente selecionado
			varBase.set_cliente(cliente);

		} catch (Exception e) {
			throw new Exception("ERRO AO INICIAR AS INFORM��ES NECESS�RIAS PARA O TESTE. Erro: " + e.toString());
		}
	}

}
