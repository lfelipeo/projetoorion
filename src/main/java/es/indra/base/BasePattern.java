package es.indra.base;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class BasePattern extends BaseTeste {

	private BasePatternVar varStep = new BasePatternVar();
	private BasePatternPage page = new BasePatternPage();
	private BaseAction baseAc = new BaseAction();

	/**
	 * Inicia os atributos com o fluxo do teste e a placa que ser� usada. Efetua o
	 * login e cria o log para escrever os resultados do teste.
	 * 
	 * @param fluxo
	 * @throws Exception
	 */

	@Given("^Inicializa as informacoes do teste - \"([^\"]*)\"$")
	public void inicializarTeste(String fluxo) throws Exception {

		try {

			page.antesDeTudo(fluxo, varStep.getElementoBusca());

			loginInicial();

			baseAc.escreverLog("TESTE INICIADO COM SUCESSO - " + fluxo);

			baseAc.logEclipse(fluxo, varStep.getSeguradora(), varStep.getElementoBusca());

		} catch (Exception e) {
			baseAc.logarErro("ERRO AO INICIAR O TESTE", e);
		}
	}

	@Then("^Finaliza o teste - \"([^\"]*)\"$")
	public void finalizarTeste(String fluxo) throws Exception {
		try {
			baseAc.finalizarTeste(fluxo);

		} catch (Exception e) {
			baseAc.logarErro("ERRO EM FINALIZAR O TESTE", e);
		}

	}
}
