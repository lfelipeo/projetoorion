package es.indra.base;

public class BaseActionVar {

	/******************* PASTAS *******************/

	private String _nomePastaArquivos = "Arquivos";
	private String _nomePastaPrint = "Prints";
	private String _nomePastaLog = "Log";
	private String _nomePastaEvidencias = "Evidencia";
	private String _nomeArquivoLog = "Log_";

	/*********************************************/

	/******************* GETTERS *******************/

	public String get_nomePastaArquivos() {
		return _nomePastaArquivos;
	}

	public String get_nomePastaPrint() {
		return _nomePastaPrint;
	}

	public String get_nomePastaLog() {
		return _nomePastaLog;
	}

	public String get_nomePastaEvidencias() {
		return _nomePastaEvidencias;
	}

	public String get_nomeArquivoLog() {
		return _nomeArquivoLog;
	}

	/**********************************************/

}
