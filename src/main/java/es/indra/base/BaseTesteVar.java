package es.indra.base;

import es.indra.atributos.TestAttributes;

public class BaseTesteVar {

	private TestAttributes testAt = new TestAttributes();

	private final String LOGIN_INTERFACE = testAt.getLogin();
	private final String SENHA_INTERFACE = testAt.getSenha();

	public String getLoginInterface() {
		return LOGIN_INTERFACE;
	}

	public String getSenhaInterface() {
		return SENHA_INTERFACE;
	}

}