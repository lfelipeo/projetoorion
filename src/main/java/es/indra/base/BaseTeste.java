package es.indra.base;

import es.indra.core.DriverFactory;

public class BaseTeste extends DriverFactory {

	private static BaseAction baseAc = new BaseAction();
	private static BasePage page = null;
	private static boolean cond = false;

	private BaseTesteVar baseTesteVar = new BaseTesteVar();

	public void loginInicial() throws Exception {

		if (page == null) {

			try {

				page = new BasePage();
				page.iniciarSistema();
				baseAc.padronizarLog("\r\n/****** SISTEMA INICIADO");
				cond = true;

			} catch (Exception e) {
				baseAc.logarErro("OCORREU UM PROBLEMA A INICIAR O SISTEMA DE TESTE", e);
			}

			if (cond) {

				try {

					page.preencheCampoLogin(baseTesteVar.getLoginInterface());
					page.preencheCampoSenha(baseTesteVar.getSenhaInterface());
					page.clicaBtnOk();

					baseAc.padronizarLog("/****** LOGIN EFETUADO COM SUCESSO");

				} catch (Exception e) {
					baseAc.logarErro("OCORREU UM PROBLEMA AO EFETUAR O LOGIN", e);
				}
			}
		} else if (page != null) {
		}
	}

	public static BasePage loginTrocarConta(String segundo_login, String segunda_senha) throws Exception {
		if (page != null) {
			try {
				page = new BasePage();
				page.iniciarSistema(); // Abre o Site da Orion
				cond = true;
			} catch (Exception e) {
				baseAc.logarErro("OCORREU UM PROBLEMA AO ABRIR O SITE DA ORION PARA EFETUAR A TROCA DE CONTA", e);
			}

			if (cond) {
				try {
					page.preencheCampoLogin(segundo_login);
					page.preencheCampoSenha(segunda_senha);
					page.clicaBtnOk();
				} catch (Exception e) {
					baseAc.logarErro("OCORREU UM PROBLEMA AO EFETUAR A TROCA DE CONTA", e);
				}
			}
		}

		return page;
	}

}
