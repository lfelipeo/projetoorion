package es.indra.base;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import es.indra.core.DriverFactory;

public class BaseAction extends DriverFactory {

	private BaseActionVar varAction = new BaseActionVar();
	private static int prints = 1;
	private static BasePageVar basePageVar = new BasePageVar();

	/**
	 * Escreve o arquivo txt contendo todas as informações sobre a execução do
	 * teste.
	 * 
	 * @param texto
	 */
	public void escreverLog(String texto) {
		try {
			getDSL().escreveArquivoLogTxt(basePageVar.getSeguradora(), BasePageVar.get_fluxo(),
					BasePageVar.get_cliente(), "Relatorio", varAction.get_nomeArquivoLog(), "/------ " + texto);
		} catch (IOException e) {
			System.out.println("Erro ao tentar escrever o arquivo de Log");
		}
	}

	public void padronizarLog(String texto) {
		try {
			getDSL().escreveArquivoLogTxt(basePageVar.getSeguradora(), BasePageVar.get_fluxo(),
					BasePageVar.get_cliente(), "Relatorio", varAction.get_nomeArquivoLog(), texto);
		} catch (IOException e) {
			System.out.println("Erro ao tentar escrever o arquivo de Log");
		}
	}

	// Usado para escrever o log em casos de Erro
	public void logarErro(String texto, Exception e) throws Exception {
		printErro(texto);
		fecharBrowser();
		escreverLog(texto + ".\r\n INFORMAÇÕES DO ERRO: " + e.getMessage());
		throw new Exception("/------ " + texto + ". Erro Info: " + e.getMessage());
	}

	// Usado para escrever o log dentro do Eclipse
	public void logEclipse(String fluxo, String seguradora, String placa) {
		System.out.println("Teste Iniciado: " + fluxo);
		System.out.println("Ambiente: " + seguradora);
		System.out.println("Sinistro: " + placa);
	}

	// Tira o print
	public static void tirarPrint(String nome_print) throws Exception {

		try {
			getDSL().printarTela(basePageVar.getSeguradora(), BasePageVar.get_fluxo(), "Print Sucesso",
					BasePageVar.get_cliente(), prints + "_" + nome_print);
			prints++;
		} catch (Exception e) {
			throw new Exception("ERRO AO TIRAR PRINT. Erro: " + e.getMessage());
		}
	}

	// Tira o print de erro
	public void printErro(String nome_print) throws Exception {
		try {
			getDSL().printarTela(basePageVar.getSeguradora(), BasePageVar.get_fluxo(), "Print Erro",
					BasePageVar.get_cliente(), nome_print);
			prints++;
		} catch (Exception e) {
			throw new Exception("ERRO AO TIRAR O PRINT DE ERRO");
		}
	}

	// Feche o navegador
	public void fecharBrowser() {
		getDriver().quit();
	}

	// Para o fluxo da automação por 2 segundos
	public void esperarPagina() {
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			System.out.println("Erro na espera");
			System.exit(0);
		}
	}

	// Finaliza o teste
	public void finalizarTeste(String fluxo) {

		// Escreve log final
		escreverLog("TESTE FINALIZADO COM SUCESSO - " + fluxo);
		// Fecha o browser e a instância do navegador
		fecharBrowser();

		// Escreve as informações finais do teste no Eclipse
		System.out.println("Teste Finalizado: " + fluxo);
		System.out.println("Todas as informações sobre o teste foram escritas no log.");
	}

}
