Feature: Unitario Aba Adicionais 

Scenario: Execute o teste na aba Baremo 

	Given Inicializa as informacoes do teste - "Unitario Aba Adicionais_Abrir Sinistro" 
	Given Que login foi efetuado seleciona o sinistro - "Unitario Aba Adicionais_Abrir Sinistro" 
	
	### ------------------- ABA ADICIONAIS ------------------- ###
	
	Then Clica na aba Adicionais na tela de Orcamento - "Unitario Aba Adicionais_Abrir Sinistro" 
	And Seleciona uma opcao em Itens na aba Adicionais na tela de Orcamento - "Unitario Aba Adicionais_Abrir Sinistro" 
	And Preenche as informacoes na aba Adicionais na tela de Orcamento - "Unitario Aba Adicionais_Abrir Sinistro"
	Then Clica Adicionar na aba Adicionais na tela de Orcamento - "Unitario Aba Adicionais_Abrir Sinistro"
	