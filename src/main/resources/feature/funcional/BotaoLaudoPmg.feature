Feature: Testa o botao Laudo Pmg 

Scenario: Execute o teste no botao Laudo Pmg 

## Informa��es iniciais do teste e efetua o login
	Given Inicializa as informacoes do teste - "Unitario Botao Laudo Pmg_Abrir Sinistro" 
	## Seleciona o Sinistro desejado
	Given Que login foi efetuado seleciona o sinistro - "Unitario Botao Laudo Pmg_Abrir Sinistro" 
	## Execute o teste em 'Dados do Veiculo'
	When Preenche as informacoes na tela Dados do Veiculo - "Unitario Botao Laudo Pmg_Abrir Sinistro" 
	
	### ------------------- BOT�O LAUDO PMG ------------------- ###
	And Verifica as informacoes na tela Laudo PMG - "Unitario Botao Laudo Pmg_Abrir Sinistro" 
	
	## Finaliza o teste
	Then  Finaliza o teste - "Unitario Botao Laudo Pmg_Abrir Sinistro" 