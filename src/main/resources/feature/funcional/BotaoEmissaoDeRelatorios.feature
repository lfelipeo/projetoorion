Feature: Unitario Botao Emissao de Relatorios

Scenario: Execute o teste na Emissao de Relatorios 

### ------------------- PADR�O - INICIO ------------------- ###
	Given Inicializa as informacoes do teste - "Unitario Botao Emissao de Relatorios" 
	Given Que login foi efetuado seleciona o sinistro - "Unitario Botao Emissao de Relatorios" 
	
	### ------------------- DADOS DO VEICULO ------------------- ###
	When Preenche as informacoes na tela Dados do Veiculo - "Unitario Botao Emissao de Relatorios" 
	
	### ------------------- ABA BAREMO ------------------- ###
	Then Verifica aba Baremo esta visivel na tela de Orcamento - "Unitario Botao Emissao de Relatorios" 
	And Seleciona uma regiao do carro na Aba Baremo na tela de Orcamento - "Unitario Botao Emissao de Relatorios" 
	And Seleciona e adiciona uma peca do carro na Aba Baremo na tela de Orcamento - "Unitario Botao Emissao de Relatorios"

	 
	