Feature: Unitario Aba Baremo 

Scenario: Execute o teste na aba Baremo 

	### ------------------- PADR�O ------------------- ###

	Given Inicializa as informacoes do teste - "Unitario Aba Baremo" 
	Given Que login foi efetuado seleciona o sinistro - "Unitario Aba Baremo" 
	
	### ------------------- DADOS DO VEICULO ------------------- ###
	
	Then Verifica se a tela Dados do Veiculo esta visivel - "Unitario Aba Baremo" 
	And Preenche as informacoes do Veiculo na tela Dados do Veiculo - "Unitario Aba Baremo" 
	And Seleciona os Opcionais na tela Dados do Veiculo - "Unitario Aba Baremo" 
	Then Clica Confirmar na tela Dados do Veiculo - "Unitario Aba Baremo" 
	
	### ------------------- ABA BAREMO ------------------- ###
	
	Then Verifica aba Baremo esta visivel na tela de Orcamento - "Unitario Aba Baremo" 
	And Seleciona uma regiao do carro na Aba Baremo na tela de Orcamento - "Unitario Aba Baremo" 
	And Seleciona e adiciona uma peca do carro na Aba Baremo na tela de Orcamento - "Unitario Aba Baremo" 
	
	### ------------------- BOT�O LISTA DE PE�AS ------------------- ###
	
	Then Clica no botao Lista de Pecas na tela Orcamento - "Unitario Aba Baremo" 
	And Verifica a peca adicionada na aba Baremo em Lista de Pecas na tela Orcamento - "Unitario Aba Baremo" 
	Then Fecha Lista de Pecas - "Unitario Aba Baremo" 
	
	## Finaliza o teste
	Then  Finaliza o teste - "Unitario Aba Baremo" 
	