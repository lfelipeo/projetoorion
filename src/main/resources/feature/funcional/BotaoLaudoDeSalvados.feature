Feature: Testa o botao Laudo Salvados 

Scenario: Execute o teste no botao Laudo Salvados 

## Informa��es iniciais do teste e efetua o login
	Given Inicializa as informacoes do teste - "Unitario Botao Laudo Salvados_Abrir Sinistro" 
	## Seleciona o Sinistro desejado
	Given Que login foi efetuado seleciona o sinistro - "Unitario Botao Salvados_Abrir Sinistro" 
	## Execute o teste em 'Dados do Veiculo'
	When Preenche as informacoes na tela Dados do Veiculo - "Unitario Botao Salvados_Abrir Sinistro" 
	
	### ------------------- BOT�O LAUDO SALVADOS ------------------- ###
	Then Clica no botao Laudo de Salvados - "Unitario Botao Salvados_Abrir Sinistro" 
	And Seleciona a opcao em Carroceria em Laudo de Salvados - "Unitario Botao Salvados_Abrir Sinistro" 
	And Seleciona a opcao em Vidros em Laudo de Salvados - "Unitario Botao Salvados_Abrir Sinistro" 
	And Seleciona a opcao em Acessorios em Laudo de Salvados - "Unitario Botao Salvados_Abrir Sinistro" 
	And Seleciona a opcao em Pneus/Roda em Laudo de Salvados - "Unitario Botao Salvados_Abrir Sinistro" 
	And Seleciona a opcao em Instalacao Eletrica em Laudo de Salvados - "Unitario Botao Salvados_Abrir Sinistro" 
	And Seleciona a opcao em Mecanica em Laudo de Salvados - "Unitario Botao Salvados_Abrir Sinistro" 
	Then fecha Laudo de Salvados - "Unitario Botao Salvados_Abrir Sinistro" 
	
	## Finaliza o teste
	Then  Finaliza o teste - "Unitario Botao Salvados_Abrir Sinistro" 