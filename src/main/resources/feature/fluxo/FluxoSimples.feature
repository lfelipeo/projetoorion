Feature: Fluxo de Teste do Oderlei 

Scenario: Execute os passos de um teste comum 

	### ------------------- PADR�O - INICIO ------------------- ###

	Given Inicializa as informacoes do teste - "Fluxo A" 
	Given Que login foi efetuado seleciona o sinistro - "Fluxo A" 
	
	### ------------------- DADOS DO VEICULO ------------------- ###
	
	Then Verifica se a tela Dados do Veiculo esta visivel - "Fluxo A" 
	And Preenche as informacoes do Veiculo na tela Dados do Veiculo - "Fluxo A" 
	And Seleciona os Opcionais na tela Dados do Veiculo - "Fluxo A" 
	Then Clica Confirmar na tela Dados do Veiculo - "Fluxo A" 
	
	### ------------------- ABA BAREMO ------------------- ###
	
	Then Verifica aba Baremo esta visivel na tela de Orcamento - "Fluxo A" 
	And Seleciona uma regiao do carro na Aba Baremo na tela de Orcamento - "Fluxo A" 
	And Seleciona e adiciona uma peca do carro na Aba Baremo na tela de Orcamento - "Fluxo A" 
	
	### ------------------- BOT�O EMISS�O DE RELATORIOS ------------------- ###
	
	Then Clica no botao Emissao de Relatorios - "Fluxo A" 
	Then Abre a janela Ordenar Por Descricao em Emissao de Relatorios - "Fluxo A" 
	And  Verifica a peca adicionada na aba Baremo na janela Ordenar Por Descricao em Emissao de Relatorios - "Fluxo A" 
	And Verifica a peca adicionada na aba Manual na janela Ordenar Por Descricao em Emissao de Relatorios - "Fluxo A" 
	Then Fecha a janela Ordenar Por Descricao em Emissao de Relatorios - "Fluxo A" 
	Then Fecha Emissao de Relatorios - "Fluxo A" 
	
	### ------------------- BOT�O LISTA DE PE�AS ------------------- ###
	
	Then Clica no botao Lista de Pecas na tela Orcamento - "Fluxo A" 
	And Verifica a peca adicionada na aba Baremo em Lista de Pecas na tela Orcamento - "Fluxo A" 
	Then Fecha Lista de Pecas - "Fluxo A" 
	
	### ------------------- BOT�O M�O DE OBRA ------------------- ###
	
	Then Clica no botao Mao-De-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Mecanica em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Funilaria em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Pintura em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Eletrica em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Tapecaria em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Vidracaria em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Reparacao em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	Then Clica no botao Confirmar em Mao-De-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	
	### ------------------- BOT�O OUTROS VALOERS ------------------- ###
	
	Then Clica no botao Outros Valores - "Fluxo A" 
	And Altera a Previsao de Entrega em Outros Valores - "Fluxo A" 
	And Preenche o capmo Observacao em Outros Valores - "Fluxo A" 
	Then Clica em confirmar Outros Valores - "Fluxo A" 
	
	### ------------------- BOT�O IDENTIFICA��O DE SINISTRO ------------------- ###
	
	Then Clica no botao Identificacao de Sinistro - "Fluxo Abrir Sinistro" 
	And Verifica a placa em Identificacao de Sinistro - "Fluxo Abrir Sinistro" 
	Then Fecha Identificacao de Sinistro - "Fluxo Abrir Sinistro" 
	
	### ------------------- BOT�O FECHAR ESTE SINISTRO ------------------- ###
	
	#	Then Clica no botao Fechar Esta Sinistro na tela de Orcamento - "Fluxo Abrir Sinistro" 
	#	And Seleciona uma opcao em Carimbo do Processo em Fechar Esta Sinistro - "Fluxo Abrir Sinistro" 
	#	And Escreve em Observacao em Fechar Esta Sinistro - "Fluxo Abrir Sinistro" 
	#	Then Clica em Confirmar em Fechar Esta Sinistro - "Fluxo Abrir Sinistro" 
	#	And Verifica a janela pos botao confirmar em Fechar Esta Sinistro - "Fluxo Abrir Sinistro" 
	
	### ------------------- PADR�O - FIM ------------------- ###
	
	Then Finaliza o teste - "Fluxo Abrir Sinistro" 
	
	
	
	