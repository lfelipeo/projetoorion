Feature: Fluxo com as a��es do Sinistro 
Scenario: Todas as a��es do fluxo de Abertura de Sinistro  HYT-4567 

### ------------------- PADR�O ------------------- ###
	Given Inicializa as informacoes do teste - "Fluxo Abrir Sinistro" 
	Given Que login foi efetuado seleciona o sinistro - "Fluxo Abrir Sinistro" 
	
	#	### ------------------- DADOS DO VEICULO ------------------- ###
	#	Then Verifica se a tela Dados do Veiculo esta visivel - "Fluxo Abrir Sinistro" 
	#	And Preenche as informacoes do Veiculo na tela Dados do Veiculo - "Fluxo Abrir Sinistro" 
	#	And Seleciona os Opcionais na tela Dados do Veiculo - "Fluxo Abrir Sinistro" 
	#	Then Clica Confirmar na tela Dados do Veiculo - "Fluxo Abrir Sinistro" 
	
	#	#	## ------------------- PR� OR�AMENTO ------------------- ### 
	#	#	#	Then Abre a tela de Pre Orcamento - "Fluxo Abrir Sinistro" 
	#	#	#	And Seleciona uma regiao do carro em Pre Orcamento na tela de Orcamento - "Fluxo Abrir Sinistro" 
	#	#	#	And Escolhe peca em Pre Orcamento na tela de Orcamento - "Fluxo Abrir Sinistro" 
	#	#	#	And Escolhe peca na tela Pecas em Pre Orcamento na tela de Orcamento - "Fluxo Abrir Sinistro" 
	#	#	#	Then Clica no botao atualizar do Pre Orcamento na tela Orcamento - "Fluxo Abrir Sinistro" 
	
	# ------------------- ABA BAREMO ------------------- ###
	Then Verifica aba Baremo esta visivel na tela de Orcamento - "Unitario Aba Baremo_Abrir Sinistro" 
	And Seleciona uma regiao do carro na Aba Baremo na tela de Orcamento - "Unitario Aba Baremo_Abrir Sinistro" 
	And Seleciona e adiciona uma peca do carro na Aba Baremo na tela de Orcamento - "Unitario Aba Baremo_Abrir Sinistro" 
	
	### ------------------- ABA MANUAL ------------------- ###
	Then Clica na aba manual na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche as informacoes em Substituicao na aba Manual - "Fluxo Abrir Sinistro" 
	And Preenche as informacoes em Reparacao na aba Manual - "Abertura SinistroS" 
	And Preenche as informacoes em Pintura na aba Manual - "Fluxo Abrir Sinistro" 
	
	### ------------------- ABA ADICIONAIS ------------------- ###
	Then Clica na aba Adicionais - "Fluxo Abrir Sinistro" 
	And Seleciona uma opcao em Itens na aba Adicionais - "Fluxo Abrir Sinistro" 
	And Preenche as informacoes na aba Adicionais - "Fluxo Abrir Sinistro" 
	Then Clica Adicionar na aba Adicionais - "Fluxo Abrir Sinistro" 
	
	#	# ## ------------------- ABA FOTOS ------------------- ###
	#	# Then Clica na aba fotos - "Fluxo Abrir Sinistro" 
	#	# And Envia a foto em Fotos - "Fluxo Abrir Sinistro" 
	#	# Then Verifica se a foto foi adicionada em Fotos - "Fluxo Abrir Sinistro" 
	
	#	#	### ------------------- BOTAO FOTOS ------------------- ###
	#	#	Then Clica no botao Fotos - "Fluxo Abrir Sinistro" 
	#	#	And Verifica a foto adicionada em Fotos - "Fluxo Abrir Sinistro" 
	#	#	Then Fecha Fotos - "Fluxo Abrir Sinistro" 
	
	### ------------------- BOT�O EMISS�O DE RELATORIOS ------------------- ###
	
	Then Clica no botao Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre a janela Ordenar Por Descricao em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica a peca adicionada na aba Baremo na janela Ordenar Por Descricao em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica a peca adicionada na aba Manual na janela Ordenar Por Descricao em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha a janela Ordenar Por Descricao em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre a janela Peca em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica a peca adicionada na aba Baremo na janela Peca em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica a peca adicionada na aba Manual na janela Peca em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha a janela Peca em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre a janela Servico em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica a peca adicionada na aba Baremo na janela Servico em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica a peca adicionada na aba Manual na janela Servico em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha a janela Servico em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre a janela Salvado em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica o numero da placa do Sinistro na janela Salvado em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha a janela Salvado em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre a janela Pmg em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica o numero da placa do Sinistro na janela Pmg em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha a janela Pmg em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre o pop-up Foto em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica a tabela de fotos na janela Foto em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha o pop-up Foto em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre a janela Historico em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica o numero da placa do Sinistro na janela Historico em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha a janela Historico em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre a janela Revisao em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica o numero da placa do Sinistro na janela Revisao em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha a janela Revisao em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre a janela Acesso em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica o numero da placa do Sinistro na janela Acesso em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha a janela Acesso em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre a janela Connect em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica o numero da placa do Sinistro na janela Connect em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha a janela Connect em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre a janela Avarias em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica o numero da placa do Sinistro na janela Avarias em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha a janela Avarias em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Abre a janela Fotos Avarias em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	And Verifica o numero da placa do Sinistro na janela Fotos Avarias em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	Then Fecha a janela Fotos Avarias em Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	Then Fecha Emissao de Relatorios - "Fluxo Abrir Sinistro" 
	
	### ------------------- BOT�O LISTA DE PE�AS ------------------- ###
	
	Then Clica no botao Lista de Pecas na tela Orcamento - "Fluxo Abrir Sinistro" 
	And Verifica a peca adicionada na aba Baremo em Lista de Pecas na tela Orcamento - "Unitario Aba Baremo_Abrir Sinistro" 
	And Verifica a peca adicionada em Substituicao na aba Manual em Lista de Pecas na tela Orcamento - "Fluxo Abrir Sinistro" 
	And Verifica a peca adicionada em Reparacao na aba Manual em Lista de Pecas na tela Orcamento - "Fluxo Abrir Sinistro" 
	And Verifica a peca adicionada em Pintura na aba Manual em Lista de Pecas na tela Orcamento - "Fluxo Abrir Sinistro" 
	Then Fecha Lista de Pecas - "Fluxo Abrir Sinistro" 
	
	### ------------------- BOT�O LAUDO DE SALVADOS ------------------- ###
	Then Clica no botao Laudo de Salvados - "Fluxo Abrir Sinistro" 
	And Seleciona a opcao em Carroceria em Laudo de Salvados - "Fluxo Abrir Sinistro" 
	And Seleciona a opcao em Vidros em Laudo de Salvados - "Fluxo Abrir Sinistro" 
	And Seleciona a opcao em Acessorios em Laudo de Salvados - "Fluxo Abrir Sinistro" 
	And Seleciona a opcao em Pneus/Roda em Laudo de Salvados - "Fluxo Abrir Sinistro" 
	And Seleciona a opcao em Instalacao Eletrica em Laudo de Salvados - "Fluxo Abrir Sinistro" 
	And Seleciona a opcao em Mecanica em Laudo de Salvados - "Fluxo Abrir Sinistro" 
	Then fecha Laudo de Salvados - "Fluxo Abrir Sinistro" 
	
	### ------------------- BOT�O LAUDO PMG ------------------- ###
	Then Clica no botao Laudo PMG na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Seleciona uma opcao em PMG Automovel em Laudo PMG - "Fluxo Abrir Sinistro" 
	And Preenche as informacoes em Observacao em Laudo PMG - "Fluxo Abrir Sinistro" 
	Then Clica em Confirmar em Laudo PMG - "Fluxo Abrir Sinistro" 
	
	### ------------------- BOT�O M�O DE OBRA ------------------- ###
	Then Clica no botao Mao-De-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Mecanica em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Funilaria em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Pintura em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Eletrica em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Tapecaria em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Vidracaria em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Preenche o campo Reparacao em Mao-de-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	Then Clica no botao Confirmar em Mao-De-Obra na tela de Orcamento - "Fluxo Abrir Sinistro" 
	
	### ------------------- BOT�O OUTROS VALOERS ------------------- ###
	Then Clica no botao Outros Valores - "Fluxo Abrir Sinistro" 
	#	And Altera a Importancia Segurada em Outros Valores - "Fluxo Abrir Sinistro"
	And Altera a Previsao de Entrega em Outros Valores - "Fluxo Abrir Sinistro" 
	Then Clica em confirmar Outros Valores - "Fluxo Abrir Sinistro" 
	
	### ------------------- BOT�O DADOS DO VEICULO ------------------- ###
	Then Clica no botao Dados do Veiculo na tela de Orcamento - "Fluxo Abrir Sinistro" 
	And Altera os opcionais em Dados do Veiculo na tela de Orcamento - "Fluxo Abrir Sinistro" 
	Then Confirmar Dados do Veiculo na tela de Orcamento - "Fluxo Abrir Sinistro" 
	
	## ------------------- BOT�O DETALHES ------------------- ###
	Then Clica no botao Detalhes na Tela Orcamento - "Fluxo Abrir Sinistro" 
	And Verifica a placa em Detalhes na Tela Orcamento - "Fluxo Abrir Sinistro" 
	Then Fecha Detalhes na Tela Orcamento - "Fluxo Abrir Sinistro" 
	
	### ------------------- BOT�O IDENTIFICA��O DE SINISTRO ------------------- ###
	Then Clica no botao Identificacao de Sinistro - "Fluxo Abrir Sinistro" 
	And Verifica a placa em Identificacao de Sinistro - "Fluxo Abrir Sinistro" 
	Then Fecha Identificacao de Sinistro - "Fluxo Abrir Sinistro" 
	
	#	### ------------------- BOT�O FECHAR ESTE SINISTRO ------------------- ###
	#	Then Clica no botao Fechar Esta Sinistro na tela de Orcamento - "Fluxo Abrir Sinistro" 
	#	And Seleciona uma opcao em Carimbo do Processo em Fechar Esta Sinistro - "Fluxo Abrir Sinistro" 
	#	And Preenche Observacao em Fechar Esta Sinistro - "Fluxo Abrir Sinistro" 
	#	Then Clica em Confirmar em Fechar Esta Sinistro - "Fluxo Abrir Sinistro" 
	#	And Verifica a janela pos botao confirmar em Fechar Esta Sinistro - "Fluxo Abrir Sinistro" 
	#	
	#	### ------------------- BOT�O LOCALIZADOR (TELA INICIAL) ------------------- ###
	#	Then Clica no botao Localizador - "Fluxo Abrir Sinistro" 
	#	And Seleciona a opcao "Placa" em Criterio de Consulta em Localizador - "Fluxo Abrir Sinistro" 
	#	And Preenche a Placa e pesquisa em Criterio de Consulta em Localizador - "Fluxo Abrir Sinistro" 
	#	And Valida se o elemento pesquisado esta com a situacao "Realizado" em Localizador - "Fluxo Abrir Sinistro" 
	#	Then Fecha Localizador - "Fluxo Abrir Sinistro" 
	
	## Finaliza o teste
	Then Finaliza o teste - "Fluxo Abrir Sinistro"