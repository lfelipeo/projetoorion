Feature: Teste o botao Outros Valores 

Scenario: Execute o teste no botao Outros Valores 

## Informa��es iniciais do teste e efetua o login
	Given Inicializa as informacoes do teste - "Unitario Botao Outros Valores_Abrir Sinistro" 
	
	## Seleciona o Sinistro desejado
	Given Que login foi efetuado seleciona o sinistro - "Unitario Botao Outros Valores_Abrir Sinistro" 
	
	## Caso necess�rio, executa o dados do veiculo
	When Preenche as informacoes na tela Dados do Veiculo - "Unitario Botao Outros Valores_Abrir Sinistro" 
	
	## A��es do testes no bot�o 'Outros Valores'
	Then Clica no botao Outros Valores - "Unitario Botao Outros Valores_Abrir Sinistro" 
	#	And Altera a Importancia Segurada em Outros Valores - "Unitario Botao Outros Valores_Abrir Sinistro" 
	And Altera a Previsao de Entrega em Outros Valores - "Unitario Botao Outros Valores_Abrir Sinistro" 
	Then Clica em confirmar Outros Valores - "Unitario Botao Outros Valores_Abrir Sinistro" 
	
	# Finaliza o teste
	Then  Finaliza o teste - "Unitario Botao Outros Valores_Abrir Sinistro" 
	
	
