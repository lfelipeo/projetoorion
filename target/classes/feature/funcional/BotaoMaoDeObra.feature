Feature: Fluxo com as a��es do Sinistro 

Scenario: Todas as a��es do fluxo de Abertura de Sinistro  HYT-4567 

## Informa��es iniciais do teste e efetua o login
	Given Inicializa as informacoes do teste - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	
	## Seleciona o Sinistro desejado
	Given Que login foi efetuado seleciona o sinistro - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	
	## Execute o teste em 'Dados do Veiculo'
	When Preenche as informacoes na tela Dados do Veiculo - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	
	### ------------------- BOT�O M�O DE OBRA ------------------- ###
	Then Clica no botao Mao-De-Obra na tela de Orcamento - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	And Preenche o campo Mecanica em Mao-de-Obra na tela de Orcamento - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	And Preenche o campo Funilaria em Mao-de-Obra na tela de Orcamento - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	And Preenche o campo Pintura em Mao-de-Obra na tela de Orcamento - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	And Preenche o campo Eletrica em Mao-de-Obra na tela de Orcamento - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	And Preenche o campo Tapecaria em Mao-de-Obra na tela de Orcamento - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	And Preenche o campo Vidracaria em Mao-de-Obra na tela de Orcamento - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	And Preenche o campo Reparacao em Mao-de-Obra na tela de Orcamento - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	Then Clica no botao Confirmar em Mao-De-Obra na tela de Orcamento - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	
	## Finaliza o teste
	Then  Finaliza o teste - "Unitario Botao Mao de Obra_Abrir Sinistro" 
	