Feature: Unitario Aba Manual - Abrir Sinistro 

Scenario: Execute o teste na aba Manual 


### ------------------- PADR�O - INICIO ------------------- ###
	Given Inicializa as informacoes do teste - "Unitario Aba Manual_Abrir Sinistro" 
	Given Que login foi efetuado seleciona o sinistro - "Unitario Aba Manual_Abrir Sinistro" 
	
	### ------------------- DADOS DO VEICULO ------------------- ###
	When Preenche as informacoes na tela Dados do Veiculo - "Unitario Aba Manual_Abrir Sinistro" 
	
	### ------------------- ABA MANUAL ------------------- ###
	Then Clica na aba manual na tela de Orcamento - "Unitario Aba Manual_Abrir Sinistro" 
	And Preenche as informacoes em Substituicao na aba Manual - "Unitario Aba Manual_Abrir Sinistro" 
	And Preenche as informacoes em Reparacao na aba Manual - "Unitario Aba Manual_Abrir Sinistro" 
	And Preenche as informacoes em Pintura na aba Manual - "Unitario Aba Manual_Abrir Sinistro" 
	
	### ------------------- BOT�O LISTA DE PE�AS ------------------- ###
#	Then Clica no botao Lista de Pecas na tela Orcamento - "Unitario Aba Manual_Abrir Sinistro" 
#	And Verifica a peca adicionada em Substituicao na aba Manual em Lista de Pecas na tela Orcamento - "Unitario Aba Manual_Abrir Sinistro" 
#	And Verifica a peca adicionada em Reparacao na aba Manual em Lista de Pecas na tela Orcamento - "Unitario Aba Manual_Abrir Sinistro" 
#	And Verifica a peca adicionada em Pintura na aba Manual em Lista de Pecas na tela Orcamento - "Unitario Aba Manual_Abrir Sinistro" 
#	Then Fecha Lista de Pecas - "Unitario Aba Manual_Abrir Sinistro" 
	
	### ------------------- PADR�O - FIM ------------------- ###
	Then  Finaliza o teste - "Unitario Aba Manual_Abrir Sinistro" 