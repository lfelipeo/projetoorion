Feature: Testa o botao Detalhes 

Scenario: Execute o teste no botao Detalhes 

## Informa��es iniciais do teste e efetua o login
	Given Inicializa as informacoes do teste - "Unitario Botao Detalhes_Abrir Sinistro" 
	
	## Seleciona o Sinistro desejado
	Given Que login foi efetuado seleciona o sinistro - "Unitario Botao Detalhes_Abrir Sinistro" 
	
	## Execute o teste em 'Dados do Veiculo'
	When Preenche as informacoes na tela Dados do Veiculo - "Unitario Botao Detalhes_Abrir Sinistro" 
	
	## Executa o teste no bot�o 'Detalhes'
	Then Clica no botao Detalhes na Tela Orcamento - "Unitario Botao Detalhes_Abrir Sinistro" 
	And Verifica a placa em Detalhes na Tela Orcamento - "Unitario Botao Detalhes_Abrir Sinistro" 
	Then Fecha Detalhes na Tela Orcamento - "Unitario Botao Detalhes_Abrir Sinistro" 
	
	## Finaliza o teste
	Then  Finaliza o teste - "Unitario Botao Detalhes_Abrir Sinistro" 