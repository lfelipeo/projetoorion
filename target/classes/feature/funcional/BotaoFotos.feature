Feature: Fluxo com as a��es do Sinistro 

Scenario: Todas as a��es do fluxo de Abertura de Sinistro  HYT-4567 

	### ------------------- PADR�O ------------------- ###
	Given Inicializa as informacoes do teste - "Unitario Botao Fotos" 
	Given Que login foi efetuado seleciona o sinistro - "Unitario Botao Fotos" 
	
	### ------------------- DADOS DO VEICULO ------------------- ###
	When Preenche as informacoes na tela Dados do Veiculo - "Fluxo Abrir Sinistro" 
	
	### ------------------- BOT�O FOTOS ------------------- ###
	Then Clica no botao Fotos - "Unitario Botao Fotos" 
	And Verifica a foto adicionada em Fotos - "Unitario Botao Fotos" 
	Then Fecha Fotos - "Unitario Botao Fotos" 
	
	## Finaliza o teste
	Then  Finaliza o teste - "Unitario Botao Fotos" 
	
		