Feature: Unitario Aba Fotos 

Scenario: Execute o teste na aba Fotos 

### ------------------- PADR�O - INICIO ------------------- ###
	Given Inicializa as informacoes do teste - "Unitario Aba Fotos_Abrir Sinistro" 
	Given Que login foi efetuado seleciona o sinistro - "Unitario Aba Fotos_Abrir Sinistro" 
	
	### ------------------- DADOS DO VEICULO ------------------- ###
	When Preenche as informacoes na tela Dados do Veiculo - "Unitario Aba Fotos_Abrir Sinistro" 
	
	### ------------------- ABA FOTOS ------------------- ###
	Then Clica na aba fotos - "Unitario Aba Fotos_Abrir Sinistro" 
	And Envia a foto em Fotos - "Unitario Aba Fotos_Abrir Sinistro" 
	Then Verifica se a foto foi adicionada em Fotos - "Fluxo Abrir Sinistro" 
	
	
	### ------------------- PADR�O - FINAL ------------------- ###
	Then  Finaliza o teste - "Unitario Aba Fotos_Abrir Sinistro" 