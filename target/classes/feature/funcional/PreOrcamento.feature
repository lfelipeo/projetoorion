Feature: Unitario Pre Orcamento 
Scenario: Executa os testes em Pre-Orcamento

	### ------------------- PADR�O ------------------- ###
	Given Inicializa as informacoes do teste - "Unitario_PreOrcamento" 
	Given Que login foi efetuado seleciona o sinistro - "Unitario_PreOrcamento" 
	
	### ------------------- DADOS DO VEICULO ------------------- ###
	Then Verifica se a tela Dados do Veiculo esta visivel - "Unitario_PreOrcamento" 
	And Preenche as informacoes do Veiculo na tela Dados do Veiculo - "Unitario_PreOrcamento" 
	And Seleciona os Opcionais na tela Dados do Veiculo - "Unitario_PreOrcamento" 
	Then Clica Confirmar na tela Dados do Veiculo - "Unitario_PreOrcamento" 
	
	## ------------------- PR� OR�AMENTO ------------------- ###
	Then Abre a tela de Pre Orcamento - "Unitario_PreOrcamento" 
	And Seleciona uma regiao do carro em Pre Orcamento na tela de Orcamento - "Unitario_PreOrcamento" 
	And Escolhe peca em Pre Orcamento na tela de Orcamento - "Unitario_PreOrcamento" 
	And Escolhe peca na tela Pecas em Pre Orcamento na tela de Orcamento - "Unitario_PreOrcamento" 
	Then Clica no botao atualizar do Pre Orcamento na tela Orcamento - "Unitario_PreOrcamento" 
