$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/resources/feature/fluxo/FluxoSinistro.feature");
formatter.feature({
  "line": 1,
  "name": "Fluxo com as a��es do Sinistro",
  "description": "",
  "id": "fluxo-com-as-a��es-do-sinistro",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 2,
  "name": "Todas as a��es do fluxo de Abertura de Sinistro  HYT-4567",
  "description": "",
  "id": "fluxo-com-as-a��es-do-sinistro;todas-as-a��es-do-fluxo-de-abertura-de-sinistro--hyt-4567",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "comments": [
    {
      "line": 4,
      "value": "### ------------------- PADR�O ------------------- ###"
    }
  ],
  "line": 5,
  "name": "Inicializa as informacoes do teste - \"Fluxo Abrir Sinistro\"",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Que login foi efetuado seleciona o sinistro - \"Fluxo Abrir Sinistro\"",
  "keyword": "Given "
});
formatter.step({
  "comments": [
    {
      "line": 8,
      "value": "#\t### ------------------- DADOS DO VEICULO ------------------- ###"
    },
    {
      "line": 9,
      "value": "#\tThen Verifica se a tela Dados do Veiculo esta visivel - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 10,
      "value": "#\tAnd Preenche as informacoes do Veiculo na tela Dados do Veiculo - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 11,
      "value": "#\tAnd Seleciona os Opcionais na tela Dados do Veiculo - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 12,
      "value": "#\tThen Clica Confirmar na tela Dados do Veiculo - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 14,
      "value": "#\t#\t## ------------------- PR� OR�AMENTO ------------------- ###"
    },
    {
      "line": 15,
      "value": "#\t#\t#\tThen Abre a tela de Pre Orcamento - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 16,
      "value": "#\t#\t#\tAnd Seleciona uma regiao do carro em Pre Orcamento na tela de Orcamento - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 17,
      "value": "#\t#\t#\tAnd Escolhe peca em Pre Orcamento na tela de Orcamento - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 18,
      "value": "#\t#\t#\tAnd Escolhe peca na tela Pecas em Pre Orcamento na tela de Orcamento - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 19,
      "value": "#\t#\t#\tThen Clica no botao atualizar do Pre Orcamento na tela Orcamento - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 21,
      "value": "# ------------------- ABA BAREMO ------------------- ###"
    }
  ],
  "line": 22,
  "name": "Verifica aba Baremo esta visivel na tela de Orcamento - \"Unitario Aba Baremo_Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "Seleciona uma regiao do carro na Aba Baremo na tela de Orcamento - \"Unitario Aba Baremo_Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "Seleciona e adiciona uma peca do carro na Aba Baremo na tela de Orcamento - \"Unitario Aba Baremo_Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 26,
      "value": "### ------------------- ABA MANUAL ------------------- ###"
    }
  ],
  "line": 27,
  "name": "Clica na aba manual na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "Preenche as informacoes em Substituicao na aba Manual - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "Preenche as informacoes em Reparacao na aba Manual - \"Abertura SinistroS\"",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "Preenche as informacoes em Pintura na aba Manual - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 32,
      "value": "### ------------------- ABA ADICIONAIS ------------------- ###"
    }
  ],
  "line": 33,
  "name": "Clica na aba Adicionais - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "Seleciona uma opcao em Itens na aba Adicionais - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "Preenche as informacoes na aba Adicionais - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "Clica Adicionar na aba Adicionais - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 38,
      "value": "#\t# ## ------------------- ABA FOTOS ------------------- ###"
    },
    {
      "line": 39,
      "value": "#\t# Then Clica na aba fotos - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 40,
      "value": "#\t# And Envia a foto em Fotos - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 41,
      "value": "#\t# Then Verifica se a foto foi adicionada em Fotos - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 43,
      "value": "#\t#\t### ------------------- BOTAO FOTOS ------------------- ###"
    },
    {
      "line": 44,
      "value": "#\t#\tThen Clica no botao Fotos - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 45,
      "value": "#\t#\tAnd Verifica a foto adicionada em Fotos - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 46,
      "value": "#\t#\tThen Fecha Fotos - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 48,
      "value": "### ------------------- BOT�O EMISS�O DE RELATORIOS ------------------- ###"
    }
  ],
  "line": 50,
  "name": "Clica no botao Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 52,
  "name": "Abre a janela Ordenar Por Descricao em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 53,
  "name": "Verifica a peca adicionada na aba Baremo na janela Ordenar Por Descricao em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "Verifica a peca adicionada na aba Manual na janela Ordenar Por Descricao em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 55,
  "name": "Fecha a janela Ordenar Por Descricao em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 57,
  "name": "Abre a janela Peca em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 58,
  "name": "Verifica a peca adicionada na aba Baremo na janela Peca em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "Verifica a peca adicionada na aba Manual na janela Peca em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": "Fecha a janela Peca em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 62,
  "name": "Abre a janela Servico em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 63,
  "name": "Verifica a peca adicionada na aba Baremo na janela Servico em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 64,
  "name": "Verifica a peca adicionada na aba Manual na janela Servico em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 65,
  "name": "Fecha a janela Servico em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 67,
  "name": "Abre a janela Salvado em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 68,
  "name": "Verifica o numero da placa do Sinistro na janela Salvado em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 69,
  "name": "Fecha a janela Salvado em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 71,
  "name": "Abre a janela Pmg em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 72,
  "name": "Verifica o numero da placa do Sinistro na janela Pmg em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 73,
  "name": "Fecha a janela Pmg em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 75,
  "name": "Abre o pop-up Foto em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 76,
  "name": "Verifica a tabela de fotos na janela Foto em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 77,
  "name": "Fecha o pop-up Foto em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 79,
  "name": "Abre a janela Historico em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 80,
  "name": "Verifica o numero da placa do Sinistro na janela Historico em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 81,
  "name": "Fecha a janela Historico em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 83,
  "name": "Abre a janela Revisao em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 84,
  "name": "Verifica o numero da placa do Sinistro na janela Revisao em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "Fecha a janela Revisao em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 87,
  "name": "Abre a janela Acesso em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 88,
  "name": "Verifica o numero da placa do Sinistro na janela Acesso em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 89,
  "name": "Fecha a janela Acesso em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 91,
  "name": "Abre a janela Connect em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 92,
  "name": "Verifica o numero da placa do Sinistro na janela Connect em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 93,
  "name": "Fecha a janela Connect em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 95,
  "name": "Abre a janela Avarias em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "Verifica o numero da placa do Sinistro na janela Avarias em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 97,
  "name": "Fecha a janela Avarias em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 99,
  "name": "Abre a janela Fotos Avarias em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 100,
  "name": "Verifica o numero da placa do Sinistro na janela Fotos Avarias em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 101,
  "name": "Fecha a janela Fotos Avarias em Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 103,
  "name": "Fecha Emissao de Relatorios - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 105,
      "value": "### ------------------- BOT�O LISTA DE PE�AS ------------------- ###"
    }
  ],
  "line": 107,
  "name": "Clica no botao Lista de Pecas na tela Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 108,
  "name": "Verifica a peca adicionada na aba Baremo em Lista de Pecas na tela Orcamento - \"Unitario Aba Baremo_Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 109,
  "name": "Verifica a peca adicionada em Substituicao na aba Manual em Lista de Pecas na tela Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 110,
  "name": "Verifica a peca adicionada em Reparacao na aba Manual em Lista de Pecas na tela Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 111,
  "name": "Verifica a peca adicionada em Pintura na aba Manual em Lista de Pecas na tela Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 112,
  "name": "Fecha Lista de Pecas - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 114,
      "value": "### ------------------- BOT�O LAUDO DE SALVADOS ------------------- ###"
    }
  ],
  "line": 115,
  "name": "Clica no botao Laudo de Salvados - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 116,
  "name": "Seleciona a opcao em Carroceria em Laudo de Salvados - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 117,
  "name": "Seleciona a opcao em Vidros em Laudo de Salvados - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 118,
  "name": "Seleciona a opcao em Acessorios em Laudo de Salvados - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 119,
  "name": "Seleciona a opcao em Pneus/Roda em Laudo de Salvados - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 120,
  "name": "Seleciona a opcao em Instalacao Eletrica em Laudo de Salvados - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 121,
  "name": "Seleciona a opcao em Mecanica em Laudo de Salvados - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 122,
  "name": "fecha Laudo de Salvados - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 124,
      "value": "### ------------------- BOT�O LAUDO PMG ------------------- ###"
    }
  ],
  "line": 125,
  "name": "Clica no botao Laudo PMG na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 126,
  "name": "Seleciona uma opcao em PMG Automovel em Laudo PMG - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 127,
  "name": "Preenche as informacoes em Observacao em Laudo PMG - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 128,
  "name": "Clica em Confirmar em Laudo PMG - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 130,
      "value": "### ------------------- BOT�O M�O DE OBRA ------------------- ###"
    }
  ],
  "line": 131,
  "name": "Clica no botao Mao-De-Obra na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 132,
  "name": "Preenche o campo Mecanica em Mao-de-Obra na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 133,
  "name": "Preenche o campo Funilaria em Mao-de-Obra na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 134,
  "name": "Preenche o campo Pintura em Mao-de-Obra na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 135,
  "name": "Preenche o campo Eletrica em Mao-de-Obra na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 136,
  "name": "Preenche o campo Tapecaria em Mao-de-Obra na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 137,
  "name": "Preenche o campo Vidracaria em Mao-de-Obra na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 138,
  "name": "Preenche o campo Reparacao em Mao-de-Obra na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 139,
  "name": "Clica no botao Confirmar em Mao-De-Obra na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 141,
      "value": "### ------------------- BOT�O OUTROS VALOERS ------------------- ###"
    }
  ],
  "line": 142,
  "name": "Clica no botao Outros Valores - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 143,
      "value": "#\tAnd Altera a Importancia Segurada em Outros Valores - \"Fluxo Abrir Sinistro\""
    }
  ],
  "line": 144,
  "name": "Altera a Previsao de Entrega em Outros Valores - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 145,
  "name": "Clica em confirmar Outros Valores - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 147,
      "value": "### ------------------- BOT�O DADOS DO VEICULO ------------------- ###"
    }
  ],
  "line": 148,
  "name": "Clica no botao Dados do Veiculo na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 149,
  "name": "Altera os opcionais em Dados do Veiculo na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 150,
  "name": "Confirmar Dados do Veiculo na tela de Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 152,
      "value": "## ------------------- BOT�O DETALHES ------------------- ###"
    }
  ],
  "line": 153,
  "name": "Clica no botao Detalhes na Tela Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 154,
  "name": "Verifica a placa em Detalhes na Tela Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 155,
  "name": "Fecha Detalhes na Tela Orcamento - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 157,
      "value": "### ------------------- BOT�O IDENTIFICA��O DE SINISTRO ------------------- ###"
    }
  ],
  "line": 158,
  "name": "Clica no botao Identificacao de Sinistro - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "line": 159,
  "name": "Verifica a placa em Identificacao de Sinistro - \"Fluxo Abrir Sinistro\"",
  "keyword": "And "
});
formatter.step({
  "line": 160,
  "name": "Fecha Identificacao de Sinistro - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 162,
      "value": "#\t### ------------------- BOT�O FECHAR ESTE SINISTRO ------------------- ###"
    },
    {
      "line": 163,
      "value": "#\tThen Clica no botao Fechar Esta Sinistro na tela de Orcamento - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 164,
      "value": "#\tAnd Seleciona uma opcao em Carimbo do Processo em Fechar Esta Sinistro - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 165,
      "value": "#\tAnd Preenche Observacao em Fechar Esta Sinistro - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 166,
      "value": "#\tThen Clica em Confirmar em Fechar Esta Sinistro - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 167,
      "value": "#\tAnd Verifica a janela pos botao confirmar em Fechar Esta Sinistro - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 168,
      "value": "#"
    },
    {
      "line": 169,
      "value": "#\t### ------------------- BOT�O LOCALIZADOR (TELA INICIAL) ------------------- ###"
    },
    {
      "line": 170,
      "value": "#\tThen Clica no botao Localizador - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 171,
      "value": "#\tAnd Seleciona a opcao \"Placa\" em Criterio de Consulta em Localizador - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 172,
      "value": "#\tAnd Preenche a Placa e pesquisa em Criterio de Consulta em Localizador - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 173,
      "value": "#\tAnd Valida se o elemento pesquisado esta com a situacao \"Realizado\" em Localizador - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 174,
      "value": "#\tThen Fecha Localizador - \"Fluxo Abrir Sinistro\""
    },
    {
      "line": 176,
      "value": "## Finaliza o teste"
    }
  ],
  "line": 177,
  "name": "Finaliza o teste - \"Fluxo Abrir Sinistro\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 38
    }
  ],
  "location": "BasePattern.inicializarTeste(String)"
});
formatter.result({
  "duration": 20147803137,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 47
    }
  ],
  "location": "TelaInicalStep.telaInicialOrion(String)"
});
formatter.result({
  "duration": 1753721348,
  "error_message": "java.lang.Exception: /------ ERRO NA TELA INICIAL. Erro Info: ERRO AO CLICAR NO BOTÃO ABRIR SINISTRO DO SINISTRO (HYT-4567).\r\nERRO AO TROCAR O FOCO PARA O FRAME AGENDA. Erro: java.lang.Exception: ERRO AO VOLTAR O FOCO PARA O FRAME PRINCIPAL. Selenium Erro: org.openqa.selenium.UnhandledAlertException: Modal dialog present with text: Esta aréa é somente para usuários autorizados. Verifique se não houve \nerro na digitação, caso contrário contate o suporte indicado abaixo. \nBus_Caixa_Postal.Obtem_Acesso : Esta aréa é somente para usuários autorizados. Verifique se não houve \nerro na digitação, caso contrário contate o suporte indicado abaixo. \nBus_Caixa_Postal.Obtem_Acesso \nBuild info: version: \u00273.13.0\u0027, revision: \u00272f0d292\u0027, time: \u00272018-06-25T15:24:21.231Z\u0027\nSystem info: host: \u0027LFO\u0027, ip: \u0027192.168.0.108\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_161\u0027\nDriver info: org.openqa.selenium.ie.InternetExplorerDriver\nCapabilities {acceptInsecureCerts: false, browserName: internet explorer, browserVersion: 11, javascriptEnabled: true, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), se:ieOptions: {browserAttachTimeout: 0, elementScrollBehavior: 0, enablePersistentHover: true, ie.browserCommandLineSwitches: , ie.ensureCleanSession: false, ie.fileUploadDialogTimeout: 3000, ie.forceCreateProcessApi: false, ignoreProtectedModeSettings: true, ignoreZoomSetting: true, initialBrowserUrl: http://localhost:45045/, nativeEvents: true, requireWindowFocus: false}, setWindowRect: true, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: ignore}\nSession ID: c3105b17-8a8f-4abc-9248-afcef4f181aa\r\n\tat es.indra.base.BaseAction.logarErro(BaseAction.java:43)\r\n\tat es.indra.step.perito.telainicial.TelaInicalStep.telaInicialOrion(TelaInicalStep.java:38)\r\n\tat ✽.Given Que login foi efetuado seleciona o sinistro - \"Fluxo Abrir Sinistro\"(src/main/resources/feature/fluxo/FluxoSinistro.feature:6)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "Unitario Aba Baremo_Abrir Sinistro",
      "offset": 57
    }
  ],
  "location": "AbaBaremoStep.abaBaremo(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Unitario Aba Baremo_Abrir Sinistro",
      "offset": 68
    }
  ],
  "location": "AbaBaremoStep.selecionaRegiaoCarroAbaBaremo(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Unitario Aba Baremo_Abrir Sinistro",
      "offset": 77
    }
  ],
  "location": "AbaBaremoStep.selecionaPecaCarroAbaBaremo(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 44
    }
  ],
  "location": "AbaManualStep.abaManual(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 57
    }
  ],
  "location": "AbaManualStep.substituicaoAbaManual(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Abertura SinistroS",
      "offset": 54
    }
  ],
  "location": "AbaManualStep.reparacaoAbaManual(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 52
    }
  ],
  "location": "AbaManualStep.pinturaAbaManual(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 27
    }
  ],
  "location": "AbaAdicionaisStep.abaAdicionais(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 50
    }
  ],
  "location": "AbaAdicionaisStep.selecionaItensAbaAdicionaisOrcamento(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 45
    }
  ],
  "location": "AbaAdicionaisStep.preencheInfoAbaAdicionaisOrcamento(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 37
    }
  ],
  "location": "AbaAdicionaisStep.adicionaInfoAbaAdicionaisOrcamento(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 40
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.botaoEmissaoRelatorios(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 64
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.ordenarPorDescricao(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 101
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPecaBaremoOrdenarPorDescricao(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 101
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPecaManualOrdenarPorDescricao(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 65
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaOrdenarPorDescricao(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 47
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.peca(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 84
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPecaBaremoPeca(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 84
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPecaManualPeca(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 48
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaPeca(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 50
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.servico(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 87
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPecaBaremoServico(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 87
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPecaManualServico(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 51
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaServico(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 50
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.salvado(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 85
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPlacaSinistroSalvado(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 51
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaSalvado(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 46
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.pmg(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 81
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPlacaSinistroPmg(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 47
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaPmg(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 47
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.foto(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 70
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaFoto(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 48
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaFoto(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 52
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.historico(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 87
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPlacaSinistroHistorico(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 53
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaHistorico(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 50
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.revisao(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 85
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPlacaSinistroRevisao(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 51
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaRevisao(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 49
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.acesso(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 84
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPlacaSinistroAcesso(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 50
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaAcesso(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 50
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.connect(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 85
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPlacaSinistroConnect(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 51
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaConnect(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 50
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.avarias(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 85
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPlacaSinistroAvarias(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 51
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaAvarias(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 56
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fotosAvarias(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 91
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.verificaPlacaSinistroFotosAvarias(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 57
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaFotosAvarias(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 31
    }
  ],
  "location": "BotaoEmissaoDeRelatoriosStep.fechaEmissaoRelatorios(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 51
    }
  ],
  "location": "BotaoListaDePecasStep.botaoListaDePecas(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Unitario Aba Baremo_Abrir Sinistro",
      "offset": 80
    }
  ],
  "location": "BotaoListaDePecasStep.verificaPecaBaremo(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 96
    }
  ],
  "location": "BotaoListaDePecasStep.verificaPecaSubstituicaoAbaManual(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 93
    }
  ],
  "location": "BotaoListaDePecasStep.verificaPecaReparacaoAbaManual(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 91
    }
  ],
  "location": "BotaoListaDePecasStep.verificaPecaPinturaAbaManual(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 24
    }
  ],
  "location": "BotaoListaDePecasStep.fechaListaDePecas(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 36
    }
  ],
  "location": "BotaoLaudoDeSalvadosStep.botaoLaudosDeSalvados(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 56
    }
  ],
  "location": "BotaoLaudoDeSalvadosStep.carroceriaLaudoDeSalvados(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 52
    }
  ],
  "location": "BotaoLaudoDeSalvadosStep.vidrosLaudoDeSalvados(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 56
    }
  ],
  "location": "BotaoLaudoDeSalvadosStep.acessoriosLaudosDeSalvados(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 56
    }
  ],
  "location": "BotaoLaudoDeSalvadosStep.pneusRodaLaudoDeSalvados(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 65
    }
  ],
  "location": "BotaoLaudoDeSalvadosStep.instalacaoEletricaLaudoDeSalvados(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 54
    }
  ],
  "location": "BotaoLaudoDeSalvadosStep.mecanicaLaudoDeSalvados(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 27
    }
  ],
  "location": "BotaoLaudoDeSalvadosStep.fecharLaudoDeSalvados(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 49
    }
  ],
  "location": "BotaoLaudoPmgStep.botaoLaudoPmg(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 53
    }
  ],
  "location": "BotaoLaudoPmgStep.pmgAutomovel(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 54
    }
  ],
  "location": "BotaoLaudoPmgStep.observacao(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 35
    }
  ],
  "location": "BotaoLaudoPmgStep.confirma(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 51
    }
  ],
  "location": "BotaoMaoDeObraStep.botaoMaoDeObra(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 65
    }
  ],
  "location": "BotaoMaoDeObraStep.mecanicaMaoDeObra(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 66
    }
  ],
  "location": "BotaoMaoDeObraStep.funilariaMaoDeObra(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 64
    }
  ],
  "location": "BotaoMaoDeObraStep.pinturaMaoDeObra(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 65
    }
  ],
  "location": "BotaoMaoDeObraStep.eletricaMaoDeObra(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 66
    }
  ],
  "location": "BotaoMaoDeObraStep.tapecariaMaoDeObra(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 67
    }
  ],
  "location": "BotaoMaoDeObraStep.vidracariaMaoDeObra(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 66
    }
  ],
  "location": "BotaoMaoDeObraStep.reparacaoMaoDeObra(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 64
    }
  ],
  "location": "BotaoMaoDeObraStep.confirmaMaoDeObra(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 33
    }
  ],
  "location": "BotaoOutrosValoresStep.botaoOutrosValores(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 50
    }
  ],
  "location": "BotaoOutrosValoresStep.alteraPrevisaoDeEntregaOutrosValores(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 37
    }
  ],
  "location": "BotaoOutrosValoresStep.confirmaOutrosValores(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 56
    }
  ],
  "location": "BotaoDadosDoVeiculoStep.botaoDadosDoVeiculo(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 64
    }
  ],
  "location": "BotaoDadosDoVeiculoStep.alteraOpcionaisDadosDoVeiculo(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 51
    }
  ],
  "location": "BotaoDadosDoVeiculoStep.confirmaDadosDoVeiculo(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 45
    }
  ],
  "location": "BotaoDetalhesStep.botaoDetalhesOrcamento(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 50
    }
  ],
  "location": "BotaoDetalhesStep.verificaPlacaDetalhes(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 36
    }
  ],
  "location": "BotaoDetalhesStep.fechaDetalhes(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 44
    }
  ],
  "location": "BotaoIdentificacaoSinistroStep.botaoIdentificacaoSinistro(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 49
    }
  ],
  "location": "BotaoIdentificacaoSinistroStep.verificaEComparaPlaca(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 35
    }
  ],
  "location": "BotaoIdentificacaoSinistroStep.fechaIdentificacaoSinistro(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Fluxo Abrir Sinistro",
      "offset": 20
    }
  ],
  "location": "BasePattern.finalizarTeste(String)"
});
formatter.result({
  "status": "skipped"
});
});